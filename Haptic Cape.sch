<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="SparkFun-Aesthetics">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find non-functional items- supply symbols, logos, notations, frame blocks, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="CREATIVE_COMMONS">
<text x="-20.32" y="5.08" size="1.778" layer="51" font="vector">Released under the Creative Commons Attribution Share-Alike 4.0 License</text>
<text x="0" y="2.54" size="1.778" layer="51" font="vector"> https://creativecommons.org/licenses/by-sa/4.0/</text>
<text x="11.43" y="0" size="1.778" layer="51" font="vector">Designed by:</text>
</package>
<package name="DUMMY">
<description>NOTHING HERE!!! For when you want a symbol with no package as an option against symbols with a package.</description>
</package>
</packages>
<symbols>
<symbol name="LETTER_L">
<wire x1="0" y1="185.42" x2="248.92" y2="185.42" width="0.4064" layer="94"/>
<wire x1="248.92" y1="185.42" x2="248.92" y2="0" width="0.4064" layer="94"/>
<wire x1="0" y1="185.42" x2="0" y2="0" width="0.4064" layer="94"/>
<wire x1="0" y1="0" x2="248.92" y2="0" width="0.4064" layer="94"/>
</symbol>
<symbol name="DOCFIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.254" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.254" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.254" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.254" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.254" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.254" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.254" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.254" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.254" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.254" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.254" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.254" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.254" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94" font="vector">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94" font="vector">REV:</text>
<text x="1.524" y="17.78" size="2.54" layer="94" font="vector">TITLE:</text>
<text x="15.494" y="17.78" size="2.7432" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="2.54" y="31.75" size="1.9304" layer="94">Released under the Creative Commons</text>
<text x="2.54" y="27.94" size="1.9304" layer="94">Attribution Share-Alike 4.0 License</text>
<text x="2.54" y="24.13" size="1.9304" layer="94"> https://creativecommons.org/licenses/by-sa/4.0/</text>
<text x="1.27" y="11.43" size="2.54" layer="94">Design by:</text>
</symbol>
<symbol name="DGND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VCC_1">
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<text x="-1.016" y="3.556" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VCC_1" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="VCC_2">
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<text x="-1.016" y="3.556" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VCC_2" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FRAME-LETTER" prefix="FRAME">
<description>&lt;b&gt;Schematic Frame&lt;/b&gt;&lt;p&gt;
Standard 8.5x11 US Letter frame</description>
<gates>
<gate name="G$1" symbol="LETTER_L" x="0" y="0"/>
<gate name="V" symbol="DOCFIELD" x="147.32" y="0" addlevel="must"/>
</gates>
<devices>
<device name="" package="CREATIVE_COMMONS">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NO_PACKAGE" package="DUMMY">
<technologies>
<technology name="">
<attribute name="DESIGNER" value="Nobody" constant="no"/>
<attribute name="VERSION" value="v01" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC_1" prefix="SUPPLY">
<gates>
<gate name="G$2" symbol="VCC_1" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC_2" prefix="SUPPLY">
<gates>
<gate name="G$1" symbol="VCC_2" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Capacitors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find resistors, capacitors, inductors, test points, jumper pads, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="0603-CAP">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="0" y1="0.027940625" x2="0" y2="-0.027940625" width="0.381" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="CAP-PTH-SMALL">
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651"/>
<text x="0.508" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="0.254" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="CAP-PTH-SMALL2">
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651"/>
<text x="-1.27" y="1.905" size="0.6096" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27">&gt;Value</text>
</package>
<package name="0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="CAP-PTH-LARGE">
<wire x1="0" y1="0.635" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-4.826" y="0" drill="0.9" diameter="1.905"/>
<pad name="2" x="4.572" y="0" drill="0.9" diameter="1.905"/>
<text x="-0.762" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.016" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="GRM43D">
<wire x1="2.25" y1="1.6" x2="1.1" y2="1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="-2.25" y1="1.6" x2="-2.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.25" y1="-1.6" x2="-1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="2.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="2.25" y1="-1.6" x2="2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="1.6" x2="1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.3" y1="1.8" x2="2.3" y2="1.8" width="0.127" layer="21"/>
<wire x1="-2.3" y1="-1.8" x2="2.3" y2="-1.8" width="0.127" layer="21"/>
<smd name="A" x="1.927" y="0" dx="3.2" dy="1.65" layer="1" rot="R90"/>
<smd name="C" x="-1.927" y="0" dx="3.2" dy="1.65" layer="1" rot="R90"/>
<text x="-2" y="2" size="0.4064" layer="25">&gt;NAME</text>
<text x="0" y="-2" size="0.4064" layer="27" rot="R180">&gt;VALUE</text>
<rectangle x1="-2.2" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="2.2" y2="1.6" layer="51"/>
</package>
<package name="0402-CAP">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.4064" layer="21"/>
<smd name="1" x="-0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<smd name="2" x="0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="CAP-PTH-5MM">
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.5" y="0" drill="0.7" diameter="1.651"/>
<text x="-0.762" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.016" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="AXIAL-5MM">
<wire x1="-1.14" y1="0.762" x2="1.14" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0.762" x2="1.14" y2="0" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0" x2="1.14" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="-0.762" x2="-1.14" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="-0.762" x2="-1.14" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="0" x2="-1.14" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0" x2="1.394" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="0" x2="-1.394" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-2.5" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="2.5" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.17" size="0.4" layer="25">&gt;Name</text>
<text x="-1.032" y="-0.208" size="0.4" layer="21" ratio="15">&gt;Value</text>
</package>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="21"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2" layer="1"/>
<text x="-0.8" y="0.5" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.9" y="-0.7" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="CTZ3">
<description>CTZ3 Series land pattern for variable capacitor - CTZ3E-50C-W1-PF</description>
<wire x1="-1.6" y1="1.4" x2="-1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-2.25" x2="1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.4" x2="1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="0" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.4" x2="-1" y2="2.2" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.4" x2="1" y2="2.2" width="0.127" layer="51"/>
<wire x1="-1" y1="2.2" x2="1" y2="2.2" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-0.8" y1="0" x2="0.8" y2="0" width="0.127" layer="51"/>
<wire x1="-1.05" y1="2.25" x2="-1.7" y2="1.45" width="0.127" layer="21"/>
<wire x1="-1.7" y1="1.45" x2="-1.7" y2="-2.35" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-2.35" x2="-1.05" y2="-2.35" width="0.127" layer="21"/>
<wire x1="1.05" y1="2.25" x2="1.7" y2="1.4" width="0.127" layer="21"/>
<wire x1="1.7" y1="1.4" x2="1.7" y2="-2.35" width="0.127" layer="21"/>
<wire x1="1.7" y1="-2.35" x2="1.05" y2="-2.35" width="0.127" layer="21"/>
<smd name="+" x="0" y="2.05" dx="1.5" dy="1.2" layer="1"/>
<smd name="-" x="0" y="-2.05" dx="1.5" dy="1.2" layer="1"/>
<text x="-2" y="3" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2" y="-3.4" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="CAP-PTH-SMALL-KIT">
<description>&lt;h3&gt;CAP-PTH-SMALL-KIT&lt;/h3&gt;
Commonly used for small ceramic capacitors. Like our 0.1uF (http://www.sparkfun.com/products/8375) or 22pF caps (http://www.sparkfun.com/products/8571).&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.254" layer="21"/>
<wire x1="-2.667" y1="1.27" x2="2.667" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="1.27" x2="2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="-1.27" x2="-2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.667" y1="-1.27" x2="-2.667" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="2" x="1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<polygon width="0.127" layer="30">
<vertex x="-1.4021" y="-0.9475" curve="-90"/>
<vertex x="-2.357" y="-0.0178" curve="-90.011749"/>
<vertex x="-1.4046" y="0.9576" curve="-90"/>
<vertex x="-0.4546" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.4046" y="-0.4395" curve="-90.012891"/>
<vertex x="-1.8491" y="-0.0153" curve="-90"/>
<vertex x="-1.4046" y="0.452" curve="-90"/>
<vertex x="-0.9627" y="-0.0051" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.397" y="-0.9475" curve="-90"/>
<vertex x="0.4421" y="-0.0178" curve="-90.011749"/>
<vertex x="1.3945" y="0.9576" curve="-90"/>
<vertex x="2.3445" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.3945" y="-0.4395" curve="-90.012891"/>
<vertex x="0.95" y="-0.0153" curve="-90"/>
<vertex x="1.3945" y="0.452" curve="-90"/>
<vertex x="1.8364" y="-0.0051" curve="-90.012967"/>
</polygon>
</package>
<package name="CAP-PTH-SMALLEZ">
<description>This is the "EZ" version of the .1" spaced ceramic thru-hole cap.&lt;br&gt;
It has reduced top mask to make it harder to put the component on the wrong side of the board.</description>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651" stop="no"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651" stop="no"/>
<text x="-1.27" y="1.905" size="0.6096" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27">&gt;Value</text>
<circle x="0" y="0" radius="0.898025" width="0" layer="30"/>
<circle x="2.54" y="0" radius="0.915809375" width="0" layer="30"/>
<circle x="0" y="0" radius="0.40160625" width="0" layer="29"/>
<circle x="2.54" y="0" radius="0.40160625" width="0" layer="29"/>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="CAP-PTH-SMALL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH2" package="CAP-PTH-SMALL2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH3" package="CAP-PTH-LARGE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="GRM43D">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-CAP" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402-CAP" package="0402-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH1" package="CAP-PTH-5MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_" package="AXIAL-5MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ASMD" package="CTZ3">
<connects>
<connect gate="G$1" pin="1" pad="+"/>
<connect gate="G$1" pin="2" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="CAP-PTH-SMALL-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EZ" package="CAP-PTH-SMALLEZ">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Seeed-OPL-Resistor">
<packages>
<package name="RA1206-8">
<description>&lt;b&gt;BOURNS&lt;/b&gt; Chip Resistor Array&lt;p&gt;
Source: RS Component / BUORNS</description>
<wire x1="-1.55" y1="0.75" x2="-1" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-0.6" y1="0.75" x2="-0.2" y2="0.75" width="0.1016" layer="51"/>
<wire x1="0.2" y1="0.75" x2="0.6" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1" y1="0.75" x2="-0.6" y2="0.75" width="0.1016" layer="51" curve="180"/>
<wire x1="-0.2" y1="0.75" x2="0.2" y2="0.75" width="0.1016" layer="51" curve="180"/>
<wire x1="0.6" y1="0.75" x2="1" y2="0.75" width="0.1016" layer="51" curve="180"/>
<wire x1="1.55" y1="-0.75" x2="1" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="0.6" y1="-0.75" x2="0.2" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.2" y1="-0.75" x2="-0.6" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="1" y1="-0.75" x2="0.6" y2="-0.75" width="0.1016" layer="51" curve="180"/>
<wire x1="0.2" y1="-0.75" x2="-0.2" y2="-0.75" width="0.1016" layer="51" curve="180"/>
<wire x1="-0.6" y1="-0.75" x2="-1" y2="-0.75" width="0.1016" layer="51" curve="180"/>
<wire x1="-1.8" y1="1.5" x2="1.8" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.8" y1="1.5" x2="1.8" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.8" y1="-1.5" x2="-1.65858125" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.65858125" y1="-1.5" x2="-1.8" y2="-1.35858125" width="0.127" layer="21"/>
<wire x1="-1.8" y1="-1.35858125" x2="-1.8" y2="1.5" width="0.127" layer="21"/>
<smd name="1" x="-1.3" y="-0.85" dx="0.7" dy="1" layer="1"/>
<smd name="2" x="-0.4" y="-0.85" dx="0.5" dy="1" layer="1"/>
<smd name="3" x="0.4" y="-0.85" dx="0.5" dy="1" layer="1"/>
<smd name="4" x="1.3" y="-0.85" dx="0.7" dy="1" layer="1"/>
<smd name="5" x="1.3" y="0.85" dx="0.7" dy="1" layer="1"/>
<smd name="6" x="0.4" y="0.85" dx="0.5" dy="1" layer="1"/>
<smd name="7" x="-0.4" y="0.85" dx="0.5" dy="1" layer="1"/>
<smd name="8" x="-1.3" y="0.85" dx="0.7" dy="1" layer="1"/>
<text x="-1.905" y="1.901" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.651" y="-2.667" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
<text x="-1.27" y="0" size="0.4572" layer="33" ratio="10">&gt;NAME</text>
<rectangle x1="-1.5875" y1="-1.143" x2="1.5875" y2="1.143" layer="39"/>
</package>
<package name="R1206">
<wire x1="-2.159" y1="1.016" x2="2.159" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.159" y1="1.016" x2="2.159" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.159" y1="-1.016" x2="-2.159" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.159" y1="-1.016" x2="-2.159" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.27" y="0" dx="1.27" dy="1.524" layer="1"/>
<smd name="2" x="1.27" y="0" dx="1.27" dy="1.524" layer="1"/>
<text x="-1.905" y="1.27" size="0.889" layer="25" font="vector" ratio="11">&gt;NAME</text>
<text x="-1.905" y="-2.159" size="0.889" layer="27" font="vector" ratio="11">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.889" x2="2.032" y2="0.889" layer="39"/>
</package>
<package name="PR-D1.8*L3.3MM">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<pad name="6" x="2.54" y="0" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="5" x="-2.54" y="0" drill="0.8128" shape="octagon" rot="R90"/>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<text x="-1.905" y="1.143" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.651" y="-2.159" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="R0204_7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.27" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.905" y="-0.254" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="PR-D2.3XL6.5MM">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.207" y1="0" x2="4.191" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.953" y1="0" x2="-3.937" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.048" y1="0.889" x2="-2.794" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.048" y1="-0.889" x2="-2.794" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="3.048" y1="-1.143" x2="3.302" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="3.048" y1="1.143" x2="3.302" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.048" y1="-0.889" x2="-3.048" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="1.143" x2="-2.413" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-2.413" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="-1.143" x2="-2.413" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-2.413" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.016" x2="2.667" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.016" x2="-2.286" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.667" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="-2.286" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="3.048" y1="1.143" x2="2.667" y2="1.143" width="0.1524" layer="21"/>
<wire x1="3.048" y1="-1.143" x2="2.667" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-0.889" x2="3.302" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-4.953" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.207" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.413" y="1.397" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-2.413" y="-2.921" size="0.889" layer="27" ratio="11">&gt;VALUE</text>
<rectangle x1="3.302" y1="-0.3048" x2="4.1656" y2="0.3048" layer="21"/>
<rectangle x1="-3.9116" y1="-0.3048" x2="-3.048" y2="0.3048" layer="21"/>
</package>
<package name="R0207_12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.27" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-2.54" y="-0.4318" size="0.889" layer="27" ratio="11">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
</package>
<package name="R0207_7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 7.5 mm</description>
<wire x1="-3.81" y1="0" x2="-3.429" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0" x2="3.81" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-2.54" y="-0.4318" size="0.889" layer="27" ratio="11">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-0.3048" x2="-3.175" y2="0.3048" layer="51"/>
<rectangle x1="3.175" y1="-0.3048" x2="3.429" y2="0.3048" layer="51"/>
</package>
<package name="R0309_10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.905" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-3.175" y="-0.4318" size="0.889" layer="27" ratio="11">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="R0309_12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.778" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-2.54" y="-0.4318" size="0.889" layer="27" ratio="11">&gt;VALUE</text>
<rectangle x1="4.318" y1="-0.3048" x2="5.1816" y2="0.3048" layer="21"/>
<rectangle x1="-5.1816" y1="-0.3048" x2="-4.318" y2="0.3048" layer="21"/>
</package>
<package name="R0309_15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 15mm</description>
<wire x1="-6.604" y1="0" x2="-7.62" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="7.62" y1="0" x2="6.477" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.778" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-3.175" y="-0.4318" size="0.889" layer="27" ratio="11">&gt;VALUE</text>
<rectangle x1="-6.477" y1="-0.3175" x2="-4.318" y2="0.3175" layer="21"/>
<rectangle x1="4.318" y1="-0.3175" x2="6.477" y2="0.3175" layer="21"/>
</package>
<package name="R0309_20">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 20mm</description>
<wire x1="-9.144" y1="0" x2="-10.16" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="10.16" y1="0" x2="9.017" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-10.16" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="10.16" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.905" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-3.175" y="-0.4318" size="0.889" layer="27" ratio="11">&gt;VALUE</text>
<rectangle x1="-9.017" y1="-0.3175" x2="-4.318" y2="0.3175" layer="21"/>
<rectangle x1="4.318" y1="-0.3175" x2="9.017" y2="0.3175" layer="21"/>
</package>
<package name="R2512">
<description>2512</description>
<wire x1="-4.445" y1="1.905" x2="4.445" y2="1.905" width="0.127" layer="21"/>
<wire x1="4.445" y1="1.905" x2="4.445" y2="-1.905" width="0.127" layer="21"/>
<wire x1="4.445" y1="-1.905" x2="-4.445" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-1.905" x2="-4.445" y2="1.905" width="0.127" layer="21"/>
<smd name="1" x="-3.175" y="0" dx="3.302" dy="2.032" layer="1" rot="R90"/>
<smd name="2" x="3.175" y="0" dx="3.302" dy="2.032" layer="1" rot="R90"/>
<text x="-2.54" y="2.159" size="0.889" layer="25" font="vector" ratio="11">&gt;NAME</text>
<text x="-3.81" y="-3.048" size="0.889" layer="27" ratio="11">&gt;VALUE</text>
<text x="-1.778" y="-0.254" size="0.8128" layer="33" font="vector" ratio="10">&gt;NAME</text>
</package>
<package name="R0821_27">
<wire x1="7.48840625" y1="-3.3" x2="-7.48840625" y2="-3.3" width="0.127" layer="21"/>
<wire x1="-7.48840625" y1="-3.3" x2="-7.74075625" y2="-3.352296875" width="0.127" layer="21" curve="23.415886"/>
<wire x1="-7.74075625" y1="-3.352296875" x2="-8.76924375" y2="-3.797703125" width="0.127" layer="21"/>
<wire x1="-8.76924375" y1="-3.797703125" x2="-9.02159375" y2="-3.85" width="0.127" layer="21" curve="-23.415751"/>
<wire x1="-9.02159375" y1="-3.85" x2="-9.865" y2="-3.85" width="0.127" layer="21"/>
<wire x1="-9.865" y1="-3.85" x2="-10.5" y2="-3.215" width="0.127" layer="21" curve="-90"/>
<wire x1="-10.5" y1="-3.215" x2="-10.5" y2="3.215" width="0.127" layer="21"/>
<wire x1="-10.5" y1="3.215" x2="-9.865" y2="3.85" width="0.127" layer="21" curve="-90"/>
<wire x1="-9.865" y1="3.85" x2="-9.02159375" y2="3.85" width="0.127" layer="21"/>
<wire x1="-9.02159375" y1="3.85" x2="-8.76924375" y2="3.797703125" width="0.127" layer="21" curve="-23.415886"/>
<wire x1="-8.76924375" y1="3.797703125" x2="-7.74075625" y2="3.352296875" width="0.127" layer="21"/>
<wire x1="-7.74075625" y1="3.352296875" x2="-7.48840625" y2="3.3" width="0.127" layer="21" curve="23.415751"/>
<wire x1="-7.48840625" y1="3.3" x2="7.48840625" y2="3.3" width="0.127" layer="21"/>
<wire x1="7.48840625" y1="3.3" x2="7.74075625" y2="3.352296875" width="0.127" layer="21" curve="23.415886"/>
<wire x1="7.74075625" y1="3.352296875" x2="8.76924375" y2="3.797703125" width="0.127" layer="21"/>
<wire x1="8.76924375" y1="3.797703125" x2="9.02159375" y2="3.85" width="0.127" layer="21" curve="-23.415751"/>
<wire x1="9.02159375" y1="3.85" x2="9.865" y2="3.85" width="0.127" layer="21"/>
<wire x1="9.865" y1="3.85" x2="10.5" y2="3.215" width="0.127" layer="21" curve="-90"/>
<wire x1="10.5" y1="3.215" x2="10.5" y2="-3.215" width="0.127" layer="21"/>
<wire x1="10.5" y1="-3.215" x2="9.865" y2="-3.85" width="0.127" layer="21" curve="-90"/>
<wire x1="9.865" y1="-3.85" x2="9.02159375" y2="-3.85" width="0.127" layer="21"/>
<wire x1="9.02159375" y1="-3.85" x2="8.76924375" y2="-3.797703125" width="0.127" layer="21" curve="-23.415886"/>
<wire x1="8.76924375" y1="-3.797703125" x2="7.74075625" y2="-3.352296875" width="0.127" layer="21"/>
<wire x1="7.74075625" y1="-3.352296875" x2="7.48840625" y2="-3.3" width="0.127" layer="21" curve="23.415751"/>
<wire x1="-12.065" y1="0" x2="-10.541" y2="0" width="0.127" layer="21"/>
<pad name="P$1" x="-13.5" y="0" drill="1.2" diameter="2.2"/>
<pad name="P$2" x="13.5" y="0" drill="1.2" diameter="2.2"/>
<text x="-5.08" y="3.683" size="0.889" layer="25" ratio="11">&gt;name</text>
<text x="-5.08" y="-0.635" size="0.889" layer="27" ratio="11">&gt;value</text>
<rectangle x1="10.541" y1="-0.254" x2="12.192" y2="0.254" layer="21"/>
<rectangle x1="-12.192" y1="-0.254" x2="-10.541" y2="0.254" layer="21"/>
</package>
<package name="R0515_20.4">
<wire x1="-5.08" y1="2.032" x2="5.08" y2="2.032" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.032" x2="5.715" y2="2.413" width="0.127" layer="21"/>
<wire x1="5.715" y1="2.413" x2="7.239" y2="2.413" width="0.127" layer="21"/>
<wire x1="7.239" y1="2.413" x2="7.62" y2="2.032" width="0.127" layer="21" curve="-90"/>
<wire x1="7.62" y1="2.032" x2="7.62" y2="-2.032" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.032" x2="7.239" y2="-2.413" width="0.127" layer="21" curve="-90"/>
<wire x1="7.239" y1="-2.413" x2="5.715" y2="-2.413" width="0.127" layer="21"/>
<wire x1="5.715" y1="-2.413" x2="5.08" y2="-2.032" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.032" x2="-5.08" y2="-2.032" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.032" x2="-5.715" y2="-2.413" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-2.413" x2="-7.239" y2="-2.413" width="0.127" layer="21"/>
<wire x1="-7.239" y1="-2.413" x2="-7.62" y2="-2.032" width="0.127" layer="21" curve="-90"/>
<wire x1="-7.62" y1="-2.032" x2="-7.62" y2="2.032" width="0.127" layer="21"/>
<wire x1="-7.62" y1="2.032" x2="-7.239" y2="2.413" width="0.127" layer="21" curve="-90"/>
<wire x1="-7.239" y1="2.413" x2="-5.715" y2="2.413" width="0.127" layer="21"/>
<wire x1="-5.715" y1="2.413" x2="-5.08" y2="2.032" width="0.127" layer="21"/>
<pad name="P$1" x="-10.2362" y="0" drill="1.0414" diameter="1.905"/>
<pad name="P$2" x="10.2362" y="0" drill="1.0414" diameter="1.905"/>
<text x="-3.81" y="2.54" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-4.445" y="-0.635" size="0.889" layer="27" ratio="11">&gt;VALUE</text>
<text x="-1.905" y="-0.635" size="0.8128" layer="33" ratio="10">&gt;NAME</text>
<rectangle x1="-9.144" y1="-0.3175" x2="-7.62" y2="0.3175" layer="21"/>
<rectangle x1="7.62" y1="-0.3175" x2="9.144" y2="0.3175" layer="21"/>
</package>
<package name="R0824_26">
<wire x1="6.508515625" y1="-3.6068" x2="-6.508515625" y2="-3.6068" width="0.127" layer="21"/>
<wire x1="-6.508515625" y1="-3.6068" x2="-6.6952625" y2="-3.63488125" width="0.127" layer="21" curve="17.102957"/>
<wire x1="-6.6952625" y1="-3.63488125" x2="-7.8335375" y2="-3.98511875" width="0.127" layer="21"/>
<wire x1="-7.8335375" y1="-3.98511875" x2="-8.020284375" y2="-4.0132" width="0.127" layer="21" curve="-17.102969"/>
<wire x1="-8.020284375" y1="-4.0132" x2="-11.3538" y2="-4.0132" width="0.127" layer="21"/>
<wire x1="-11.3538" y1="-4.0132" x2="-11.9888" y2="-3.3782" width="0.127" layer="21" curve="-90"/>
<wire x1="-11.9888" y1="-3.3782" x2="-11.9888" y2="3.3782" width="0.127" layer="21"/>
<wire x1="-11.9888" y1="3.3782" x2="-11.3538" y2="4.0132" width="0.127" layer="21" curve="-90"/>
<wire x1="-11.3538" y1="4.0132" x2="-8.020284375" y2="4.0132" width="0.127" layer="21"/>
<wire x1="-8.020284375" y1="4.0132" x2="-7.8335375" y2="3.98511875" width="0.127" layer="21" curve="-17.102957"/>
<wire x1="-7.8335375" y1="3.98511875" x2="-6.6952625" y2="3.63488125" width="0.127" layer="21"/>
<wire x1="-6.6952625" y1="3.63488125" x2="-6.508515625" y2="3.6068" width="0.127" layer="21" curve="17.102969"/>
<wire x1="-6.508515625" y1="3.6068" x2="6.508515625" y2="3.6068" width="0.127" layer="21"/>
<wire x1="6.508515625" y1="3.6068" x2="6.6952625" y2="3.63488125" width="0.127" layer="21" curve="17.102957"/>
<wire x1="6.6952625" y1="3.63488125" x2="7.8335375" y2="3.98511875" width="0.127" layer="21"/>
<wire x1="7.8335375" y1="3.98511875" x2="8.020284375" y2="4.0132" width="0.127" layer="21" curve="-17.102969"/>
<wire x1="8.020284375" y1="4.0132" x2="11.3538" y2="4.0132" width="0.127" layer="21"/>
<wire x1="11.3538" y1="4.0132" x2="11.9888" y2="3.3782" width="0.127" layer="21" curve="-90"/>
<wire x1="11.9888" y1="3.3782" x2="11.9888" y2="-3.3782" width="0.127" layer="21"/>
<wire x1="11.9888" y1="-3.3782" x2="11.3538" y2="-4.0132" width="0.127" layer="21" curve="-90"/>
<wire x1="11.3538" y1="-4.0132" x2="8.020284375" y2="-4.0132" width="0.127" layer="21"/>
<wire x1="8.020284375" y1="-4.0132" x2="7.8335375" y2="-3.98511875" width="0.127" layer="21" curve="-17.102957"/>
<wire x1="7.8335375" y1="-3.98511875" x2="6.6952625" y2="-3.63488125" width="0.127" layer="21"/>
<wire x1="6.6952625" y1="-3.63488125" x2="6.508515625" y2="-3.6068" width="0.127" layer="21" curve="17.102969"/>
<pad name="1" x="-13.335" y="0" drill="1.27" diameter="2.2098"/>
<pad name="2" x="13.335" y="0" drill="1.27" diameter="2.2098"/>
<text x="-4.445" y="3.683" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-4.445" y="-0.254" size="0.889" layer="27" ratio="11">&gt;VALUE</text>
</package>
<package name="R7519_60.4">
<wire x1="-38.1" y1="9.525" x2="38.1" y2="9.525" width="0.127" layer="21"/>
<wire x1="38.1" y1="9.525" x2="38.1" y2="-9.525" width="0.127" layer="21"/>
<wire x1="38.1" y1="-9.525" x2="-38.1" y2="-9.525" width="0.127" layer="21"/>
<wire x1="-38.1" y1="-9.525" x2="-38.1" y2="9.525" width="0.127" layer="21"/>
<pad name="1" x="-30.226" y="2.54" drill="1.016" diameter="2.032" shape="long" rot="R90"/>
<pad name="2" x="-30.226" y="-2.54" drill="1.016" diameter="2.032" shape="long" rot="R90"/>
<pad name="3" x="30.226" y="2.54" drill="1.016" diameter="2.032" shape="long" rot="R90"/>
<pad name="4" x="30.226" y="-2.54" drill="1.016" diameter="2.032" shape="long" rot="R90"/>
<wire x1="-30.226" y1="1.524" x2="-30.226" y2="3.556" width="1.016" layer="46"/>
<wire x1="-30.226" y1="-3.556" x2="-30.226" y2="-1.524" width="1.016" layer="46"/>
<wire x1="30.226" y1="-3.556" x2="30.226" y2="-1.524" width="1.016" layer="46"/>
<wire x1="30.226" y1="1.524" x2="30.226" y2="3.556" width="1.016" layer="46"/>
<text x="-4.445" y="10.16" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-4.445" y="-1.778" size="0.889" layer="27" ratio="11">&gt;VALUE</text>
<rectangle x1="-38.1" y1="-9.525" x2="38.1" y2="9.525" layer="39"/>
</package>
<package name="R0402">
<wire x1="-0.4445" y1="0.762" x2="-0.3175" y2="0.889" width="0.0762" layer="21" curve="-90"/>
<wire x1="-0.3175" y1="0.889" x2="0.3175" y2="0.889" width="0.0762" layer="21"/>
<wire x1="0.3175" y1="0.889" x2="0.4445" y2="0.762" width="0.0762" layer="21" curve="-90"/>
<wire x1="0.4445" y1="0.762" x2="0.4445" y2="-0.762" width="0.0762" layer="21"/>
<wire x1="0.4445" y1="-0.762" x2="0.3175" y2="-0.889" width="0.0762" layer="21" curve="-90"/>
<wire x1="0.3175" y1="-0.889" x2="-0.3175" y2="-0.889" width="0.0762" layer="21"/>
<wire x1="-0.3175" y1="-0.889" x2="-0.4445" y2="-0.762" width="0.0762" layer="21" curve="-90"/>
<wire x1="-0.4445" y1="-0.762" x2="-0.4445" y2="0.762" width="0.0762" layer="21"/>
<smd name="1" x="0" y="0.4445" dx="0.635" dy="0.635" layer="1" roundness="50" rot="R270"/>
<smd name="2" x="0" y="-0.4445" dx="0.635" dy="0.635" layer="1" roundness="50" rot="R270"/>
<text x="0.635" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.524" y="1.27" size="0.889" layer="27" font="vector" ratio="11" rot="R270">&gt;VALUE</text>
</package>
<package name="R0603">
<wire x1="0.635" y1="1.397" x2="0.635" y2="-1.397" width="0.127" layer="21"/>
<wire x1="0.635" y1="-1.397" x2="-0.635" y2="-1.397" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-1.397" x2="-0.635" y2="1.397" width="0.127" layer="21"/>
<wire x1="-0.635" y1="1.397" x2="0.635" y2="1.397" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.762" dx="0.889" dy="0.889" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.762" dx="0.889" dy="0.889" layer="1" roundness="25" rot="R270"/>
<text x="-1.016" y="-1.905" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
</package>
<package name="R0805">
<wire x1="0.889" y1="-1.651" x2="-0.889" y2="-1.651" width="0.127" layer="21"/>
<wire x1="-0.889" y1="-1.651" x2="-0.889" y2="1.651" width="0.127" layer="21"/>
<wire x1="-0.889" y1="1.651" x2="0.889" y2="1.651" width="0.127" layer="21"/>
<wire x1="0.889" y1="1.651" x2="0.889" y2="-1.651" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.889" dx="1.016" dy="1.397" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.889" dx="1.016" dy="1.397" layer="1" roundness="25" rot="R270"/>
<text x="1.016" y="1.905" size="0.889" layer="25" font="vector" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.905" y="1.905" size="0.889" layer="27" ratio="11" rot="R270">&gt;VALUE</text>
</package>
<package name="R0201">
<rectangle x1="-0.3048" y1="-0.1524" x2="-0.1524" y2="0.1524" layer="51"/>
<rectangle x1="0.1524" y1="-0.1524" x2="0.3048" y2="0.1524" layer="51"/>
<smd name="1" x="-0.2794" y="0" dx="0.2794" dy="0.3175" layer="1" roundness="50"/>
<smd name="2" x="0.2794" y="0" dx="0.2794" dy="0.3175" layer="1" roundness="50"/>
<wire x1="-0.3048" y1="0.1524" x2="0.3048" y2="0.1524" width="0.01" layer="51"/>
<wire x1="0.3048" y1="0.1524" x2="0.3048" y2="-0.1524" width="0.01" layer="51"/>
<wire x1="0.3048" y1="-0.1524" x2="-0.3048" y2="-0.1524" width="0.01" layer="51"/>
<wire x1="-0.3048" y1="-0.1524" x2="-0.3048" y2="0.1524" width="0.01" layer="51"/>
<text x="-0.381" y="0.254" size="0.635" layer="25" ratio="11">&gt;name</text>
<text x="-0.381" y="-0.635" size="0.381" layer="27" ratio="11">&gt;value</text>
<wire x1="-0.15" y1="0.1524" x2="0.15" y2="0.1524" width="0.01" layer="21"/>
<wire x1="-0.15" y1="-0.1524" x2="0.15" y2="-0.1524" width="0.01" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="RES">
<wire x1="-1.27" y1="0.508" x2="1.27" y2="0.508" width="0.254" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.27" y2="-0.508" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.508" x2="-1.27" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-0.508" x2="-1.27" y2="0.508" width="0.254" layer="94"/>
<text x="-3.81" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-3.81" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="3.81" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RES-ARRAY" prefix="R" uservalue="yes">
<description>Resistor Array</description>
<gates>
<gate name="A" symbol="RES" x="0" y="7.62" swaplevel="4"/>
<gate name="B" symbol="RES" x="0" y="2.54" swaplevel="4"/>
<gate name="C" symbol="RES" x="0" y="-2.54" swaplevel="4"/>
<gate name="D" symbol="RES" x="0" y="-7.62" swaplevel="4"/>
</gates>
<devices>
<device name="-1206" package="RA1206-8">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="8"/>
<connect gate="B" pin="1" pad="2"/>
<connect gate="B" pin="2" pad="7"/>
<connect gate="C" pin="1" pad="3"/>
<connect gate="C" pin="2" pad="6"/>
<connect gate="D" pin="1" pad="4"/>
<connect gate="D" pin="2" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RES" prefix="R" uservalue="yes">
<gates>
<gate name="R" symbol="RES" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="-0204/5" package="PR-D1.8*L3.3MM">
<connects>
<connect gate="R" pin="1" pad="6"/>
<connect gate="R" pin="2" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0204/7" package="R0204_7">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0207/10" package="PR-D2.3XL6.5MM">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0207/12" package="R0207_12">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0207/7" package="R0207_7">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0309/10" package="R0309_10">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0309/12" package="R0309_12">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0309/15" package="R0309_15">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0309/20" package="R0309_20">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1206" package="R1206">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2512" package="R2512">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0821/27-5W" package="R0821_27">
<connects>
<connect gate="R" pin="1" pad="P$1"/>
<connect gate="R" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0515" package="R0515_20.4">
<connects>
<connect gate="R" pin="1" pad="P$1"/>
<connect gate="R" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0824/26-5W" package="R0824_26">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7519" package="R7519_60.4">
<connects>
<connect gate="R" pin="1" pad="1 2"/>
<connect gate="R" pin="2" pad="3 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0402" package="R0402">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0603" package="R0603">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0805" package="R0805">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0201" package="R0201">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Seeed-OPL-Transistor">
<packages>
<package name="SOT-23">
<description>&lt;b&gt;SOT23&lt;/b&gt;</description>
<wire x1="-0.1905" y1="-0.635" x2="0.1905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.4605" y1="-0.254" x2="1.4605" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.4605" y1="0.635" x2="0.6985" y2="0.635" width="0.127" layer="21"/>
<wire x1="-0.6985" y1="0.635" x2="-1.4605" y2="0.635" width="0.127" layer="21"/>
<wire x1="-1.4605" y1="0.635" x2="-1.4605" y2="-0.254" width="0.127" layer="21"/>
<smd name="3" x="0" y="1.016" dx="1.016" dy="1.143" layer="1"/>
<smd name="2" x="0.889" y="-1.016" dx="1.016" dy="1.143" layer="1"/>
<smd name="1" x="-0.889" y="-1.016" dx="1.016" dy="1.143" layer="1" rot="R180"/>
<text x="-1.905" y="1.905" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.27" y="-2.794" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
<rectangle x1="-1.524" y1="-1.651" x2="1.524" y2="1.651" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="MOSFET-N">
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="1.905" x2="1.5875" y2="1.905" width="0.254" layer="94"/>
<wire x1="1.5875" y1="-1.905" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="2.54" x2="-1.905" y2="1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="1.905" x2="-1.905" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0" x2="-1.905" y2="-0.635" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-2.54" x2="-1.905" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-1.905" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="1.905" x2="0" y2="1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.5875" y1="-1.905" x2="1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0" x2="1.5875" y2="0.4445" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0.4445" x2="1.5875" y2="1.905" width="0.254" layer="94"/>
<wire x1="2.2225" y1="-0.4445" x2="1.905" y2="-0.4445" width="0.254" layer="94"/>
<wire x1="1.905" y1="-0.4445" x2="1.27" y2="-0.4445" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.4445" x2="0.9525" y2="-0.4445" width="0.254" layer="94"/>
<wire x1="0.9525" y1="-0.4445" x2="1.5875" y2="0.4445" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0.4445" x2="2.2225" y2="-0.4445" width="0.254" layer="94"/>
<wire x1="2.2225" y1="0.4445" x2="1.5875" y2="0.4445" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0.4445" x2="0.9525" y2="0.4445" width="0.254" layer="94"/>
<wire x1="1.905" y1="-0.4445" x2="1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0" x2="1.27" y2="-0.4445" width="0.254" layer="94"/>
<wire x1="1.905" y1="-0.3175" x2="1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0" x2="1.27" y2="-0.3175" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0" x2="-0.9525" y2="-0.3175" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0" x2="-0.9525" y2="0.3175" width="0.254" layer="94"/>
<wire x1="-0.9525" y1="-0.3175" x2="-0.9525" y2="0.3175" width="0.254" layer="94"/>
<wire x1="-0.9525" y1="0.3175" x2="-1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="-1.5875" y1="0" x2="-0.9525" y2="-0.3175" width="0.254" layer="94"/>
<wire x1="-0.9525" y1="-0.3175" x2="-1.397" y2="0.127" width="0.254" layer="94"/>
<circle x="0" y="1.905" radius="0.254" width="0.254" layer="94"/>
<circle x="0" y="-1.905" radius="0.254" width="0.254" layer="94"/>
<text x="-3.81" y="2.54" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="0" y="2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<text x="-0.635" y="2.2225" size="0.8128" layer="93">D</text>
<text x="-0.635" y="-3.175" size="0.8128" layer="93">S</text>
<text x="-3.4925" y="0" size="0.8128" layer="93">G</text>
<pin name="G" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="D" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MOSFET-N" prefix="Q">
<gates>
<gate name="Q" symbol="MOSFET-N" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT-23">
<connects>
<connect gate="Q" pin="D" pad="3"/>
<connect gate="Q" pin="G" pad="1"/>
<connect gate="Q" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Seeed-OPL-Diode">
<packages>
<package name="SOD-123">
<description>&lt;b&gt;Diode&lt;/b&gt;</description>
<wire x1="-1.143" y1="-0.635" x2="-1.143" y2="0.635" width="0.127" layer="51"/>
<wire x1="1.143" y1="0.635" x2="1.143" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-1.143" y1="0.889" x2="1.143" y2="0.889" width="0.127" layer="21"/>
<wire x1="1.143" y1="-0.889" x2="-1.143" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="0" x2="-0.381" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.381" y1="-0.508" x2="-0.381" y2="-0.381" width="0.127" layer="21"/>
<wire x1="-0.381" y1="-0.381" x2="-0.381" y2="0.635" width="0.127" layer="21"/>
<wire x1="-0.381" y1="0.635" x2="0.381" y2="0" width="0.127" layer="21"/>
<wire x1="0.381" y1="0" x2="-0.381" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.381" y1="-0.508" x2="-0.254" y2="0.508" width="0.127" layer="21"/>
<wire x1="-0.254" y1="0.508" x2="0.254" y2="0" width="0.127" layer="21"/>
<wire x1="0.254" y1="0" x2="-0.381" y2="-0.381" width="0.127" layer="21"/>
<wire x1="-0.381" y1="-0.381" x2="-0.254" y2="0.381" width="0.127" layer="21"/>
<wire x1="-0.254" y1="0.381" x2="0.127" y2="0" width="0.127" layer="21"/>
<wire x1="0.127" y1="0" x2="-0.254" y2="-0.254" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-0.254" x2="-0.254" y2="-0.127" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.254" width="0.127" layer="21"/>
<wire x1="-0.254" y1="0.254" x2="-0.127" y2="0.127" width="0.127" layer="21"/>
<wire x1="-0.127" y1="0.127" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="-0.254" y2="-0.127" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-0.127" x2="-0.127" y2="0.127" width="0.127" layer="21"/>
<wire x1="-0.127" y1="0.127" x2="-0.127" y2="0" width="0.127" layer="21"/>
<smd name="-" x="1.651" y="0" dx="1.397" dy="1.397" layer="1" rot="R180"/>
<smd name="+" x="-1.651" y="0" dx="1.397" dy="1.397" layer="1" rot="R180"/>
<text x="-1.905" y="1.143" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.905" y="-2.032" size="0.889" layer="27" font="vector" ratio="11">&gt;VALUE</text>
<text x="-0.889" y="-0.635" size="0.381" layer="33" ratio="10">&gt;NAME</text>
<rectangle x1="1.2" y1="-0.4" x2="1.95" y2="0.45" layer="51" rot="R180"/>
<rectangle x1="-1.95" y1="-0.4" x2="-1.2" y2="0.45" layer="51" rot="R180"/>
</package>
</packages>
<symbols>
<symbol name="DIODE*-1">
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<text x="-3.81" y="2.54" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-3.81" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="+" x="-3.81" y="0" visible="off" length="short" direction="pas"/>
<pin name="-" x="3.81" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="1.27" y1="2.54" x2="0.635" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="1.905" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.905" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.905" y1="-2.54" x2="1.905" y2="-1.905" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SMD-DIODE-SCHOTTKY-20V-1A(SOD-123)" prefix="D" uservalue="yes">
<description>304020034</description>
<gates>
<gate name="G$1" symbol="DIODE*-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOD-123">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="B5819W"/>
<attribute name="VALUE" value="20V-1A"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="adafruit">
<packages>
<package name="BEAGLEBONE_SHIELD">
<wire x1="54.61" y1="73.66" x2="54.61" y2="14.605" width="0.127" layer="20"/>
<wire x1="54.61" y1="14.605" x2="54.61" y2="6.35" width="0.127" layer="51" style="shortdash"/>
<wire x1="12.7" y1="86.36" x2="0" y2="73.66" width="0.127" layer="20" curve="90"/>
<wire x1="54.61" y1="73.66" x2="41.91" y2="86.36" width="0.127" layer="20" curve="90"/>
<wire x1="12.7" y1="86.36" x2="41.91" y2="86.36" width="0.127" layer="20"/>
<wire x1="5.08" y1="17.78" x2="5.715" y2="18.415" width="0.1524" layer="21"/>
<wire x1="5.715" y1="18.415" x2="5.715" y2="19.685" width="0.1524" layer="21"/>
<wire x1="5.715" y1="19.685" x2="5.08" y2="20.32" width="0.1524" layer="21"/>
<wire x1="5.08" y1="20.32" x2="5.715" y2="20.955" width="0.1524" layer="21"/>
<wire x1="5.715" y1="20.955" x2="5.715" y2="22.225" width="0.1524" layer="21"/>
<wire x1="5.715" y1="22.225" x2="5.08" y2="22.86" width="0.1524" layer="21"/>
<wire x1="5.08" y1="22.86" x2="5.715" y2="23.495" width="0.1524" layer="21"/>
<wire x1="5.715" y1="23.495" x2="5.715" y2="24.765" width="0.1524" layer="21"/>
<wire x1="5.715" y1="24.765" x2="5.08" y2="25.4" width="0.1524" layer="21"/>
<wire x1="5.08" y1="25.4" x2="5.715" y2="26.035" width="0.1524" layer="21"/>
<wire x1="5.715" y1="26.035" x2="5.715" y2="27.305" width="0.1524" layer="21"/>
<wire x1="5.715" y1="27.305" x2="5.08" y2="27.94" width="0.1524" layer="21"/>
<wire x1="5.08" y1="27.94" x2="5.715" y2="28.575" width="0.1524" layer="21"/>
<wire x1="5.715" y1="28.575" x2="5.715" y2="29.845" width="0.1524" layer="21"/>
<wire x1="5.715" y1="29.845" x2="5.08" y2="30.48" width="0.1524" layer="21"/>
<wire x1="5.08" y1="30.48" x2="5.715" y2="31.115" width="0.1524" layer="21"/>
<wire x1="5.715" y1="31.115" x2="5.715" y2="32.385" width="0.1524" layer="21"/>
<wire x1="5.715" y1="32.385" x2="5.08" y2="33.02" width="0.1524" layer="21"/>
<wire x1="5.08" y1="17.78" x2="1.27" y2="17.78" width="0.1524" layer="21"/>
<wire x1="1.27" y1="17.78" x2="0.635" y2="18.415" width="0.1524" layer="21"/>
<wire x1="0.635" y1="18.415" x2="0.635" y2="19.685" width="0.1524" layer="21"/>
<wire x1="0.635" y1="19.685" x2="1.27" y2="20.32" width="0.1524" layer="21"/>
<wire x1="1.27" y1="20.32" x2="0.635" y2="20.955" width="0.1524" layer="21"/>
<wire x1="0.635" y1="20.955" x2="0.635" y2="22.225" width="0.1524" layer="21"/>
<wire x1="0.635" y1="22.225" x2="1.27" y2="22.86" width="0.1524" layer="21"/>
<wire x1="1.27" y1="22.86" x2="0.635" y2="23.495" width="0.1524" layer="21"/>
<wire x1="0.635" y1="23.495" x2="0.635" y2="24.765" width="0.1524" layer="21"/>
<wire x1="0.635" y1="24.765" x2="1.27" y2="25.4" width="0.1524" layer="21"/>
<wire x1="1.27" y1="25.4" x2="0.635" y2="26.035" width="0.1524" layer="21"/>
<wire x1="0.635" y1="26.035" x2="0.635" y2="27.305" width="0.1524" layer="21"/>
<wire x1="0.635" y1="27.305" x2="1.27" y2="27.94" width="0.1524" layer="21"/>
<wire x1="1.27" y1="27.94" x2="0.635" y2="28.575" width="0.1524" layer="21"/>
<wire x1="0.635" y1="28.575" x2="0.635" y2="29.845" width="0.1524" layer="21"/>
<wire x1="0.635" y1="29.845" x2="1.27" y2="30.48" width="0.1524" layer="21"/>
<wire x1="1.27" y1="30.48" x2="0.635" y2="31.115" width="0.1524" layer="21"/>
<wire x1="0.635" y1="31.115" x2="0.635" y2="32.385" width="0.1524" layer="21"/>
<wire x1="0.635" y1="32.385" x2="1.27" y2="33.02" width="0.1524" layer="21"/>
<wire x1="1.27" y1="33.02" x2="0.635" y2="33.655" width="0.1524" layer="21"/>
<wire x1="0.635" y1="33.655" x2="0.635" y2="34.925" width="0.1524" layer="21"/>
<wire x1="0.635" y1="34.925" x2="1.27" y2="35.56" width="0.1524" layer="21"/>
<wire x1="1.27" y1="35.56" x2="0.635" y2="36.195" width="0.1524" layer="21"/>
<wire x1="0.635" y1="36.195" x2="0.635" y2="37.465" width="0.1524" layer="21"/>
<wire x1="0.635" y1="37.465" x2="1.27" y2="38.1" width="0.1524" layer="21"/>
<wire x1="1.27" y1="38.1" x2="0.635" y2="38.735" width="0.1524" layer="21"/>
<wire x1="0.635" y1="38.735" x2="0.635" y2="40.005" width="0.1524" layer="21"/>
<wire x1="0.635" y1="40.005" x2="1.27" y2="40.64" width="0.1524" layer="21"/>
<wire x1="1.27" y1="40.64" x2="0.635" y2="41.275" width="0.1524" layer="21"/>
<wire x1="0.635" y1="41.275" x2="0.635" y2="42.545" width="0.1524" layer="21"/>
<wire x1="0.635" y1="42.545" x2="1.27" y2="43.18" width="0.1524" layer="21"/>
<wire x1="1.27" y1="43.18" x2="0.635" y2="43.815" width="0.1524" layer="21"/>
<wire x1="0.635" y1="43.815" x2="0.635" y2="45.085" width="0.1524" layer="21"/>
<wire x1="0.635" y1="45.085" x2="1.27" y2="45.72" width="0.1524" layer="21"/>
<wire x1="1.27" y1="45.72" x2="0.635" y2="46.355" width="0.1524" layer="21"/>
<wire x1="0.635" y1="46.355" x2="0.635" y2="47.625" width="0.1524" layer="21"/>
<wire x1="1.27" y1="48.26" x2="0.635" y2="47.625" width="0.1524" layer="21"/>
<wire x1="1.27" y1="48.26" x2="0.635" y2="48.895" width="0.1524" layer="21"/>
<wire x1="0.635" y1="50.165" x2="0.635" y2="48.895" width="0.1524" layer="21"/>
<wire x1="0.635" y1="50.165" x2="1.27" y2="50.8" width="0.1524" layer="21"/>
<wire x1="1.27" y1="50.8" x2="0.635" y2="51.435" width="0.1524" layer="21"/>
<wire x1="0.635" y1="52.705" x2="0.635" y2="51.435" width="0.1524" layer="21"/>
<wire x1="0.635" y1="52.705" x2="1.27" y2="53.34" width="0.1524" layer="21"/>
<wire x1="1.27" y1="53.34" x2="0.635" y2="53.975" width="0.1524" layer="21"/>
<wire x1="0.635" y1="55.245" x2="0.635" y2="53.975" width="0.1524" layer="21"/>
<wire x1="0.635" y1="55.245" x2="1.27" y2="55.88" width="0.1524" layer="21"/>
<wire x1="1.27" y1="55.88" x2="0.635" y2="56.515" width="0.1524" layer="21"/>
<wire x1="0.635" y1="57.785" x2="0.635" y2="56.515" width="0.1524" layer="21"/>
<wire x1="0.635" y1="57.785" x2="1.27" y2="58.42" width="0.1524" layer="21"/>
<wire x1="5.08" y1="58.42" x2="5.715" y2="57.785" width="0.1524" layer="21"/>
<wire x1="5.715" y1="56.515" x2="5.715" y2="57.785" width="0.1524" layer="21"/>
<wire x1="5.715" y1="56.515" x2="5.08" y2="55.88" width="0.1524" layer="21"/>
<wire x1="5.08" y1="55.88" x2="5.715" y2="55.245" width="0.1524" layer="21"/>
<wire x1="5.715" y1="53.975" x2="5.715" y2="55.245" width="0.1524" layer="21"/>
<wire x1="5.715" y1="53.975" x2="5.08" y2="53.34" width="0.1524" layer="21"/>
<wire x1="5.08" y1="53.34" x2="5.715" y2="52.705" width="0.1524" layer="21"/>
<wire x1="5.715" y1="52.705" x2="5.715" y2="51.435" width="0.1524" layer="21"/>
<wire x1="5.08" y1="50.8" x2="5.715" y2="51.435" width="0.1524" layer="21"/>
<wire x1="5.08" y1="50.8" x2="5.715" y2="50.165" width="0.1524" layer="21"/>
<wire x1="5.715" y1="50.165" x2="5.715" y2="48.895" width="0.1524" layer="21"/>
<wire x1="5.08" y1="48.26" x2="5.715" y2="48.895" width="0.1524" layer="21"/>
<wire x1="5.08" y1="48.26" x2="5.715" y2="47.625" width="0.1524" layer="21"/>
<wire x1="5.715" y1="47.625" x2="5.715" y2="46.355" width="0.1524" layer="21"/>
<wire x1="5.08" y1="45.72" x2="5.715" y2="46.355" width="0.1524" layer="21"/>
<wire x1="5.08" y1="45.72" x2="5.715" y2="45.085" width="0.1524" layer="21"/>
<wire x1="5.715" y1="45.085" x2="5.715" y2="43.815" width="0.1524" layer="21"/>
<wire x1="5.08" y1="43.18" x2="5.715" y2="43.815" width="0.1524" layer="21"/>
<wire x1="5.08" y1="43.18" x2="5.715" y2="42.545" width="0.1524" layer="21"/>
<wire x1="5.715" y1="42.545" x2="5.715" y2="41.275" width="0.1524" layer="21"/>
<wire x1="5.08" y1="40.64" x2="5.715" y2="41.275" width="0.1524" layer="21"/>
<wire x1="5.08" y1="40.64" x2="5.715" y2="40.005" width="0.1524" layer="21"/>
<wire x1="5.715" y1="40.005" x2="5.715" y2="38.735" width="0.1524" layer="21"/>
<wire x1="5.08" y1="38.1" x2="5.715" y2="38.735" width="0.1524" layer="21"/>
<wire x1="5.08" y1="38.1" x2="5.715" y2="37.465" width="0.1524" layer="21"/>
<wire x1="5.715" y1="37.465" x2="5.715" y2="36.195" width="0.1524" layer="21"/>
<wire x1="5.08" y1="35.56" x2="5.715" y2="36.195" width="0.1524" layer="21"/>
<wire x1="5.08" y1="35.56" x2="5.715" y2="34.925" width="0.1524" layer="21"/>
<wire x1="5.715" y1="34.925" x2="5.715" y2="33.655" width="0.1524" layer="21"/>
<wire x1="5.08" y1="33.02" x2="5.715" y2="33.655" width="0.1524" layer="21"/>
<wire x1="1.27" y1="20.32" x2="5.08" y2="20.32" width="0.1524" layer="21"/>
<wire x1="1.27" y1="22.86" x2="5.08" y2="22.86" width="0.1524" layer="21"/>
<wire x1="1.27" y1="25.4" x2="5.08" y2="25.4" width="0.1524" layer="21"/>
<wire x1="1.27" y1="27.94" x2="5.08" y2="27.94" width="0.1524" layer="21"/>
<wire x1="1.27" y1="30.48" x2="5.08" y2="30.48" width="0.1524" layer="21"/>
<wire x1="1.27" y1="33.02" x2="5.08" y2="33.02" width="0.1524" layer="21"/>
<wire x1="1.27" y1="35.56" x2="5.08" y2="35.56" width="0.1524" layer="21"/>
<wire x1="1.27" y1="38.1" x2="5.08" y2="38.1" width="0.1524" layer="21"/>
<wire x1="1.27" y1="40.64" x2="5.08" y2="40.64" width="0.1524" layer="21"/>
<wire x1="1.27" y1="43.18" x2="5.08" y2="43.18" width="0.1524" layer="21"/>
<wire x1="1.27" y1="45.72" x2="5.08" y2="45.72" width="0.1524" layer="21"/>
<wire x1="1.27" y1="48.26" x2="5.08" y2="48.26" width="0.1524" layer="21"/>
<wire x1="1.27" y1="50.8" x2="5.08" y2="50.8" width="0.1524" layer="21"/>
<wire x1="1.27" y1="53.34" x2="5.08" y2="53.34" width="0.1524" layer="21"/>
<wire x1="1.27" y1="55.88" x2="5.08" y2="55.88" width="0.1524" layer="21"/>
<wire x1="1.27" y1="58.42" x2="5.08" y2="58.42" width="0.1524" layer="21"/>
<wire x1="1.27" y1="58.42" x2="0.635" y2="59.055" width="0.1524" layer="21"/>
<wire x1="0.635" y1="60.325" x2="0.635" y2="59.055" width="0.1524" layer="21"/>
<wire x1="0.635" y1="60.325" x2="1.27" y2="60.96" width="0.1524" layer="21"/>
<wire x1="5.08" y1="60.96" x2="5.715" y2="60.325" width="0.1524" layer="21"/>
<wire x1="5.715" y1="59.055" x2="5.715" y2="60.325" width="0.1524" layer="21"/>
<wire x1="5.715" y1="59.055" x2="5.08" y2="58.42" width="0.1524" layer="21"/>
<wire x1="1.27" y1="60.96" x2="5.08" y2="60.96" width="0.1524" layer="21"/>
<wire x1="1.27" y1="60.96" x2="0.635" y2="61.595" width="0.1524" layer="21"/>
<wire x1="0.635" y1="62.865" x2="0.635" y2="61.595" width="0.1524" layer="21"/>
<wire x1="0.635" y1="62.865" x2="1.27" y2="63.5" width="0.1524" layer="21"/>
<wire x1="5.08" y1="63.5" x2="5.715" y2="62.865" width="0.1524" layer="21"/>
<wire x1="5.715" y1="61.595" x2="5.715" y2="62.865" width="0.1524" layer="21"/>
<wire x1="5.715" y1="61.595" x2="5.08" y2="60.96" width="0.1524" layer="21"/>
<wire x1="1.27" y1="63.5" x2="5.08" y2="63.5" width="0.1524" layer="21"/>
<wire x1="1.27" y1="63.5" x2="0.635" y2="64.135" width="0.1524" layer="21"/>
<wire x1="0.635" y1="65.405" x2="0.635" y2="64.135" width="0.1524" layer="21"/>
<wire x1="0.635" y1="65.405" x2="1.27" y2="66.04" width="0.1524" layer="21"/>
<wire x1="5.08" y1="66.04" x2="5.715" y2="65.405" width="0.1524" layer="21"/>
<wire x1="5.715" y1="64.135" x2="5.715" y2="65.405" width="0.1524" layer="21"/>
<wire x1="5.715" y1="64.135" x2="5.08" y2="63.5" width="0.1524" layer="21"/>
<wire x1="1.27" y1="66.04" x2="5.08" y2="66.04" width="0.1524" layer="21"/>
<wire x1="1.27" y1="66.04" x2="0.635" y2="66.675" width="0.1524" layer="21"/>
<wire x1="0.635" y1="67.945" x2="0.635" y2="66.675" width="0.1524" layer="21"/>
<wire x1="0.635" y1="67.945" x2="1.27" y2="68.58" width="0.1524" layer="21"/>
<wire x1="5.08" y1="68.58" x2="5.715" y2="67.945" width="0.1524" layer="21"/>
<wire x1="5.715" y1="66.675" x2="5.715" y2="67.945" width="0.1524" layer="21"/>
<wire x1="5.715" y1="66.675" x2="5.08" y2="66.04" width="0.1524" layer="21"/>
<wire x1="1.27" y1="68.58" x2="5.08" y2="68.58" width="0.1524" layer="21"/>
<wire x1="1.27" y1="68.58" x2="0.635" y2="69.215" width="0.1524" layer="21"/>
<wire x1="0.635" y1="70.485" x2="0.635" y2="69.215" width="0.1524" layer="21"/>
<wire x1="0.635" y1="70.485" x2="1.27" y2="71.12" width="0.1524" layer="21"/>
<wire x1="5.08" y1="71.12" x2="5.715" y2="70.485" width="0.1524" layer="21"/>
<wire x1="5.715" y1="69.215" x2="5.715" y2="70.485" width="0.1524" layer="21"/>
<wire x1="5.715" y1="69.215" x2="5.08" y2="68.58" width="0.1524" layer="21"/>
<wire x1="1.27" y1="71.12" x2="5.08" y2="71.12" width="0.1524" layer="21"/>
<wire x1="1.27" y1="71.12" x2="0.635" y2="71.755" width="0.1524" layer="21"/>
<wire x1="0.635" y1="73.025" x2="0.635" y2="71.755" width="0.1524" layer="21"/>
<wire x1="0.635" y1="73.025" x2="1.27" y2="73.66" width="0.1524" layer="21"/>
<wire x1="5.08" y1="73.66" x2="5.715" y2="73.025" width="0.1524" layer="21"/>
<wire x1="5.715" y1="71.755" x2="5.715" y2="73.025" width="0.1524" layer="21"/>
<wire x1="5.715" y1="71.755" x2="5.08" y2="71.12" width="0.1524" layer="21"/>
<wire x1="1.27" y1="73.66" x2="5.08" y2="73.66" width="0.1524" layer="21"/>
<wire x1="1.27" y1="73.66" x2="0.635" y2="74.295" width="0.1524" layer="21"/>
<wire x1="0.635" y1="75.565" x2="0.635" y2="74.295" width="0.1524" layer="21"/>
<wire x1="0.635" y1="75.565" x2="1.27" y2="76.2" width="0.1524" layer="21"/>
<wire x1="5.08" y1="76.2" x2="5.715" y2="75.565" width="0.1524" layer="21"/>
<wire x1="5.715" y1="74.295" x2="5.715" y2="75.565" width="0.1524" layer="21"/>
<wire x1="5.715" y1="74.295" x2="5.08" y2="73.66" width="0.1524" layer="21"/>
<wire x1="1.27" y1="76.2" x2="5.08" y2="76.2" width="0.1524" layer="21"/>
<wire x1="53.34" y1="17.78" x2="53.975" y2="18.415" width="0.1524" layer="21"/>
<wire x1="53.975" y1="18.415" x2="53.975" y2="19.685" width="0.1524" layer="21"/>
<wire x1="53.975" y1="19.685" x2="53.34" y2="20.32" width="0.1524" layer="21"/>
<wire x1="53.34" y1="20.32" x2="53.975" y2="20.955" width="0.1524" layer="21"/>
<wire x1="53.975" y1="20.955" x2="53.975" y2="22.225" width="0.1524" layer="21"/>
<wire x1="53.975" y1="22.225" x2="53.34" y2="22.86" width="0.1524" layer="21"/>
<wire x1="53.34" y1="22.86" x2="53.975" y2="23.495" width="0.1524" layer="21"/>
<wire x1="53.975" y1="23.495" x2="53.975" y2="24.765" width="0.1524" layer="21"/>
<wire x1="53.975" y1="24.765" x2="53.34" y2="25.4" width="0.1524" layer="21"/>
<wire x1="53.34" y1="25.4" x2="53.975" y2="26.035" width="0.1524" layer="21"/>
<wire x1="53.975" y1="26.035" x2="53.975" y2="27.305" width="0.1524" layer="21"/>
<wire x1="53.975" y1="27.305" x2="53.34" y2="27.94" width="0.1524" layer="21"/>
<wire x1="53.34" y1="27.94" x2="53.975" y2="28.575" width="0.1524" layer="21"/>
<wire x1="53.975" y1="28.575" x2="53.975" y2="29.845" width="0.1524" layer="21"/>
<wire x1="53.975" y1="29.845" x2="53.34" y2="30.48" width="0.1524" layer="21"/>
<wire x1="53.34" y1="30.48" x2="53.975" y2="31.115" width="0.1524" layer="21"/>
<wire x1="53.975" y1="31.115" x2="53.975" y2="32.385" width="0.1524" layer="21"/>
<wire x1="53.975" y1="32.385" x2="53.34" y2="33.02" width="0.1524" layer="21"/>
<wire x1="53.34" y1="17.78" x2="49.53" y2="17.78" width="0.1524" layer="21"/>
<wire x1="49.53" y1="17.78" x2="48.895" y2="18.415" width="0.1524" layer="21"/>
<wire x1="48.895" y1="18.415" x2="48.895" y2="19.685" width="0.1524" layer="21"/>
<wire x1="48.895" y1="19.685" x2="49.53" y2="20.32" width="0.1524" layer="21"/>
<wire x1="49.53" y1="20.32" x2="48.895" y2="20.955" width="0.1524" layer="21"/>
<wire x1="48.895" y1="20.955" x2="48.895" y2="22.225" width="0.1524" layer="21"/>
<wire x1="48.895" y1="22.225" x2="49.53" y2="22.86" width="0.1524" layer="21"/>
<wire x1="49.53" y1="22.86" x2="48.895" y2="23.495" width="0.1524" layer="21"/>
<wire x1="48.895" y1="23.495" x2="48.895" y2="24.765" width="0.1524" layer="21"/>
<wire x1="48.895" y1="24.765" x2="49.53" y2="25.4" width="0.1524" layer="21"/>
<wire x1="49.53" y1="25.4" x2="48.895" y2="26.035" width="0.1524" layer="21"/>
<wire x1="48.895" y1="26.035" x2="48.895" y2="27.305" width="0.1524" layer="21"/>
<wire x1="48.895" y1="27.305" x2="49.53" y2="27.94" width="0.1524" layer="21"/>
<wire x1="49.53" y1="27.94" x2="48.895" y2="28.575" width="0.1524" layer="21"/>
<wire x1="48.895" y1="28.575" x2="48.895" y2="29.845" width="0.1524" layer="21"/>
<wire x1="48.895" y1="29.845" x2="49.53" y2="30.48" width="0.1524" layer="21"/>
<wire x1="49.53" y1="30.48" x2="48.895" y2="31.115" width="0.1524" layer="21"/>
<wire x1="48.895" y1="31.115" x2="48.895" y2="32.385" width="0.1524" layer="21"/>
<wire x1="48.895" y1="32.385" x2="49.53" y2="33.02" width="0.1524" layer="21"/>
<wire x1="49.53" y1="33.02" x2="48.895" y2="33.655" width="0.1524" layer="21"/>
<wire x1="48.895" y1="33.655" x2="48.895" y2="34.925" width="0.1524" layer="21"/>
<wire x1="48.895" y1="34.925" x2="49.53" y2="35.56" width="0.1524" layer="21"/>
<wire x1="49.53" y1="35.56" x2="48.895" y2="36.195" width="0.1524" layer="21"/>
<wire x1="48.895" y1="36.195" x2="48.895" y2="37.465" width="0.1524" layer="21"/>
<wire x1="48.895" y1="37.465" x2="49.53" y2="38.1" width="0.1524" layer="21"/>
<wire x1="49.53" y1="38.1" x2="48.895" y2="38.735" width="0.1524" layer="21"/>
<wire x1="48.895" y1="38.735" x2="48.895" y2="40.005" width="0.1524" layer="21"/>
<wire x1="48.895" y1="40.005" x2="49.53" y2="40.64" width="0.1524" layer="21"/>
<wire x1="49.53" y1="40.64" x2="48.895" y2="41.275" width="0.1524" layer="21"/>
<wire x1="48.895" y1="41.275" x2="48.895" y2="42.545" width="0.1524" layer="21"/>
<wire x1="48.895" y1="42.545" x2="49.53" y2="43.18" width="0.1524" layer="21"/>
<wire x1="49.53" y1="43.18" x2="48.895" y2="43.815" width="0.1524" layer="21"/>
<wire x1="48.895" y1="43.815" x2="48.895" y2="45.085" width="0.1524" layer="21"/>
<wire x1="48.895" y1="45.085" x2="49.53" y2="45.72" width="0.1524" layer="21"/>
<wire x1="49.53" y1="45.72" x2="48.895" y2="46.355" width="0.1524" layer="21"/>
<wire x1="48.895" y1="46.355" x2="48.895" y2="47.625" width="0.1524" layer="21"/>
<wire x1="49.53" y1="48.26" x2="48.895" y2="47.625" width="0.1524" layer="21"/>
<wire x1="49.53" y1="48.26" x2="48.895" y2="48.895" width="0.1524" layer="21"/>
<wire x1="48.895" y1="50.165" x2="48.895" y2="48.895" width="0.1524" layer="21"/>
<wire x1="48.895" y1="50.165" x2="49.53" y2="50.8" width="0.1524" layer="21"/>
<wire x1="49.53" y1="50.8" x2="48.895" y2="51.435" width="0.1524" layer="21"/>
<wire x1="48.895" y1="52.705" x2="48.895" y2="51.435" width="0.1524" layer="21"/>
<wire x1="48.895" y1="52.705" x2="49.53" y2="53.34" width="0.1524" layer="21"/>
<wire x1="49.53" y1="53.34" x2="48.895" y2="53.975" width="0.1524" layer="21"/>
<wire x1="48.895" y1="55.245" x2="48.895" y2="53.975" width="0.1524" layer="21"/>
<wire x1="48.895" y1="55.245" x2="49.53" y2="55.88" width="0.1524" layer="21"/>
<wire x1="49.53" y1="55.88" x2="48.895" y2="56.515" width="0.1524" layer="21"/>
<wire x1="48.895" y1="57.785" x2="48.895" y2="56.515" width="0.1524" layer="21"/>
<wire x1="48.895" y1="57.785" x2="49.53" y2="58.42" width="0.1524" layer="21"/>
<wire x1="53.34" y1="58.42" x2="53.975" y2="57.785" width="0.1524" layer="21"/>
<wire x1="53.975" y1="56.515" x2="53.975" y2="57.785" width="0.1524" layer="21"/>
<wire x1="53.975" y1="56.515" x2="53.34" y2="55.88" width="0.1524" layer="21"/>
<wire x1="53.34" y1="55.88" x2="53.975" y2="55.245" width="0.1524" layer="21"/>
<wire x1="53.975" y1="53.975" x2="53.975" y2="55.245" width="0.1524" layer="21"/>
<wire x1="53.975" y1="53.975" x2="53.34" y2="53.34" width="0.1524" layer="21"/>
<wire x1="53.34" y1="53.34" x2="53.975" y2="52.705" width="0.1524" layer="21"/>
<wire x1="53.975" y1="52.705" x2="53.975" y2="51.435" width="0.1524" layer="21"/>
<wire x1="53.34" y1="50.8" x2="53.975" y2="51.435" width="0.1524" layer="21"/>
<wire x1="53.34" y1="50.8" x2="53.975" y2="50.165" width="0.1524" layer="21"/>
<wire x1="53.975" y1="50.165" x2="53.975" y2="48.895" width="0.1524" layer="21"/>
<wire x1="53.34" y1="48.26" x2="53.975" y2="48.895" width="0.1524" layer="21"/>
<wire x1="53.34" y1="48.26" x2="53.975" y2="47.625" width="0.1524" layer="21"/>
<wire x1="53.975" y1="47.625" x2="53.975" y2="46.355" width="0.1524" layer="21"/>
<wire x1="53.34" y1="45.72" x2="53.975" y2="46.355" width="0.1524" layer="21"/>
<wire x1="53.34" y1="45.72" x2="53.975" y2="45.085" width="0.1524" layer="21"/>
<wire x1="53.975" y1="45.085" x2="53.975" y2="43.815" width="0.1524" layer="21"/>
<wire x1="53.34" y1="43.18" x2="53.975" y2="43.815" width="0.1524" layer="21"/>
<wire x1="53.34" y1="43.18" x2="53.975" y2="42.545" width="0.1524" layer="21"/>
<wire x1="53.975" y1="42.545" x2="53.975" y2="41.275" width="0.1524" layer="21"/>
<wire x1="53.34" y1="40.64" x2="53.975" y2="41.275" width="0.1524" layer="21"/>
<wire x1="53.34" y1="40.64" x2="53.975" y2="40.005" width="0.1524" layer="21"/>
<wire x1="53.975" y1="40.005" x2="53.975" y2="38.735" width="0.1524" layer="21"/>
<wire x1="53.34" y1="38.1" x2="53.975" y2="38.735" width="0.1524" layer="21"/>
<wire x1="53.34" y1="38.1" x2="53.975" y2="37.465" width="0.1524" layer="21"/>
<wire x1="53.975" y1="37.465" x2="53.975" y2="36.195" width="0.1524" layer="21"/>
<wire x1="53.34" y1="35.56" x2="53.975" y2="36.195" width="0.1524" layer="21"/>
<wire x1="53.34" y1="35.56" x2="53.975" y2="34.925" width="0.1524" layer="21"/>
<wire x1="53.975" y1="34.925" x2="53.975" y2="33.655" width="0.1524" layer="21"/>
<wire x1="53.34" y1="33.02" x2="53.975" y2="33.655" width="0.1524" layer="21"/>
<wire x1="49.53" y1="20.32" x2="53.34" y2="20.32" width="0.1524" layer="21"/>
<wire x1="49.53" y1="22.86" x2="53.34" y2="22.86" width="0.1524" layer="21"/>
<wire x1="49.53" y1="25.4" x2="53.34" y2="25.4" width="0.1524" layer="21"/>
<wire x1="49.53" y1="27.94" x2="53.34" y2="27.94" width="0.1524" layer="21"/>
<wire x1="49.53" y1="30.48" x2="53.34" y2="30.48" width="0.1524" layer="21"/>
<wire x1="49.53" y1="33.02" x2="53.34" y2="33.02" width="0.1524" layer="21"/>
<wire x1="49.53" y1="35.56" x2="53.34" y2="35.56" width="0.1524" layer="21"/>
<wire x1="49.53" y1="38.1" x2="53.34" y2="38.1" width="0.1524" layer="21"/>
<wire x1="49.53" y1="40.64" x2="53.34" y2="40.64" width="0.1524" layer="21"/>
<wire x1="49.53" y1="43.18" x2="53.34" y2="43.18" width="0.1524" layer="21"/>
<wire x1="49.53" y1="45.72" x2="53.34" y2="45.72" width="0.1524" layer="21"/>
<wire x1="49.53" y1="48.26" x2="53.34" y2="48.26" width="0.1524" layer="21"/>
<wire x1="49.53" y1="50.8" x2="53.34" y2="50.8" width="0.1524" layer="21"/>
<wire x1="49.53" y1="53.34" x2="53.34" y2="53.34" width="0.1524" layer="21"/>
<wire x1="49.53" y1="55.88" x2="53.34" y2="55.88" width="0.1524" layer="21"/>
<wire x1="49.53" y1="58.42" x2="53.34" y2="58.42" width="0.1524" layer="21"/>
<wire x1="49.53" y1="58.42" x2="48.895" y2="59.055" width="0.1524" layer="21"/>
<wire x1="48.895" y1="60.325" x2="48.895" y2="59.055" width="0.1524" layer="21"/>
<wire x1="48.895" y1="60.325" x2="49.53" y2="60.96" width="0.1524" layer="21"/>
<wire x1="53.34" y1="60.96" x2="53.975" y2="60.325" width="0.1524" layer="21"/>
<wire x1="53.975" y1="59.055" x2="53.975" y2="60.325" width="0.1524" layer="21"/>
<wire x1="53.975" y1="59.055" x2="53.34" y2="58.42" width="0.1524" layer="21"/>
<wire x1="49.53" y1="60.96" x2="53.34" y2="60.96" width="0.1524" layer="21"/>
<wire x1="49.53" y1="60.96" x2="48.895" y2="61.595" width="0.1524" layer="21"/>
<wire x1="48.895" y1="62.865" x2="48.895" y2="61.595" width="0.1524" layer="21"/>
<wire x1="48.895" y1="62.865" x2="49.53" y2="63.5" width="0.1524" layer="21"/>
<wire x1="53.34" y1="63.5" x2="53.975" y2="62.865" width="0.1524" layer="21"/>
<wire x1="53.975" y1="61.595" x2="53.975" y2="62.865" width="0.1524" layer="21"/>
<wire x1="53.975" y1="61.595" x2="53.34" y2="60.96" width="0.1524" layer="21"/>
<wire x1="49.53" y1="63.5" x2="53.34" y2="63.5" width="0.1524" layer="21"/>
<wire x1="49.53" y1="63.5" x2="48.895" y2="64.135" width="0.1524" layer="21"/>
<wire x1="48.895" y1="65.405" x2="48.895" y2="64.135" width="0.1524" layer="21"/>
<wire x1="48.895" y1="65.405" x2="49.53" y2="66.04" width="0.1524" layer="21"/>
<wire x1="53.34" y1="66.04" x2="53.975" y2="65.405" width="0.1524" layer="21"/>
<wire x1="53.975" y1="64.135" x2="53.975" y2="65.405" width="0.1524" layer="21"/>
<wire x1="53.975" y1="64.135" x2="53.34" y2="63.5" width="0.1524" layer="21"/>
<wire x1="49.53" y1="66.04" x2="53.34" y2="66.04" width="0.1524" layer="21"/>
<wire x1="49.53" y1="66.04" x2="48.895" y2="66.675" width="0.1524" layer="21"/>
<wire x1="48.895" y1="67.945" x2="48.895" y2="66.675" width="0.1524" layer="21"/>
<wire x1="48.895" y1="67.945" x2="49.53" y2="68.58" width="0.1524" layer="21"/>
<wire x1="53.34" y1="68.58" x2="53.975" y2="67.945" width="0.1524" layer="21"/>
<wire x1="53.975" y1="66.675" x2="53.975" y2="67.945" width="0.1524" layer="21"/>
<wire x1="53.975" y1="66.675" x2="53.34" y2="66.04" width="0.1524" layer="21"/>
<wire x1="49.53" y1="68.58" x2="53.34" y2="68.58" width="0.1524" layer="21"/>
<wire x1="49.53" y1="68.58" x2="48.895" y2="69.215" width="0.1524" layer="21"/>
<wire x1="48.895" y1="70.485" x2="48.895" y2="69.215" width="0.1524" layer="21"/>
<wire x1="48.895" y1="70.485" x2="49.53" y2="71.12" width="0.1524" layer="21"/>
<wire x1="53.34" y1="71.12" x2="53.975" y2="70.485" width="0.1524" layer="21"/>
<wire x1="53.975" y1="69.215" x2="53.975" y2="70.485" width="0.1524" layer="21"/>
<wire x1="53.975" y1="69.215" x2="53.34" y2="68.58" width="0.1524" layer="21"/>
<wire x1="49.53" y1="71.12" x2="53.34" y2="71.12" width="0.1524" layer="21"/>
<wire x1="49.53" y1="71.12" x2="48.895" y2="71.755" width="0.1524" layer="21"/>
<wire x1="48.895" y1="73.025" x2="48.895" y2="71.755" width="0.1524" layer="21"/>
<wire x1="48.895" y1="73.025" x2="49.53" y2="73.66" width="0.1524" layer="21"/>
<wire x1="53.34" y1="73.66" x2="53.975" y2="73.025" width="0.1524" layer="21"/>
<wire x1="53.975" y1="71.755" x2="53.975" y2="73.025" width="0.1524" layer="21"/>
<wire x1="53.975" y1="71.755" x2="53.34" y2="71.12" width="0.1524" layer="21"/>
<wire x1="49.53" y1="73.66" x2="53.34" y2="73.66" width="0.1524" layer="21"/>
<wire x1="49.53" y1="73.66" x2="48.895" y2="74.295" width="0.1524" layer="21"/>
<wire x1="48.895" y1="75.565" x2="48.895" y2="74.295" width="0.1524" layer="21"/>
<wire x1="48.895" y1="75.565" x2="49.53" y2="76.2" width="0.1524" layer="21"/>
<wire x1="53.34" y1="76.2" x2="53.975" y2="75.565" width="0.1524" layer="21"/>
<wire x1="53.975" y1="74.295" x2="53.975" y2="75.565" width="0.1524" layer="21"/>
<wire x1="53.975" y1="74.295" x2="53.34" y2="73.66" width="0.1524" layer="21"/>
<wire x1="49.53" y1="76.2" x2="53.34" y2="76.2" width="0.1524" layer="21"/>
<wire x1="0" y1="6.35" x2="6.35" y2="0" width="0.127" layer="51" style="shortdash" curve="90"/>
<wire x1="0" y1="73.66" x2="0" y2="14.605" width="0.127" layer="20"/>
<wire x1="48.26" y1="0" x2="54.61" y2="6.35" width="0.127" layer="51" style="shortdash" curve="90"/>
<wire x1="6.35" y1="0" x2="48.26" y2="0" width="0.127" layer="51" style="shortdash"/>
<wire x1="0" y1="14.605" x2="0" y2="6.35" width="0.127" layer="51" style="shortdash"/>
<wire x1="0" y1="14.605" x2="12.7" y2="14.605" width="0.127" layer="20"/>
<wire x1="41.91" y1="14.605" x2="54.61" y2="14.605" width="0.127" layer="20"/>
<wire x1="18.415" y1="20.32" x2="36.195" y2="20.32" width="0.127" layer="20"/>
<wire x1="18.415" y1="20.32" x2="12.7" y2="14.605" width="0.127" layer="20" curve="90"/>
<wire x1="36.195" y1="20.32" x2="41.91" y2="14.605" width="0.127" layer="20" curve="-90"/>
<pad name="1" x="4.445" y="19.05" drill="1" shape="square" rot="R90"/>
<pad name="2" x="1.905" y="19.05" drill="1" rot="R90"/>
<pad name="3" x="4.445" y="21.59" drill="1" rot="R90"/>
<pad name="4" x="1.905" y="21.59" drill="1" rot="R90"/>
<pad name="5" x="4.445" y="24.13" drill="1" rot="R90"/>
<pad name="6" x="1.905" y="24.13" drill="1" rot="R90"/>
<pad name="7" x="4.445" y="26.67" drill="1" rot="R90"/>
<pad name="8" x="1.905" y="26.67" drill="1" rot="R90"/>
<pad name="9" x="4.445" y="29.21" drill="1" rot="R90"/>
<pad name="10" x="1.905" y="29.21" drill="1" rot="R90"/>
<pad name="11" x="4.445" y="31.75" drill="1" rot="R90"/>
<pad name="12" x="1.905" y="31.75" drill="1" rot="R90"/>
<pad name="13" x="4.445" y="34.29" drill="1" rot="R90"/>
<pad name="14" x="1.905" y="34.29" drill="1" rot="R90"/>
<pad name="15" x="4.445" y="36.83" drill="1" rot="R90"/>
<pad name="16" x="1.905" y="36.83" drill="1" rot="R90"/>
<pad name="17" x="4.445" y="39.37" drill="1" rot="R90"/>
<pad name="18" x="1.905" y="39.37" drill="1" rot="R90"/>
<pad name="19" x="4.445" y="41.91" drill="1" rot="R90"/>
<pad name="20" x="1.905" y="41.91" drill="1" rot="R90"/>
<pad name="21" x="4.445" y="44.45" drill="1" rot="R90"/>
<pad name="22" x="1.905" y="44.45" drill="1" rot="R90"/>
<pad name="23" x="4.445" y="46.99" drill="1" rot="R90"/>
<pad name="24" x="1.905" y="46.99" drill="1" rot="R90"/>
<pad name="25" x="4.445" y="49.53" drill="1" rot="R90"/>
<pad name="26" x="1.905" y="49.53" drill="1" rot="R90"/>
<pad name="27" x="4.445" y="52.07" drill="1" rot="R90"/>
<pad name="28" x="1.905" y="52.07" drill="1" rot="R90"/>
<pad name="29" x="4.445" y="54.61" drill="1" rot="R90"/>
<pad name="30" x="1.905" y="54.61" drill="1" rot="R90"/>
<pad name="31" x="4.445" y="57.15" drill="1" rot="R90"/>
<pad name="32" x="1.905" y="57.15" drill="1" rot="R90"/>
<pad name="33" x="4.445" y="59.69" drill="1" rot="R90"/>
<pad name="34" x="1.905" y="59.69" drill="1" rot="R90"/>
<pad name="35" x="4.445" y="62.23" drill="1" rot="R90"/>
<pad name="36" x="1.905" y="62.23" drill="1" rot="R90"/>
<pad name="37" x="4.445" y="64.77" drill="1" rot="R90"/>
<pad name="38" x="1.905" y="64.77" drill="1" rot="R90"/>
<pad name="39" x="4.445" y="67.31" drill="1" rot="R90"/>
<pad name="40" x="1.905" y="67.31" drill="1" rot="R90"/>
<pad name="41" x="4.445" y="69.85" drill="1" rot="R90"/>
<pad name="42" x="1.905" y="69.85" drill="1" rot="R90"/>
<pad name="43" x="4.445" y="72.39" drill="1" rot="R90"/>
<pad name="44" x="1.905" y="72.39" drill="1" rot="R90"/>
<pad name="45" x="4.445" y="74.93" drill="1" rot="R90"/>
<pad name="46" x="1.905" y="74.93" drill="1" rot="R90"/>
<pad name="47" x="52.705" y="19.05" drill="1" shape="square" rot="R90"/>
<pad name="48" x="50.165" y="19.05" drill="1" rot="R90"/>
<pad name="49" x="52.705" y="21.59" drill="1" rot="R90"/>
<pad name="50" x="50.165" y="21.59" drill="1" rot="R90"/>
<pad name="51" x="52.705" y="24.13" drill="1" rot="R90"/>
<pad name="52" x="50.165" y="24.13" drill="1" rot="R90"/>
<pad name="53" x="52.705" y="26.67" drill="1" rot="R90"/>
<pad name="54" x="50.165" y="26.67" drill="1" rot="R90"/>
<pad name="55" x="52.705" y="29.21" drill="1" rot="R90"/>
<pad name="56" x="50.165" y="29.21" drill="1" rot="R90"/>
<pad name="57" x="52.705" y="31.75" drill="1" rot="R90"/>
<pad name="58" x="50.165" y="31.75" drill="1" rot="R90"/>
<pad name="59" x="52.705" y="34.29" drill="1" rot="R90"/>
<pad name="60" x="50.165" y="34.29" drill="1" rot="R90"/>
<pad name="61" x="52.705" y="36.83" drill="1" rot="R90"/>
<pad name="62" x="50.165" y="36.83" drill="1" rot="R90"/>
<pad name="63" x="52.705" y="39.37" drill="1" rot="R90"/>
<pad name="64" x="50.165" y="39.37" drill="1" rot="R90"/>
<pad name="65" x="52.705" y="41.91" drill="1" rot="R90"/>
<pad name="66" x="50.165" y="41.91" drill="1" rot="R90"/>
<pad name="67" x="52.705" y="44.45" drill="1" rot="R90"/>
<pad name="68" x="50.165" y="44.45" drill="1" rot="R90"/>
<pad name="69" x="52.705" y="46.99" drill="1" rot="R90"/>
<pad name="70" x="50.165" y="46.99" drill="1" rot="R90"/>
<pad name="71" x="52.705" y="49.53" drill="1" rot="R90"/>
<pad name="72" x="50.165" y="49.53" drill="1" rot="R90"/>
<pad name="73" x="52.705" y="52.07" drill="1" rot="R90"/>
<pad name="74" x="50.165" y="52.07" drill="1" rot="R90"/>
<pad name="75" x="52.705" y="54.61" drill="1" rot="R90"/>
<pad name="76" x="50.165" y="54.61" drill="1" rot="R90"/>
<pad name="77" x="52.705" y="57.15" drill="1" rot="R90"/>
<pad name="78" x="50.165" y="57.15" drill="1" rot="R90"/>
<pad name="79" x="52.705" y="59.69" drill="1" rot="R90"/>
<pad name="80" x="50.165" y="59.69" drill="1" rot="R90"/>
<pad name="81" x="52.705" y="62.23" drill="1" rot="R90"/>
<pad name="82" x="50.165" y="62.23" drill="1" rot="R90"/>
<pad name="83" x="52.705" y="64.77" drill="1" rot="R90"/>
<pad name="84" x="50.165" y="64.77" drill="1" rot="R90"/>
<pad name="85" x="52.705" y="67.31" drill="1" rot="R90"/>
<pad name="86" x="50.165" y="67.31" drill="1" rot="R90"/>
<pad name="87" x="52.705" y="69.85" drill="1" rot="R90"/>
<pad name="88" x="50.165" y="69.85" drill="1" rot="R90"/>
<pad name="89" x="52.705" y="72.39" drill="1" rot="R90"/>
<pad name="90" x="50.165" y="72.39" drill="1" rot="R90"/>
<pad name="91" x="52.705" y="74.93" drill="1" rot="R90"/>
<pad name="92" x="50.165" y="74.93" drill="1" rot="R90"/>
<text x="7.62" y="17.78" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="10.16" y="17.78" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="45.72" y="17.78" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="48.26" y="17.78" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="2.54" y="77.47" size="1.27" layer="51">P8</text>
<text x="50.8" y="77.47" size="1.27" layer="51">P9</text>
<rectangle x1="4.191" y1="18.796" x2="4.699" y2="19.304" layer="51" rot="R90"/>
<rectangle x1="1.651" y1="18.796" x2="2.159" y2="19.304" layer="51" rot="R90"/>
<rectangle x1="1.651" y1="21.336" x2="2.159" y2="21.844" layer="51" rot="R90"/>
<rectangle x1="4.191" y1="21.336" x2="4.699" y2="21.844" layer="51" rot="R90"/>
<rectangle x1="1.651" y1="23.876" x2="2.159" y2="24.384" layer="51" rot="R90"/>
<rectangle x1="4.191" y1="23.876" x2="4.699" y2="24.384" layer="51" rot="R90"/>
<rectangle x1="1.651" y1="26.416" x2="2.159" y2="26.924" layer="51" rot="R90"/>
<rectangle x1="1.651" y1="28.956" x2="2.159" y2="29.464" layer="51" rot="R90"/>
<rectangle x1="1.651" y1="31.496" x2="2.159" y2="32.004" layer="51" rot="R90"/>
<rectangle x1="4.191" y1="26.416" x2="4.699" y2="26.924" layer="51" rot="R90"/>
<rectangle x1="4.191" y1="28.956" x2="4.699" y2="29.464" layer="51" rot="R90"/>
<rectangle x1="4.191" y1="31.496" x2="4.699" y2="32.004" layer="51" rot="R90"/>
<rectangle x1="1.651" y1="34.036" x2="2.159" y2="34.544" layer="51" rot="R90"/>
<rectangle x1="4.191" y1="34.036" x2="4.699" y2="34.544" layer="51" rot="R90"/>
<rectangle x1="1.651" y1="36.576" x2="2.159" y2="37.084" layer="51" rot="R90"/>
<rectangle x1="4.191" y1="36.576" x2="4.699" y2="37.084" layer="51" rot="R90"/>
<rectangle x1="1.651" y1="39.116" x2="2.159" y2="39.624" layer="51" rot="R90"/>
<rectangle x1="4.191" y1="39.116" x2="4.699" y2="39.624" layer="51" rot="R90"/>
<rectangle x1="1.651" y1="41.656" x2="2.159" y2="42.164" layer="51" rot="R90"/>
<rectangle x1="4.191" y1="41.656" x2="4.699" y2="42.164" layer="51" rot="R90"/>
<rectangle x1="1.651" y1="44.196" x2="2.159" y2="44.704" layer="51" rot="R90"/>
<rectangle x1="4.191" y1="44.196" x2="4.699" y2="44.704" layer="51" rot="R90"/>
<rectangle x1="1.651" y1="46.736" x2="2.159" y2="47.244" layer="51" rot="R90"/>
<rectangle x1="4.191" y1="46.736" x2="4.699" y2="47.244" layer="51" rot="R90"/>
<rectangle x1="1.651" y1="49.276" x2="2.159" y2="49.784" layer="51" rot="R90"/>
<rectangle x1="4.191" y1="49.276" x2="4.699" y2="49.784" layer="51" rot="R90"/>
<rectangle x1="1.651" y1="51.816" x2="2.159" y2="52.324" layer="51" rot="R90"/>
<rectangle x1="4.191" y1="51.816" x2="4.699" y2="52.324" layer="51" rot="R90"/>
<rectangle x1="1.651" y1="54.356" x2="2.159" y2="54.864" layer="51" rot="R90"/>
<rectangle x1="4.191" y1="54.356" x2="4.699" y2="54.864" layer="51" rot="R90"/>
<rectangle x1="1.651" y1="56.896" x2="2.159" y2="57.404" layer="51" rot="R90"/>
<rectangle x1="4.191" y1="56.896" x2="4.699" y2="57.404" layer="51" rot="R90"/>
<rectangle x1="1.651" y1="59.436" x2="2.159" y2="59.944" layer="51" rot="R90"/>
<rectangle x1="4.191" y1="59.436" x2="4.699" y2="59.944" layer="51" rot="R90"/>
<rectangle x1="1.651" y1="61.976" x2="2.159" y2="62.484" layer="51" rot="R90"/>
<rectangle x1="4.191" y1="61.976" x2="4.699" y2="62.484" layer="51" rot="R90"/>
<rectangle x1="1.651" y1="64.516" x2="2.159" y2="65.024" layer="51" rot="R90"/>
<rectangle x1="4.191" y1="64.516" x2="4.699" y2="65.024" layer="51" rot="R90"/>
<rectangle x1="1.651" y1="67.056" x2="2.159" y2="67.564" layer="51" rot="R90"/>
<rectangle x1="4.191" y1="67.056" x2="4.699" y2="67.564" layer="51" rot="R90"/>
<rectangle x1="1.651" y1="69.596" x2="2.159" y2="70.104" layer="51" rot="R90"/>
<rectangle x1="4.191" y1="69.596" x2="4.699" y2="70.104" layer="51" rot="R90"/>
<rectangle x1="1.651" y1="72.136" x2="2.159" y2="72.644" layer="51" rot="R90"/>
<rectangle x1="4.191" y1="72.136" x2="4.699" y2="72.644" layer="51" rot="R90"/>
<rectangle x1="1.651" y1="74.676" x2="2.159" y2="75.184" layer="51" rot="R90"/>
<rectangle x1="4.191" y1="74.676" x2="4.699" y2="75.184" layer="51" rot="R90"/>
<rectangle x1="52.451" y1="18.796" x2="52.959" y2="19.304" layer="51" rot="R90"/>
<rectangle x1="49.911" y1="18.796" x2="50.419" y2="19.304" layer="51" rot="R90"/>
<rectangle x1="49.911" y1="21.336" x2="50.419" y2="21.844" layer="51" rot="R90"/>
<rectangle x1="52.451" y1="21.336" x2="52.959" y2="21.844" layer="51" rot="R90"/>
<rectangle x1="49.911" y1="23.876" x2="50.419" y2="24.384" layer="51" rot="R90"/>
<rectangle x1="52.451" y1="23.876" x2="52.959" y2="24.384" layer="51" rot="R90"/>
<rectangle x1="49.911" y1="26.416" x2="50.419" y2="26.924" layer="51" rot="R90"/>
<rectangle x1="49.911" y1="28.956" x2="50.419" y2="29.464" layer="51" rot="R90"/>
<rectangle x1="49.911" y1="31.496" x2="50.419" y2="32.004" layer="51" rot="R90"/>
<rectangle x1="52.451" y1="26.416" x2="52.959" y2="26.924" layer="51" rot="R90"/>
<rectangle x1="52.451" y1="28.956" x2="52.959" y2="29.464" layer="51" rot="R90"/>
<rectangle x1="52.451" y1="31.496" x2="52.959" y2="32.004" layer="51" rot="R90"/>
<rectangle x1="49.911" y1="34.036" x2="50.419" y2="34.544" layer="51" rot="R90"/>
<rectangle x1="52.451" y1="34.036" x2="52.959" y2="34.544" layer="51" rot="R90"/>
<rectangle x1="49.911" y1="36.576" x2="50.419" y2="37.084" layer="51" rot="R90"/>
<rectangle x1="52.451" y1="36.576" x2="52.959" y2="37.084" layer="51" rot="R90"/>
<rectangle x1="49.911" y1="39.116" x2="50.419" y2="39.624" layer="51" rot="R90"/>
<rectangle x1="52.451" y1="39.116" x2="52.959" y2="39.624" layer="51" rot="R90"/>
<rectangle x1="49.911" y1="41.656" x2="50.419" y2="42.164" layer="51" rot="R90"/>
<rectangle x1="52.451" y1="41.656" x2="52.959" y2="42.164" layer="51" rot="R90"/>
<rectangle x1="49.911" y1="44.196" x2="50.419" y2="44.704" layer="51" rot="R90"/>
<rectangle x1="52.451" y1="44.196" x2="52.959" y2="44.704" layer="51" rot="R90"/>
<rectangle x1="49.911" y1="46.736" x2="50.419" y2="47.244" layer="51" rot="R90"/>
<rectangle x1="52.451" y1="46.736" x2="52.959" y2="47.244" layer="51" rot="R90"/>
<rectangle x1="49.911" y1="49.276" x2="50.419" y2="49.784" layer="51" rot="R90"/>
<rectangle x1="52.451" y1="49.276" x2="52.959" y2="49.784" layer="51" rot="R90"/>
<rectangle x1="49.911" y1="51.816" x2="50.419" y2="52.324" layer="51" rot="R90"/>
<rectangle x1="52.451" y1="51.816" x2="52.959" y2="52.324" layer="51" rot="R90"/>
<rectangle x1="49.911" y1="54.356" x2="50.419" y2="54.864" layer="51" rot="R90"/>
<rectangle x1="52.451" y1="54.356" x2="52.959" y2="54.864" layer="51" rot="R90"/>
<rectangle x1="49.911" y1="56.896" x2="50.419" y2="57.404" layer="51" rot="R90"/>
<rectangle x1="52.451" y1="56.896" x2="52.959" y2="57.404" layer="51" rot="R90"/>
<rectangle x1="49.911" y1="59.436" x2="50.419" y2="59.944" layer="51" rot="R90"/>
<rectangle x1="52.451" y1="59.436" x2="52.959" y2="59.944" layer="51" rot="R90"/>
<rectangle x1="49.911" y1="61.976" x2="50.419" y2="62.484" layer="51" rot="R90"/>
<rectangle x1="52.451" y1="61.976" x2="52.959" y2="62.484" layer="51" rot="R90"/>
<rectangle x1="49.911" y1="64.516" x2="50.419" y2="65.024" layer="51" rot="R90"/>
<rectangle x1="52.451" y1="64.516" x2="52.959" y2="65.024" layer="51" rot="R90"/>
<rectangle x1="49.911" y1="67.056" x2="50.419" y2="67.564" layer="51" rot="R90"/>
<rectangle x1="52.451" y1="67.056" x2="52.959" y2="67.564" layer="51" rot="R90"/>
<rectangle x1="49.911" y1="69.596" x2="50.419" y2="70.104" layer="51" rot="R90"/>
<rectangle x1="52.451" y1="69.596" x2="52.959" y2="70.104" layer="51" rot="R90"/>
<rectangle x1="49.911" y1="72.136" x2="50.419" y2="72.644" layer="51" rot="R90"/>
<rectangle x1="52.451" y1="72.136" x2="52.959" y2="72.644" layer="51" rot="R90"/>
<rectangle x1="49.911" y1="74.676" x2="50.419" y2="75.184" layer="51" rot="R90"/>
<rectangle x1="52.451" y1="74.676" x2="52.959" y2="75.184" layer="51" rot="R90"/>
<rectangle x1="40.005" y1="0" x2="50.165" y2="11.43" layer="39"/>
<rectangle x1="15.875" y1="0" x2="34.29" y2="17.78" layer="39"/>
</package>
<package name="TSSOP28">
<wire x1="-4.4646" y1="-2.2828" x2="4.4646" y2="-2.2828" width="0.1524" layer="51"/>
<wire x1="4.4646" y1="2.2828" x2="4.4646" y2="-2.2828" width="0.1524" layer="21"/>
<wire x1="4.4646" y1="2.2828" x2="-4.4646" y2="2.2828" width="0.1524" layer="51"/>
<wire x1="-4.4646" y1="-2.2828" x2="-4.4646" y2="2.2828" width="0.1524" layer="21"/>
<circle x="-3.5756" y="-1.2192" radius="0.4572" width="0.1524" layer="21"/>
<smd name="1" x="-4.225" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="2" x="-3.575" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="3" x="-2.925" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="4" x="-2.275" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="5" x="-1.625" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="6" x="-0.975" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="7" x="-0.325" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="8" x="0.325" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="21" x="0.325" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="22" x="-0.325" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="23" x="-0.975" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="24" x="-1.625" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="25" x="-2.275" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="26" x="-2.925" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="27" x="-3.575" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="28" x="-4.225" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="9" x="0.975" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="10" x="1.625" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="20" x="0.975" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="19" x="1.625" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="11" x="2.275" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="12" x="2.925" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="13" x="3.575" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="14" x="4.225" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="15" x="4.225" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="16" x="3.575" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="17" x="2.925" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="18" x="2.275" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<text x="-4.8456" y="-2.0828" size="0.4064" layer="25" rot="R90">&gt;NAME</text>
<text x="5.0742" y="-2.0828" size="0.4064" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.3266" y1="-3.121" x2="-4.1234" y2="-2.2828" layer="51"/>
<rectangle x1="-3.6766" y1="-3.121" x2="-3.4734" y2="-2.2828" layer="51"/>
<rectangle x1="-3.0266" y1="-3.121" x2="-2.8234" y2="-2.2828" layer="51"/>
<rectangle x1="-2.3766" y1="-3.121" x2="-2.1734" y2="-2.2828" layer="51"/>
<rectangle x1="-1.7266" y1="-3.121" x2="-1.5234" y2="-2.2828" layer="51"/>
<rectangle x1="-1.0766" y1="-3.121" x2="-0.8734" y2="-2.2828" layer="51"/>
<rectangle x1="-0.4266" y1="-3.121" x2="-0.2234" y2="-2.2828" layer="51"/>
<rectangle x1="0.2234" y1="-3.121" x2="0.4266" y2="-2.2828" layer="51"/>
<rectangle x1="0.2234" y1="2.2828" x2="0.4266" y2="3.121" layer="51"/>
<rectangle x1="-0.4266" y1="2.2828" x2="-0.2234" y2="3.121" layer="51"/>
<rectangle x1="-1.0766" y1="2.2828" x2="-0.8734" y2="3.121" layer="51"/>
<rectangle x1="-1.7266" y1="2.2828" x2="-1.5234" y2="3.121" layer="51"/>
<rectangle x1="-2.3766" y1="2.2828" x2="-2.1734" y2="3.121" layer="51"/>
<rectangle x1="-3.0266" y1="2.2828" x2="-2.8234" y2="3.121" layer="51"/>
<rectangle x1="-3.6766" y1="2.2828" x2="-3.4734" y2="3.121" layer="51"/>
<rectangle x1="-4.3266" y1="2.2828" x2="-4.1234" y2="3.121" layer="51"/>
<rectangle x1="0.8734" y1="-3.121" x2="1.0766" y2="-2.2828" layer="51"/>
<rectangle x1="1.5234" y1="-3.121" x2="1.7266" y2="-2.2828" layer="51"/>
<rectangle x1="0.8734" y1="2.2828" x2="1.0766" y2="3.121" layer="51"/>
<rectangle x1="1.5234" y1="2.2828" x2="1.7266" y2="3.121" layer="51"/>
<rectangle x1="2.1734" y1="-3.121" x2="2.3766" y2="-2.2828" layer="51"/>
<rectangle x1="2.8234" y1="-3.121" x2="3.0266" y2="-2.2828" layer="51"/>
<rectangle x1="3.4734" y1="-3.121" x2="3.6766" y2="-2.2828" layer="51"/>
<rectangle x1="4.1234" y1="-3.121" x2="4.3266" y2="-2.2828" layer="51"/>
<rectangle x1="4.1234" y1="2.2828" x2="4.3266" y2="3.121" layer="51"/>
<rectangle x1="3.4734" y1="2.2828" x2="3.6766" y2="3.121" layer="51"/>
<rectangle x1="2.8234" y1="2.2828" x2="3.0266" y2="3.121" layer="51"/>
<rectangle x1="2.1734" y1="2.2828" x2="2.3766" y2="3.121" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="BEAGLEBONE-1">
<wire x1="-31.75" y1="2.54" x2="31.75" y2="2.54" width="0.4064" layer="94"/>
<wire x1="31.75" y1="2.54" x2="31.75" y2="63.5" width="0.4064" layer="94"/>
<wire x1="31.75" y1="63.5" x2="-31.75" y2="63.5" width="0.4064" layer="94"/>
<wire x1="-31.75" y1="63.5" x2="-31.75" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-31.75" y1="-63.5" x2="31.75" y2="-63.5" width="0.4064" layer="94"/>
<wire x1="31.75" y1="-63.5" x2="31.75" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="31.75" y1="-2.54" x2="-31.75" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-31.75" y1="-2.54" x2="-31.75" y2="-63.5" width="0.4064" layer="94"/>
<text x="-31.75" y="66.675" size="1.778" layer="95">&gt;NAME</text>
<text x="-31.75" y="-66.04" size="1.778" layer="96">&gt;VALUE</text>
<text x="-31.75" y="64.135" size="1.778" layer="95">P8</text>
<text x="-31.75" y="-1.905" size="1.778" layer="95">P9</text>
<pin name="GND@1" x="-27.94" y="-5.08" visible="pin" length="short" direction="pwr" function="dot"/>
<pin name="GND@2" x="27.94" y="-5.08" visible="pin" length="short" direction="pwr" function="dot" rot="R180"/>
<pin name="VDD_3V3EXP@1" x="-27.94" y="-7.62" visible="pin" length="short" direction="pwr" function="dot"/>
<pin name="VDD_3V3EXP@2" x="27.94" y="-7.62" visible="pin" length="short" direction="pwr" function="dot" rot="R180"/>
<pin name="VDD_5V@1" x="-27.94" y="-10.16" visible="pin" length="short" direction="pwr" function="dot"/>
<pin name="VDD_5V@2" x="27.94" y="-10.16" visible="pin" length="short" direction="pwr" function="dot" rot="R180"/>
<pin name="SYS_5V@1" x="-27.94" y="-12.7" visible="pin" length="short" direction="pwr" function="dot"/>
<pin name="SYS_5V@2" x="27.94" y="-12.7" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="PWR_BUT" x="-27.94" y="-15.24" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="SYS_RESETN" x="27.94" y="-15.24" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="UART4_RXD" x="-27.94" y="-17.78" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO1_28" x="27.94" y="-17.78" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="UART4_TXD" x="-27.94" y="-20.32" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="EHRPWM1A" x="27.94" y="-20.32" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO1_16" x="-27.94" y="-22.86" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="EHRPWM1B" x="27.94" y="-22.86" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="I2C1_SCL" x="-27.94" y="-25.4" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="I2C1_SDA" x="27.94" y="-25.4" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="I2C2_SCL" x="-27.94" y="-27.94" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="I2C2_SDA" x="27.94" y="-27.94" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="UART2_TXD" x="-27.94" y="-30.48" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="UART2_RXD" x="27.94" y="-30.48" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO1_17" x="-27.94" y="-33.02" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="UART1_TXD" x="27.94" y="-33.02" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO3_21" x="-27.94" y="-35.56" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="UART1_RXD" x="27.94" y="-35.56" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO3_19" x="-27.94" y="-38.1" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="SPI1_CS0" x="27.94" y="-38.1" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="SPI1_DO" x="-27.94" y="-40.64" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="SPI1_DI" x="27.94" y="-40.64" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="SPI1_SCLK" x="-27.94" y="-43.18" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="VDD_ADC" x="27.94" y="-43.18" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="AIN4" x="-27.94" y="-45.72" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GNDA_ADC" x="27.94" y="-45.72" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="AIN6" x="-27.94" y="-48.26" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="AIN5" x="27.94" y="-48.26" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="AIN2" x="-27.94" y="-50.8" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="AIN3" x="27.94" y="-50.8" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="AIN0" x="-27.94" y="-53.34" visible="pin" length="short" direction="pwr" function="dot"/>
<pin name="AIN1" x="27.94" y="-53.34" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="CLKOUT2" x="-27.94" y="-55.88" visible="pin" length="short" direction="in" function="dot"/>
<pin name="GPIO0_7" x="27.94" y="-55.88" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GND@3" x="-27.94" y="-58.42" visible="pin" length="short" direction="pwr" function="dot"/>
<pin name="GND@5" x="27.94" y="-58.42" visible="pin" length="short" direction="pwr" function="dot" rot="R180"/>
<pin name="GND@4" x="-27.94" y="-60.96" visible="pin" length="short" direction="pwr" function="dot"/>
<pin name="GND@6" x="27.94" y="-60.96" visible="pin" length="short" direction="pwr" function="dot" rot="R180"/>
<pin name="GND@7" x="-27.94" y="60.96" visible="pin" length="short" direction="pwr" function="dot"/>
<pin name="GND@8" x="27.94" y="60.96" visible="pin" length="short" direction="pwr" function="dot" rot="R180"/>
<pin name="GPIO1_6" x="-27.94" y="58.42" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO1_7" x="27.94" y="58.42" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO1_2" x="-27.94" y="55.88" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO1_3" x="27.94" y="55.88" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="TIMER4" x="-27.94" y="53.34" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="TIMER7" x="27.94" y="53.34" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="TIMER5" x="-27.94" y="50.8" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="TIMER6" x="27.94" y="50.8" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO1_13" x="-27.94" y="48.26" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO1_12" x="27.94" y="48.26" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="EHRPWM2B" x="-27.94" y="45.72" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO0_26" x="27.94" y="45.72" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO1_15" x="-27.94" y="43.18" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO1_14" x="27.94" y="43.18" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO0_27" x="-27.94" y="40.64" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO2_1" x="27.94" y="40.64" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="EHRPWM2A" x="-27.94" y="38.1" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO1_31" x="27.94" y="38.1" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO1_30" x="-27.94" y="35.56" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO1_5" x="27.94" y="35.56" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO1_4" x="-27.94" y="33.02" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO1_1" x="27.94" y="33.02" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO1_0" x="-27.94" y="30.48" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO1_29" x="27.94" y="30.48" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO2_22" x="-27.94" y="27.94" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO2_24" x="27.94" y="27.94" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO2_23" x="-27.94" y="25.4" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO2_25" x="27.94" y="25.4" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="UART5_CTSN" x="-27.94" y="22.86" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="UART5_RTSN" x="27.94" y="22.86" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="UART4_RTSN" x="-27.94" y="20.32" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="UART3_RTSN" x="27.94" y="20.32" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="UART4_CTSN" x="-27.94" y="17.78" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="UART3_CTSN" x="27.94" y="17.78" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="UART5_TXD" x="-27.94" y="15.24" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="UART5_RXD" x="27.94" y="15.24" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO2_12" x="-27.94" y="12.7" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO2_13" x="27.94" y="12.7" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO2_10" x="-27.94" y="10.16" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO2_11" x="27.94" y="10.16" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO2_8" x="-27.94" y="7.62" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO2_9" x="27.94" y="7.62" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO2_6" x="-27.94" y="5.08" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO2_7" x="27.94" y="5.08" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="PCA9865">
<wire x1="-17.78" y1="17.78" x2="17.78" y2="17.78" width="0.254" layer="94" style="shortdash"/>
<wire x1="17.78" y1="17.78" x2="17.78" y2="-30.48" width="0.254" layer="94"/>
<wire x1="17.78" y1="-30.48" x2="-17.78" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-30.48" x2="-17.78" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-25.4" x2="-17.78" y2="17.78" width="0.254" layer="94"/>
<wire x1="-17.78" y1="33.02" x2="17.78" y2="33.02" width="0.254" layer="94"/>
<wire x1="-17.78" y1="17.78" x2="-17.78" y2="33.02" width="0.254" layer="94"/>
<wire x1="17.78" y1="17.78" x2="17.78" y2="33.02" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-25.4" x2="17.78" y2="-25.4" width="0.254" layer="94" style="shortdash"/>
<text x="-5.08" y="30.48" size="1.27" layer="94">PCA9685</text>
<text x="-12.7" y="27.94" size="1.27" layer="94">16 Channel 12-Bit PWM</text>
<text x="-15.24" y="22.86" size="1.27" layer="94">VDD: 2.3-5.5V</text>
<text x="5.08" y="22.86" size="1.27" layer="94">IO: 5V Safe</text>
<text x="-15.24" y="20.32" size="1.27" layer="94">Output: 25mA Each/400mA Total</text>
<text x="-15.24" y="-27.94" size="1.27" layer="94">GND EXTCLK when not in use!</text>
<text x="-17.78" y="35.56" size="1.27" layer="95">&gt;NAME</text>
<text x="-17.78" y="-33.02" size="1.27" layer="96">&gt;VALUE</text>
<text x="-13.97" y="25.4" size="1.27" layer="94">I2C Address: 1[A5..A0][R/W]</text>
<pin name="PWM0" x="20.32" y="15.24" length="short" direction="out" rot="R180"/>
<pin name="PWM1" x="20.32" y="12.7" length="short" direction="out" rot="R180"/>
<pin name="PWM2" x="20.32" y="10.16" length="short" direction="out" rot="R180"/>
<pin name="PWM3" x="20.32" y="7.62" length="short" direction="out" rot="R180"/>
<pin name="PWM4" x="20.32" y="5.08" length="short" direction="out" rot="R180"/>
<pin name="PWM5" x="20.32" y="2.54" length="short" direction="out" rot="R180"/>
<pin name="PWM6" x="20.32" y="0" length="short" direction="out" rot="R180"/>
<pin name="PWM7" x="20.32" y="-2.54" length="short" direction="out" rot="R180"/>
<pin name="PWM8" x="20.32" y="-5.08" length="short" direction="out" rot="R180"/>
<pin name="PWM9" x="20.32" y="-7.62" length="short" direction="out" rot="R180"/>
<pin name="PWM10" x="20.32" y="-10.16" length="short" direction="out" rot="R180"/>
<pin name="PWM11" x="20.32" y="-12.7" length="short" direction="out" rot="R180"/>
<pin name="PWM12" x="20.32" y="-15.24" length="short" direction="out" rot="R180"/>
<pin name="PWM13" x="20.32" y="-17.78" length="short" direction="out" rot="R180"/>
<pin name="PWM14" x="20.32" y="-20.32" length="short" direction="out" rot="R180"/>
<pin name="PWM15" x="20.32" y="-22.86" length="short" direction="out" rot="R180"/>
<pin name="GND" x="-20.32" y="-22.86" length="short" direction="pwr"/>
<pin name="SDA" x="-20.32" y="10.16" length="short"/>
<pin name="SCL" x="-20.32" y="7.62" length="short"/>
<pin name="#OE" x="-20.32" y="2.54" length="short" direction="in"/>
<pin name="EXTCLK" x="-20.32" y="-2.54" length="short" direction="in"/>
<pin name="A0" x="-20.32" y="-5.08" length="short"/>
<pin name="A1" x="-20.32" y="-7.62" length="short"/>
<pin name="A2" x="-20.32" y="-10.16" length="short"/>
<pin name="A3" x="-20.32" y="-12.7" length="short"/>
<pin name="A4" x="-20.32" y="-15.24" length="short"/>
<pin name="A5" x="-20.32" y="-17.78" length="short"/>
<pin name="VDD" x="-20.32" y="15.24" length="short" direction="pwr"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BEAGLEBONE_OUTLINE" prefix="JP">
<description>Should match A3 pinout - but not completely tested</description>
<gates>
<gate name="G$1" symbol="BEAGLEBONE-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BEAGLEBONE_SHIELD">
<connects>
<connect gate="G$1" pin="AIN0" pad="85"/>
<connect gate="G$1" pin="AIN1" pad="86"/>
<connect gate="G$1" pin="AIN2" pad="83"/>
<connect gate="G$1" pin="AIN3" pad="84"/>
<connect gate="G$1" pin="AIN4" pad="79"/>
<connect gate="G$1" pin="AIN5" pad="82"/>
<connect gate="G$1" pin="AIN6" pad="81"/>
<connect gate="G$1" pin="CLKOUT2" pad="87"/>
<connect gate="G$1" pin="EHRPWM1A" pad="60"/>
<connect gate="G$1" pin="EHRPWM1B" pad="62"/>
<connect gate="G$1" pin="EHRPWM2A" pad="19"/>
<connect gate="G$1" pin="EHRPWM2B" pad="13"/>
<connect gate="G$1" pin="GND@1" pad="47"/>
<connect gate="G$1" pin="GND@2" pad="48"/>
<connect gate="G$1" pin="GND@3" pad="89"/>
<connect gate="G$1" pin="GND@4" pad="91"/>
<connect gate="G$1" pin="GND@5" pad="90"/>
<connect gate="G$1" pin="GND@6" pad="92"/>
<connect gate="G$1" pin="GND@7" pad="1"/>
<connect gate="G$1" pin="GND@8" pad="2"/>
<connect gate="G$1" pin="GNDA_ADC" pad="80"/>
<connect gate="G$1" pin="GPIO0_26" pad="14"/>
<connect gate="G$1" pin="GPIO0_27" pad="17"/>
<connect gate="G$1" pin="GPIO0_7" pad="88"/>
<connect gate="G$1" pin="GPIO1_0" pad="25"/>
<connect gate="G$1" pin="GPIO1_1" pad="24"/>
<connect gate="G$1" pin="GPIO1_12" pad="12"/>
<connect gate="G$1" pin="GPIO1_13" pad="11"/>
<connect gate="G$1" pin="GPIO1_14" pad="16"/>
<connect gate="G$1" pin="GPIO1_15" pad="15"/>
<connect gate="G$1" pin="GPIO1_16" pad="61"/>
<connect gate="G$1" pin="GPIO1_17" pad="69"/>
<connect gate="G$1" pin="GPIO1_2" pad="5"/>
<connect gate="G$1" pin="GPIO1_28" pad="58"/>
<connect gate="G$1" pin="GPIO1_29" pad="26"/>
<connect gate="G$1" pin="GPIO1_3" pad="6"/>
<connect gate="G$1" pin="GPIO1_30" pad="21"/>
<connect gate="G$1" pin="GPIO1_31" pad="20"/>
<connect gate="G$1" pin="GPIO1_4" pad="23"/>
<connect gate="G$1" pin="GPIO1_5" pad="22"/>
<connect gate="G$1" pin="GPIO1_6" pad="3"/>
<connect gate="G$1" pin="GPIO1_7" pad="4"/>
<connect gate="G$1" pin="GPIO2_1" pad="18"/>
<connect gate="G$1" pin="GPIO2_10" pad="41"/>
<connect gate="G$1" pin="GPIO2_11" pad="42"/>
<connect gate="G$1" pin="GPIO2_12" pad="39"/>
<connect gate="G$1" pin="GPIO2_13" pad="40"/>
<connect gate="G$1" pin="GPIO2_22" pad="27"/>
<connect gate="G$1" pin="GPIO2_23" pad="29"/>
<connect gate="G$1" pin="GPIO2_24" pad="28"/>
<connect gate="G$1" pin="GPIO2_25" pad="30"/>
<connect gate="G$1" pin="GPIO2_6" pad="45"/>
<connect gate="G$1" pin="GPIO2_7" pad="46"/>
<connect gate="G$1" pin="GPIO2_8" pad="43"/>
<connect gate="G$1" pin="GPIO2_9" pad="44"/>
<connect gate="G$1" pin="GPIO3_19" pad="73"/>
<connect gate="G$1" pin="GPIO3_21" pad="71"/>
<connect gate="G$1" pin="I2C1_SCL" pad="63"/>
<connect gate="G$1" pin="I2C1_SDA" pad="64"/>
<connect gate="G$1" pin="I2C2_SCL" pad="65"/>
<connect gate="G$1" pin="I2C2_SDA" pad="66"/>
<connect gate="G$1" pin="PWR_BUT" pad="55"/>
<connect gate="G$1" pin="SPI1_CS0" pad="74"/>
<connect gate="G$1" pin="SPI1_DI" pad="76"/>
<connect gate="G$1" pin="SPI1_DO" pad="75"/>
<connect gate="G$1" pin="SPI1_SCLK" pad="77"/>
<connect gate="G$1" pin="SYS_5V@1" pad="53"/>
<connect gate="G$1" pin="SYS_5V@2" pad="54"/>
<connect gate="G$1" pin="SYS_RESETN" pad="56"/>
<connect gate="G$1" pin="TIMER4" pad="7"/>
<connect gate="G$1" pin="TIMER5" pad="9"/>
<connect gate="G$1" pin="TIMER6" pad="10"/>
<connect gate="G$1" pin="TIMER7" pad="8"/>
<connect gate="G$1" pin="UART1_RXD" pad="72"/>
<connect gate="G$1" pin="UART1_TXD" pad="70"/>
<connect gate="G$1" pin="UART2_RXD" pad="68"/>
<connect gate="G$1" pin="UART2_TXD" pad="67"/>
<connect gate="G$1" pin="UART3_CTSN" pad="36"/>
<connect gate="G$1" pin="UART3_RTSN" pad="34"/>
<connect gate="G$1" pin="UART4_CTSN" pad="35"/>
<connect gate="G$1" pin="UART4_RTSN" pad="33"/>
<connect gate="G$1" pin="UART4_RXD" pad="57"/>
<connect gate="G$1" pin="UART4_TXD" pad="59"/>
<connect gate="G$1" pin="UART5_CTSN" pad="31"/>
<connect gate="G$1" pin="UART5_RTSN" pad="32"/>
<connect gate="G$1" pin="UART5_RXD" pad="38"/>
<connect gate="G$1" pin="UART5_TXD" pad="37"/>
<connect gate="G$1" pin="VDD_3V3EXP@1" pad="49"/>
<connect gate="G$1" pin="VDD_3V3EXP@2" pad="50"/>
<connect gate="G$1" pin="VDD_5V@1" pad="51"/>
<connect gate="G$1" pin="VDD_5V@2" pad="52"/>
<connect gate="G$1" pin="VDD_ADC" pad="78"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PCA9685" prefix="U" uservalue="yes">
<description>&lt;b&gt;PCA9685&lt;/b&gt; - 16 Channel 12-Bit I2C PWM Controller
&lt;p&gt;5.0V tolerant 16 channel, 12-bit I2C PWM controller with 25mA per output (max. 400mA total)&lt;/p&gt;
&lt;p&gt;Digikey: 568-5931-1-ND&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="PCA9865" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TSSOP28">
<connects>
<connect gate="G$1" pin="#OE" pad="23"/>
<connect gate="G$1" pin="A0" pad="1"/>
<connect gate="G$1" pin="A1" pad="2"/>
<connect gate="G$1" pin="A2" pad="3"/>
<connect gate="G$1" pin="A3" pad="4"/>
<connect gate="G$1" pin="A4" pad="5"/>
<connect gate="G$1" pin="A5" pad="24"/>
<connect gate="G$1" pin="EXTCLK" pad="25"/>
<connect gate="G$1" pin="GND" pad="14"/>
<connect gate="G$1" pin="PWM0" pad="6"/>
<connect gate="G$1" pin="PWM1" pad="7"/>
<connect gate="G$1" pin="PWM10" pad="17"/>
<connect gate="G$1" pin="PWM11" pad="18"/>
<connect gate="G$1" pin="PWM12" pad="19"/>
<connect gate="G$1" pin="PWM13" pad="20"/>
<connect gate="G$1" pin="PWM14" pad="21"/>
<connect gate="G$1" pin="PWM15" pad="22"/>
<connect gate="G$1" pin="PWM2" pad="8"/>
<connect gate="G$1" pin="PWM3" pad="9"/>
<connect gate="G$1" pin="PWM4" pad="10"/>
<connect gate="G$1" pin="PWM5" pad="11"/>
<connect gate="G$1" pin="PWM6" pad="12"/>
<connect gate="G$1" pin="PWM7" pad="13"/>
<connect gate="G$1" pin="PWM8" pad="15"/>
<connect gate="G$1" pin="PWM9" pad="16"/>
<connect gate="G$1" pin="SCL" pad="26"/>
<connect gate="G$1" pin="SDA" pad="27"/>
<connect gate="G$1" pin="VDD" pad="28"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="2X16">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-20.32" y1="-1.905" x2="-19.685" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-19.685" y1="-2.54" x2="-18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-18.415" y1="-2.54" x2="-17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="-1.905" x2="-17.145" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-17.145" y1="-2.54" x2="-15.875" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-15.875" y1="-2.54" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="-1.905" x2="-14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-14.605" y1="-2.54" x2="-13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="-2.54" x2="-10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-2.54" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="-2.54" x2="-8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.54" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-2.54" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="-1.905" x2="-20.32" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="1.905" x2="-19.685" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-19.685" y1="2.54" x2="-18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-18.415" y1="2.54" x2="-17.78" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="1.905" x2="-17.145" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-17.145" y1="2.54" x2="-15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-15.875" y1="2.54" x2="-15.24" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="1.905" x2="-14.605" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-14.605" y1="2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="2.54" x2="-12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.065" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="2.54" x2="-10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="2.54" x2="-10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-9.525" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="2.54" x2="-8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="2.54" x2="-7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="2.54" x2="-5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="2.54" x2="6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.54" x2="7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.255" y1="2.54" x2="9.525" y2="2.54" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="9.525" y2="2.54" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.905" x2="13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="14.605" y1="2.54" x2="13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="14.605" y1="2.54" x2="15.24" y2="1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="1.905" x2="15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="2.54" x2="15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="2.54" x2="17.78" y2="1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="1.905" x2="18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="2.54" x2="18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="2.54" x2="20.32" y2="1.905" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="19.685" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="18.415" y1="-2.54" x2="19.685" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="18.415" y1="-2.54" x2="17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="17.145" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="15.875" y1="-2.54" x2="17.145" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="15.875" y1="-2.54" x2="15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="14.605" y1="-2.54" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="-2.54" x2="10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-2.54" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-2.54" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="1.905" x2="-17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="1.905" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.905" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="1.905" x2="15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="1.905" x2="17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="20.32" y1="1.905" x2="20.32" y2="-1.905" width="0.1524" layer="21"/>
<pad name="1" x="-19.05" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-19.05" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-16.51" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-16.51" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="-13.97" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="-13.97" y="1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="-11.43" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="-11.43" y="1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="-8.89" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="-8.89" y="1.27" drill="1.016" shape="octagon"/>
<pad name="11" x="-6.35" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="12" x="-6.35" y="1.27" drill="1.016" shape="octagon"/>
<pad name="13" x="-3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="14" x="-3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="15" x="-1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="16" x="-1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="17" x="1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="18" x="1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="19" x="3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="20" x="3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="21" x="6.35" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="22" x="6.35" y="1.27" drill="1.016" shape="octagon"/>
<pad name="23" x="8.89" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="24" x="8.89" y="1.27" drill="1.016" shape="octagon"/>
<pad name="25" x="11.43" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="26" x="11.43" y="1.27" drill="1.016" shape="octagon"/>
<pad name="27" x="13.97" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="28" x="13.97" y="1.27" drill="1.016" shape="octagon"/>
<pad name="29" x="16.51" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="30" x="16.51" y="1.27" drill="1.016" shape="octagon"/>
<pad name="31" x="19.05" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="32" x="19.05" y="1.27" drill="1.016" shape="octagon"/>
<text x="-20.32" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-20.32" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-19.304" y1="-1.524" x2="-18.796" y2="-1.016" layer="51"/>
<rectangle x1="-19.304" y1="1.016" x2="-18.796" y2="1.524" layer="51"/>
<rectangle x1="-16.764" y1="1.016" x2="-16.256" y2="1.524" layer="51"/>
<rectangle x1="-16.764" y1="-1.524" x2="-16.256" y2="-1.016" layer="51"/>
<rectangle x1="-14.224" y1="1.016" x2="-13.716" y2="1.524" layer="51"/>
<rectangle x1="-14.224" y1="-1.524" x2="-13.716" y2="-1.016" layer="51"/>
<rectangle x1="-11.684" y1="1.016" x2="-11.176" y2="1.524" layer="51"/>
<rectangle x1="-9.144" y1="1.016" x2="-8.636" y2="1.524" layer="51"/>
<rectangle x1="-6.604" y1="1.016" x2="-6.096" y2="1.524" layer="51"/>
<rectangle x1="-11.684" y1="-1.524" x2="-11.176" y2="-1.016" layer="51"/>
<rectangle x1="-9.144" y1="-1.524" x2="-8.636" y2="-1.016" layer="51"/>
<rectangle x1="-6.604" y1="-1.524" x2="-6.096" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="6.096" y1="1.016" x2="6.604" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="-1.524" x2="6.604" y2="-1.016" layer="51"/>
<rectangle x1="8.636" y1="1.016" x2="9.144" y2="1.524" layer="51"/>
<rectangle x1="8.636" y1="-1.524" x2="9.144" y2="-1.016" layer="51"/>
<rectangle x1="11.176" y1="1.016" x2="11.684" y2="1.524" layer="51"/>
<rectangle x1="11.176" y1="-1.524" x2="11.684" y2="-1.016" layer="51"/>
<rectangle x1="13.716" y1="1.016" x2="14.224" y2="1.524" layer="51"/>
<rectangle x1="13.716" y1="-1.524" x2="14.224" y2="-1.016" layer="51"/>
<rectangle x1="16.256" y1="1.016" x2="16.764" y2="1.524" layer="51"/>
<rectangle x1="16.256" y1="-1.524" x2="16.764" y2="-1.016" layer="51"/>
<rectangle x1="18.796" y1="1.016" x2="19.304" y2="1.524" layer="51"/>
<rectangle x1="18.796" y1="-1.524" x2="19.304" y2="-1.016" layer="51"/>
</package>
<package name="2X16/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-20.32" y1="-1.905" x2="-17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="-1.905" x2="-17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="0.635" x2="-20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="0.635" x2="-20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-19.05" y1="6.985" x2="-19.05" y2="1.27" width="0.762" layer="21"/>
<wire x1="-17.78" y1="-1.905" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="-1.905" x2="-15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="0.635" x2="-17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-16.51" y1="6.985" x2="-16.51" y2="1.27" width="0.762" layer="21"/>
<wire x1="-15.24" y1="-1.905" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="0.635" x2="-15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-13.97" y1="6.985" x2="-13.97" y2="1.27" width="0.762" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="6.985" x2="-11.43" y2="1.27" width="0.762" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="6.985" x2="-8.89" y2="1.27" width="0.762" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="6.985" x2="-6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="6.985" x2="-3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="6.985" x2="6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="0.635" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="6.985" x2="8.89" y2="1.27" width="0.762" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="12.7" y1="0.635" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="11.43" y1="6.985" x2="11.43" y2="1.27" width="0.762" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="15.24" y1="0.635" x2="12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="13.97" y1="6.985" x2="13.97" y2="1.27" width="0.762" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="17.78" y1="0.635" x2="15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="16.51" y1="6.985" x2="16.51" y2="1.27" width="0.762" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="20.32" y1="0.635" x2="17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="19.05" y1="6.985" x2="19.05" y2="1.27" width="0.762" layer="21"/>
<pad name="2" x="-19.05" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="4" x="-16.51" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="6" x="-13.97" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="8" x="-11.43" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="10" x="-8.89" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="12" x="-6.35" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="14" x="-3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="16" x="-1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="18" x="1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="20" x="3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="22" x="6.35" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="24" x="8.89" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="26" x="11.43" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="28" x="13.97" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="30" x="16.51" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="32" x="19.05" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="1" x="-19.05" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="3" x="-16.51" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="5" x="-13.97" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="7" x="-11.43" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="9" x="-8.89" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="11" x="-6.35" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="13" x="-3.81" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="15" x="-1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="17" x="1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="19" x="3.81" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="21" x="6.35" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="23" x="8.89" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="25" x="11.43" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="27" x="13.97" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="29" x="16.51" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="31" x="19.05" y="-6.35" drill="1.016" shape="octagon"/>
<text x="-20.955" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="22.225" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-19.431" y1="0.635" x2="-18.669" y2="1.143" layer="21"/>
<rectangle x1="-16.891" y1="0.635" x2="-16.129" y2="1.143" layer="21"/>
<rectangle x1="-14.351" y1="0.635" x2="-13.589" y2="1.143" layer="21"/>
<rectangle x1="-11.811" y1="0.635" x2="-11.049" y2="1.143" layer="21"/>
<rectangle x1="-9.271" y1="0.635" x2="-8.509" y2="1.143" layer="21"/>
<rectangle x1="-6.731" y1="0.635" x2="-5.969" y2="1.143" layer="21"/>
<rectangle x1="-4.191" y1="0.635" x2="-3.429" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="3.429" y1="0.635" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="5.969" y1="0.635" x2="6.731" y2="1.143" layer="21"/>
<rectangle x1="8.509" y1="0.635" x2="9.271" y2="1.143" layer="21"/>
<rectangle x1="11.049" y1="0.635" x2="11.811" y2="1.143" layer="21"/>
<rectangle x1="13.589" y1="0.635" x2="14.351" y2="1.143" layer="21"/>
<rectangle x1="16.129" y1="0.635" x2="16.891" y2="1.143" layer="21"/>
<rectangle x1="18.669" y1="0.635" x2="19.431" y2="1.143" layer="21"/>
<rectangle x1="-19.431" y1="-2.921" x2="-18.669" y2="-1.905" layer="21"/>
<rectangle x1="-16.891" y1="-2.921" x2="-16.129" y2="-1.905" layer="21"/>
<rectangle x1="-19.431" y1="-5.461" x2="-18.669" y2="-4.699" layer="21"/>
<rectangle x1="-19.431" y1="-4.699" x2="-18.669" y2="-2.921" layer="51"/>
<rectangle x1="-16.891" y1="-4.699" x2="-16.129" y2="-2.921" layer="51"/>
<rectangle x1="-16.891" y1="-5.461" x2="-16.129" y2="-4.699" layer="21"/>
<rectangle x1="-14.351" y1="-2.921" x2="-13.589" y2="-1.905" layer="21"/>
<rectangle x1="-11.811" y1="-2.921" x2="-11.049" y2="-1.905" layer="21"/>
<rectangle x1="-14.351" y1="-5.461" x2="-13.589" y2="-4.699" layer="21"/>
<rectangle x1="-14.351" y1="-4.699" x2="-13.589" y2="-2.921" layer="51"/>
<rectangle x1="-11.811" y1="-4.699" x2="-11.049" y2="-2.921" layer="51"/>
<rectangle x1="-11.811" y1="-5.461" x2="-11.049" y2="-4.699" layer="21"/>
<rectangle x1="-9.271" y1="-2.921" x2="-8.509" y2="-1.905" layer="21"/>
<rectangle x1="-9.271" y1="-5.461" x2="-8.509" y2="-4.699" layer="21"/>
<rectangle x1="-9.271" y1="-4.699" x2="-8.509" y2="-2.921" layer="51"/>
<rectangle x1="-6.731" y1="-2.921" x2="-5.969" y2="-1.905" layer="21"/>
<rectangle x1="-4.191" y1="-2.921" x2="-3.429" y2="-1.905" layer="21"/>
<rectangle x1="-6.731" y1="-5.461" x2="-5.969" y2="-4.699" layer="21"/>
<rectangle x1="-6.731" y1="-4.699" x2="-5.969" y2="-2.921" layer="51"/>
<rectangle x1="-4.191" y1="-4.699" x2="-3.429" y2="-2.921" layer="51"/>
<rectangle x1="-4.191" y1="-5.461" x2="-3.429" y2="-4.699" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
<rectangle x1="-1.651" y1="-5.461" x2="-0.889" y2="-4.699" layer="21"/>
<rectangle x1="-1.651" y1="-4.699" x2="-0.889" y2="-2.921" layer="51"/>
<rectangle x1="0.889" y1="-4.699" x2="1.651" y2="-2.921" layer="51"/>
<rectangle x1="0.889" y1="-5.461" x2="1.651" y2="-4.699" layer="21"/>
<rectangle x1="3.429" y1="-2.921" x2="4.191" y2="-1.905" layer="21"/>
<rectangle x1="3.429" y1="-5.461" x2="4.191" y2="-4.699" layer="21"/>
<rectangle x1="3.429" y1="-4.699" x2="4.191" y2="-2.921" layer="51"/>
<rectangle x1="5.969" y1="-2.921" x2="6.731" y2="-1.905" layer="21"/>
<rectangle x1="8.509" y1="-2.921" x2="9.271" y2="-1.905" layer="21"/>
<rectangle x1="5.969" y1="-5.461" x2="6.731" y2="-4.699" layer="21"/>
<rectangle x1="5.969" y1="-4.699" x2="6.731" y2="-2.921" layer="51"/>
<rectangle x1="8.509" y1="-4.699" x2="9.271" y2="-2.921" layer="51"/>
<rectangle x1="8.509" y1="-5.461" x2="9.271" y2="-4.699" layer="21"/>
<rectangle x1="11.049" y1="-2.921" x2="11.811" y2="-1.905" layer="21"/>
<rectangle x1="13.589" y1="-2.921" x2="14.351" y2="-1.905" layer="21"/>
<rectangle x1="11.049" y1="-5.461" x2="11.811" y2="-4.699" layer="21"/>
<rectangle x1="11.049" y1="-4.699" x2="11.811" y2="-2.921" layer="51"/>
<rectangle x1="13.589" y1="-4.699" x2="14.351" y2="-2.921" layer="51"/>
<rectangle x1="13.589" y1="-5.461" x2="14.351" y2="-4.699" layer="21"/>
<rectangle x1="16.129" y1="-2.921" x2="16.891" y2="-1.905" layer="21"/>
<rectangle x1="16.129" y1="-5.461" x2="16.891" y2="-4.699" layer="21"/>
<rectangle x1="16.129" y1="-4.699" x2="16.891" y2="-2.921" layer="51"/>
<rectangle x1="18.669" y1="-2.921" x2="19.431" y2="-1.905" layer="21"/>
<rectangle x1="18.669" y1="-4.699" x2="19.431" y2="-2.921" layer="51"/>
<rectangle x1="18.669" y1="-5.461" x2="19.431" y2="-4.699" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="PINH2X16">
<wire x1="-6.35" y1="-22.86" x2="8.89" y2="-22.86" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-22.86" x2="8.89" y2="20.32" width="0.4064" layer="94"/>
<wire x1="8.89" y1="20.32" x2="-6.35" y2="20.32" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="20.32" x2="-6.35" y2="-22.86" width="0.4064" layer="94"/>
<text x="-6.35" y="20.955" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-25.4" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="17.78" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="17.78" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="15.24" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="11" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="13" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="14" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="15" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="16" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="17" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="18" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="19" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="20" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="21" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="22" x="5.08" y="-7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="23" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="24" x="5.08" y="-10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="25" x="-2.54" y="-12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="26" x="5.08" y="-12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="27" x="-2.54" y="-15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="28" x="5.08" y="-15.24" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="29" x="-2.54" y="-17.78" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="30" x="5.08" y="-17.78" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="31" x="-2.54" y="-20.32" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="32" x="5.08" y="-20.32" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-2X16" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINH2X16" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X16">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="19" pad="19"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="20" pad="20"/>
<connect gate="A" pin="21" pad="21"/>
<connect gate="A" pin="22" pad="22"/>
<connect gate="A" pin="23" pad="23"/>
<connect gate="A" pin="24" pad="24"/>
<connect gate="A" pin="25" pad="25"/>
<connect gate="A" pin="26" pad="26"/>
<connect gate="A" pin="27" pad="27"/>
<connect gate="A" pin="28" pad="28"/>
<connect gate="A" pin="29" pad="29"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="30" pad="30"/>
<connect gate="A" pin="31" pad="31"/>
<connect gate="A" pin="32" pad="32"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="2X16/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="19" pad="19"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="20" pad="20"/>
<connect gate="A" pin="21" pad="21"/>
<connect gate="A" pin="22" pad="22"/>
<connect gate="A" pin="23" pad="23"/>
<connect gate="A" pin="24" pad="24"/>
<connect gate="A" pin="25" pad="25"/>
<connect gate="A" pin="26" pad="26"/>
<connect gate="A" pin="27" pad="27"/>
<connect gate="A" pin="28" pad="28"/>
<connect gate="A" pin="29" pad="29"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="30" pad="30"/>
<connect gate="A" pin="31" pad="31"/>
<connect gate="A" pin="32" pad="32"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Coord-MISC_Parts">
<packages>
<package name="282834-2">
<wire x1="-2.77" y1="-3.29" x2="-2.77" y2="3.21" width="0.127" layer="21"/>
<wire x1="-2.75" y1="3.25" x2="2.79" y2="3.25" width="0.127" layer="21"/>
<wire x1="2.83" y1="-3.29" x2="2.83" y2="3.21" width="0.127" layer="21"/>
<wire x1="-2.75" y1="-3.35" x2="2.79" y2="-3.35" width="0.127" layer="21"/>
<pad name="1" x="-1.25" y="0" drill="1.1"/>
<pad name="2" x="1.25" y="0" drill="1.1"/>
<wire x1="-2.7" y1="-2.7" x2="2.8" y2="-2.7" width="0.127" layer="21"/>
<wire x1="-2.7" y1="1" x2="2.8" y2="1" width="0.127" layer="21"/>
<wire x1="-2.7" y1="-1" x2="2.8" y2="-1" width="0.127" layer="21"/>
<wire x1="-2.1" y1="2.654" x2="-0.6" y2="2.654" width="0.127" layer="21"/>
<wire x1="0.6" y1="2.654" x2="2.1" y2="2.654" width="0.127" layer="21"/>
<wire x1="-0.6" y1="2.654" x2="-0.6" y2="3.2" width="0.127" layer="21"/>
<wire x1="-2.1" y1="2.654" x2="-2.1" y2="3.2" width="0.127" layer="21"/>
<wire x1="0.6" y1="2.654" x2="0.6" y2="3.2" width="0.127" layer="21"/>
<wire x1="2.1" y1="2.654" x2="2.1" y2="3.2" width="0.127" layer="21"/>
<text x="0.085" y="1.846" size="0.6096" layer="21" font="vector" align="center">282834-2</text>
<text x="-1.27" y="-2.23" size="0.8128" layer="21" align="bottom-center">1</text>
<text x="1.27" y="-2.23" size="0.8128" layer="21" align="bottom-center">2</text>
</package>
</packages>
<symbols>
<symbol name="282834-2">
<pin name="2" x="-12.7" y="-5.08" length="middle"/>
<pin name="1" x="-12.7" y="1.27" length="middle"/>
<wire x1="-7.62" y1="4.445" x2="-7.62" y2="-8.255" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-8.255" x2="3.175" y2="-8.255" width="0.254" layer="94"/>
<wire x1="3.175" y1="-8.255" x2="3.175" y2="4.445" width="0.254" layer="94"/>
<wire x1="3.175" y1="4.445" x2="-7.62" y2="4.445" width="0.254" layer="94"/>
<text x="-7.62" y="5.08" size="1.27" layer="94">&gt;NAME</text>
<text x="-5.8674" y="-9.906" size="1.27" layer="94">282834-2</text>
<circle x="-0.635" y="1.27" radius="1.419903125" width="0.254" layer="94"/>
<circle x="-0.635" y="-5.08" radius="1.419903125" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="282834-2">
<description>TERM BLOCK 2POS SIDE ENT 2.54MM</description>
<gates>
<gate name="G$1" symbol="282834-2" x="-1.27" y="-2.54"/>
</gates>
<devices>
<device name="" package="282834-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="A98333-ND " constant="no"/>
<attribute name="MPN" value="282834-2" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="SparkFun-Aesthetics" deviceset="FRAME-LETTER" device=""/>
<part name="JP1" library="adafruit" deviceset="BEAGLEBONE_OUTLINE" device=""/>
<part name="U1" library="adafruit" deviceset="PCA9685" device="" value="PCA9685"/>
<part name="SUPPLY1" library="SparkFun-Aesthetics" deviceset="VCC_1" device="" value="VDD_3.3"/>
<part name="GND3" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="FRAME2" library="SparkFun-Aesthetics" deviceset="FRAME-LETTER" device="NO_PACKAGE"/>
<part name="JP2" library="pinhead" deviceset="PINHD-2X16" device=""/>
<part name="R1" library="Seeed-OPL-Resistor" deviceset="RES-ARRAY" device="-1206" value="10k"/>
<part name="GND6" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="A0" library="Seeed-OPL-Resistor" deviceset="RES" device="-1206" value="0 Ω"/>
<part name="SUPPLY4" library="SparkFun-Aesthetics" deviceset="VCC_1" device="" value="VDD_3.3"/>
<part name="GND7" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="A1" library="Seeed-OPL-Resistor" deviceset="RES" device="-1206" value="0 Ω"/>
<part name="SUPPLY5" library="SparkFun-Aesthetics" deviceset="VCC_1" device="" value="VDD_3.3"/>
<part name="GND8" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="A2" library="Seeed-OPL-Resistor" deviceset="RES" device="-1206" value="0 Ω"/>
<part name="SUPPLY6" library="SparkFun-Aesthetics" deviceset="VCC_1" device="" value="VDD_3.3"/>
<part name="GND9" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="A3" library="Seeed-OPL-Resistor" deviceset="RES" device="-1206" value="0 Ω"/>
<part name="SUPPLY7" library="SparkFun-Aesthetics" deviceset="VCC_1" device="" value="VDD_3.3"/>
<part name="GND10" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="A4" library="Seeed-OPL-Resistor" deviceset="RES" device="-1206" value="0 Ω"/>
<part name="SUPPLY8" library="SparkFun-Aesthetics" deviceset="VCC_1" device="" value="VDD_3.3"/>
<part name="GND11" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="A5" library="Seeed-OPL-Resistor" deviceset="RES" device="-1206" value="0 Ω"/>
<part name="SUPPLY9" library="SparkFun-Aesthetics" deviceset="VCC_1" device="" value="VDD_3.3"/>
<part name="GND12" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND13" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND4" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND5" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND1" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND2" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="SUPPLY2" library="SparkFun-Aesthetics" deviceset="VCC_1" device="" value="VDD_3.3"/>
<part name="SUPPLY3" library="SparkFun-Aesthetics" deviceset="VCC_1" device="" value="VDD_3.3"/>
<part name="Q1" library="Seeed-OPL-Transistor" deviceset="MOSFET-N" device=""/>
<part name="C1" library="SparkFun-Capacitors" deviceset="CAP" device="0603-CAP" value="100pF"/>
<part name="D1" library="Seeed-OPL-Diode" deviceset="SMD-DIODE-SCHOTTKY-20V-1A(SOD-123)" device="" value="20V-1A"/>
<part name="GND14" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="SUPPLY10" library="SparkFun-Aesthetics" deviceset="VCC_2" device="" value="VMOT"/>
<part name="SUPPLY14" library="SparkFun-Aesthetics" deviceset="VCC_2" device="" value="VMOT"/>
<part name="Q2" library="Seeed-OPL-Transistor" deviceset="MOSFET-N" device=""/>
<part name="C2" library="SparkFun-Capacitors" deviceset="CAP" device="0603-CAP" value="100pF"/>
<part name="D2" library="Seeed-OPL-Diode" deviceset="SMD-DIODE-SCHOTTKY-20V-1A(SOD-123)" device="" value="20V-1A"/>
<part name="GND15" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="SUPPLY11" library="SparkFun-Aesthetics" deviceset="VCC_2" device="" value="VMOT"/>
<part name="Q3" library="Seeed-OPL-Transistor" deviceset="MOSFET-N" device=""/>
<part name="C3" library="SparkFun-Capacitors" deviceset="CAP" device="0603-CAP" value="100pF"/>
<part name="D3" library="Seeed-OPL-Diode" deviceset="SMD-DIODE-SCHOTTKY-20V-1A(SOD-123)" device="" value="20V-1A"/>
<part name="GND16" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="SUPPLY12" library="SparkFun-Aesthetics" deviceset="VCC_2" device="" value="VMOT"/>
<part name="Q4" library="Seeed-OPL-Transistor" deviceset="MOSFET-N" device=""/>
<part name="C4" library="SparkFun-Capacitors" deviceset="CAP" device="0603-CAP" value="100pF"/>
<part name="D4" library="Seeed-OPL-Diode" deviceset="SMD-DIODE-SCHOTTKY-20V-1A(SOD-123)" device="" value="20V-1A"/>
<part name="GND17" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="SUPPLY13" library="SparkFun-Aesthetics" deviceset="VCC_2" device="" value="VMOT"/>
<part name="Q5" library="Seeed-OPL-Transistor" deviceset="MOSFET-N" device=""/>
<part name="C5" library="SparkFun-Capacitors" deviceset="CAP" device="0603-CAP" value="100pF"/>
<part name="D5" library="Seeed-OPL-Diode" deviceset="SMD-DIODE-SCHOTTKY-20V-1A(SOD-123)" device="" value="20V-1A"/>
<part name="GND18" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="SUPPLY15" library="SparkFun-Aesthetics" deviceset="VCC_2" device="" value="VMOT"/>
<part name="Q6" library="Seeed-OPL-Transistor" deviceset="MOSFET-N" device=""/>
<part name="C6" library="SparkFun-Capacitors" deviceset="CAP" device="0603-CAP" value="100pF"/>
<part name="D6" library="Seeed-OPL-Diode" deviceset="SMD-DIODE-SCHOTTKY-20V-1A(SOD-123)" device="" value="20V-1A"/>
<part name="GND19" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="SUPPLY16" library="SparkFun-Aesthetics" deviceset="VCC_2" device="" value="VMOT"/>
<part name="Q7" library="Seeed-OPL-Transistor" deviceset="MOSFET-N" device=""/>
<part name="C7" library="SparkFun-Capacitors" deviceset="CAP" device="0603-CAP" value="100pF"/>
<part name="D7" library="Seeed-OPL-Diode" deviceset="SMD-DIODE-SCHOTTKY-20V-1A(SOD-123)" device="" value="20V-1A"/>
<part name="GND20" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="SUPPLY17" library="SparkFun-Aesthetics" deviceset="VCC_2" device="" value="VMOT"/>
<part name="Q8" library="Seeed-OPL-Transistor" deviceset="MOSFET-N" device=""/>
<part name="C8" library="SparkFun-Capacitors" deviceset="CAP" device="0603-CAP" value="100pF"/>
<part name="D8" library="Seeed-OPL-Diode" deviceset="SMD-DIODE-SCHOTTKY-20V-1A(SOD-123)" device="" value="20V-1A"/>
<part name="GND21" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="SUPPLY18" library="SparkFun-Aesthetics" deviceset="VCC_2" device="" value="VMOT"/>
<part name="Q9" library="Seeed-OPL-Transistor" deviceset="MOSFET-N" device=""/>
<part name="C9" library="SparkFun-Capacitors" deviceset="CAP" device="0603-CAP" value="100pF"/>
<part name="D9" library="Seeed-OPL-Diode" deviceset="SMD-DIODE-SCHOTTKY-20V-1A(SOD-123)" device="" value="20V-1A"/>
<part name="GND22" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="SUPPLY19" library="SparkFun-Aesthetics" deviceset="VCC_2" device="" value="VMOT"/>
<part name="Q10" library="Seeed-OPL-Transistor" deviceset="MOSFET-N" device=""/>
<part name="C10" library="SparkFun-Capacitors" deviceset="CAP" device="0603-CAP" value="100pF"/>
<part name="D10" library="Seeed-OPL-Diode" deviceset="SMD-DIODE-SCHOTTKY-20V-1A(SOD-123)" device="" value="20V-1A"/>
<part name="GND23" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="SUPPLY20" library="SparkFun-Aesthetics" deviceset="VCC_2" device="" value="VMOT"/>
<part name="Q11" library="Seeed-OPL-Transistor" deviceset="MOSFET-N" device=""/>
<part name="C11" library="SparkFun-Capacitors" deviceset="CAP" device="0603-CAP" value="100pF"/>
<part name="D11" library="Seeed-OPL-Diode" deviceset="SMD-DIODE-SCHOTTKY-20V-1A(SOD-123)" device="" value="20V-1A"/>
<part name="GND24" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="SUPPLY21" library="SparkFun-Aesthetics" deviceset="VCC_2" device="" value="VMOT"/>
<part name="Q12" library="Seeed-OPL-Transistor" deviceset="MOSFET-N" device=""/>
<part name="C12" library="SparkFun-Capacitors" deviceset="CAP" device="0603-CAP" value="100pF"/>
<part name="D12" library="Seeed-OPL-Diode" deviceset="SMD-DIODE-SCHOTTKY-20V-1A(SOD-123)" device="" value="20V-1A"/>
<part name="GND25" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="SUPPLY22" library="SparkFun-Aesthetics" deviceset="VCC_2" device="" value="VMOT"/>
<part name="Q13" library="Seeed-OPL-Transistor" deviceset="MOSFET-N" device=""/>
<part name="C13" library="SparkFun-Capacitors" deviceset="CAP" device="0603-CAP" value="100pF"/>
<part name="D13" library="Seeed-OPL-Diode" deviceset="SMD-DIODE-SCHOTTKY-20V-1A(SOD-123)" device="" value="20V-1A"/>
<part name="GND26" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="SUPPLY23" library="SparkFun-Aesthetics" deviceset="VCC_2" device="" value="VMOT"/>
<part name="Q14" library="Seeed-OPL-Transistor" deviceset="MOSFET-N" device=""/>
<part name="C14" library="SparkFun-Capacitors" deviceset="CAP" device="0603-CAP" value="100pF"/>
<part name="D14" library="Seeed-OPL-Diode" deviceset="SMD-DIODE-SCHOTTKY-20V-1A(SOD-123)" device="" value="20V-1A"/>
<part name="GND27" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="SUPPLY24" library="SparkFun-Aesthetics" deviceset="VCC_2" device="" value="VMOT"/>
<part name="Q15" library="Seeed-OPL-Transistor" deviceset="MOSFET-N" device=""/>
<part name="C15" library="SparkFun-Capacitors" deviceset="CAP" device="0603-CAP" value="100pF"/>
<part name="D15" library="Seeed-OPL-Diode" deviceset="SMD-DIODE-SCHOTTKY-20V-1A(SOD-123)" device="" value="20V-1A"/>
<part name="GND28" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="SUPPLY25" library="SparkFun-Aesthetics" deviceset="VCC_2" device="" value="VMOT"/>
<part name="Q16" library="Seeed-OPL-Transistor" deviceset="MOSFET-N" device=""/>
<part name="C16" library="SparkFun-Capacitors" deviceset="CAP" device="0603-CAP" value="100pF"/>
<part name="D16" library="Seeed-OPL-Diode" deviceset="SMD-DIODE-SCHOTTKY-20V-1A(SOD-123)" device="" value="20V-1A"/>
<part name="GND29" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="SUPPLY26" library="SparkFun-Aesthetics" deviceset="VCC_2" device="" value="VMOT"/>
<part name="R8" library="Seeed-OPL-Resistor" deviceset="RES-ARRAY" device="-1206" value="10k"/>
<part name="R9" library="Seeed-OPL-Resistor" deviceset="RES-ARRAY" device="-1206" value="10k"/>
<part name="SUPPLY27" library="SparkFun-Aesthetics" deviceset="VCC_2" device="" value="VMOT"/>
<part name="GND30" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="JP3" library="Coord-MISC_Parts" deviceset="282834-2" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="167.64" y="10.16" size="2.54" layer="91">Christopher Earley (coord.space)</text>
<text x="163.83" y="90.17" size="1.778" layer="91">0 Ω pull-ups can be shorted by the user to
hardcode the I2C hardware address of the 
PCA9685. By default the address will be 40h.</text>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="FRAME1" gate="V" x="147.32" y="0"/>
<instance part="JP1" gate="G$1" x="64.77" y="105.41"/>
<instance part="U1" gate="G$1" x="184.15" y="139.7"/>
<instance part="SUPPLY1" gate="G$2" x="142.24" y="170.18"/>
<instance part="GND3" gate="1" x="154.94" y="109.22"/>
<instance part="R1" gate="A" x="154.94" y="156.21" rot="R90"/>
<instance part="R1" gate="B" x="148.59" y="156.21" rot="R90"/>
<instance part="R1" gate="C" x="154.94" y="128.27" rot="R270"/>
<instance part="R1" gate="D" x="142.24" y="153.67" rot="R90"/>
<instance part="GND6" gate="1" x="152.4" y="54.61"/>
<instance part="A0" gate="R" x="152.4" y="76.2" rot="R90"/>
<instance part="SUPPLY4" gate="G$2" x="152.4" y="82.55"/>
<instance part="GND7" gate="1" x="166.37" y="54.61"/>
<instance part="A1" gate="R" x="166.37" y="76.2" rot="R90"/>
<instance part="SUPPLY5" gate="G$2" x="166.37" y="82.55"/>
<instance part="GND8" gate="1" x="180.34" y="54.61"/>
<instance part="A2" gate="R" x="180.34" y="76.2" rot="R90"/>
<instance part="SUPPLY6" gate="G$2" x="180.34" y="82.55"/>
<instance part="GND9" gate="1" x="194.31" y="54.61"/>
<instance part="A3" gate="R" x="194.31" y="76.2" rot="R90"/>
<instance part="SUPPLY7" gate="G$2" x="194.31" y="82.55"/>
<instance part="GND10" gate="1" x="208.28" y="54.61"/>
<instance part="A4" gate="R" x="208.28" y="76.2" rot="R90"/>
<instance part="SUPPLY8" gate="G$2" x="208.28" y="82.55"/>
<instance part="GND11" gate="1" x="222.25" y="54.61"/>
<instance part="A5" gate="R" x="222.25" y="76.2" rot="R270"/>
<instance part="SUPPLY9" gate="G$2" x="222.25" y="82.55"/>
<instance part="GND12" gate="1" x="29.21" y="39.37"/>
<instance part="GND13" gate="1" x="100.33" y="39.37"/>
<instance part="GND4" gate="1" x="29.21" y="102.87" rot="R180"/>
<instance part="GND5" gate="1" x="100.33" y="102.87" rot="R180"/>
<instance part="GND1" gate="1" x="29.21" y="163.83"/>
<instance part="GND2" gate="1" x="100.33" y="163.83"/>
<instance part="SUPPLY2" gate="G$2" x="16.51" y="100.33"/>
<instance part="SUPPLY3" gate="G$2" x="105.41" y="100.33"/>
<instance part="R8" gate="A" x="152.4" y="63.5" rot="R90"/>
<instance part="R8" gate="B" x="166.37" y="63.5" rot="R90"/>
<instance part="R8" gate="C" x="180.34" y="63.5" rot="R90"/>
<instance part="R8" gate="D" x="194.31" y="63.5" rot="R90"/>
<instance part="R9" gate="A" x="208.28" y="63.5" rot="R270"/>
<instance part="R9" gate="B" x="222.25" y="63.5" rot="R270"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="154.94" y1="124.46" x2="154.94" y2="116.84" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="154.94" y1="116.84" x2="154.94" y2="111.76" width="0.1524" layer="91"/>
<wire x1="163.83" y1="116.84" x2="154.94" y2="116.84" width="0.1524" layer="91"/>
<junction x="154.94" y="116.84"/>
<pinref part="R1" gate="C" pin="2"/>
</segment>
<segment>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="208.28" y1="59.69" x2="208.28" y2="57.15" width="0.1524" layer="91"/>
<pinref part="R9" gate="A" pin="2"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="GND@4"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="36.83" y1="44.45" x2="29.21" y2="44.45" width="0.1524" layer="91"/>
<wire x1="29.21" y1="44.45" x2="29.21" y2="41.91" width="0.1524" layer="91"/>
<pinref part="JP1" gate="G$1" pin="GND@3"/>
<wire x1="36.83" y1="46.99" x2="29.21" y2="46.99" width="0.1524" layer="91"/>
<wire x1="29.21" y1="46.99" x2="29.21" y2="44.45" width="0.1524" layer="91"/>
<junction x="29.21" y="44.45"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="GND@6"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="92.71" y1="44.45" x2="100.33" y2="44.45" width="0.1524" layer="91"/>
<wire x1="100.33" y1="44.45" x2="100.33" y2="41.91" width="0.1524" layer="91"/>
<pinref part="JP1" gate="G$1" pin="GND@5"/>
<wire x1="92.71" y1="46.99" x2="100.33" y2="46.99" width="0.1524" layer="91"/>
<wire x1="100.33" y1="46.99" x2="100.33" y2="44.45" width="0.1524" layer="91"/>
<junction x="100.33" y="44.45"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="GND@2"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="92.71" y1="100.33" x2="100.33" y2="100.33" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="GND@7"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="36.83" y1="166.37" x2="29.21" y2="166.37" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="GND@8"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="92.71" y1="166.37" x2="100.33" y2="166.37" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="GND@1"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="36.83" y1="100.33" x2="29.21" y2="100.33" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R8" gate="A" pin="1"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="152.4" y1="59.69" x2="152.4" y2="57.15" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R8" gate="B" pin="1"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="166.37" y1="59.69" x2="166.37" y2="57.15" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R8" gate="C" pin="1"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="180.34" y1="59.69" x2="180.34" y2="57.15" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND9" gate="1" pin="GND"/>
<pinref part="R8" gate="D" pin="1"/>
<wire x1="194.31" y1="57.15" x2="194.31" y2="59.69" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND11" gate="1" pin="GND"/>
<pinref part="R9" gate="B" pin="2"/>
<wire x1="222.25" y1="57.15" x2="222.25" y2="59.69" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC_1" class="0">
<segment>
<pinref part="SUPPLY1" gate="G$2" pin="VCC_1"/>
<wire x1="142.24" y1="157.48" x2="142.24" y2="162.56" width="0.1524" layer="91"/>
<wire x1="142.24" y1="162.56" x2="142.24" y2="170.18" width="0.1524" layer="91"/>
<wire x1="142.24" y1="162.56" x2="148.59" y2="162.56" width="0.1524" layer="91"/>
<junction x="142.24" y="162.56"/>
<wire x1="148.59" y1="162.56" x2="154.94" y2="162.56" width="0.1524" layer="91"/>
<wire x1="154.94" y1="162.56" x2="161.29" y2="162.56" width="0.1524" layer="91"/>
<wire x1="161.29" y1="162.56" x2="161.29" y2="154.94" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VDD"/>
<wire x1="161.29" y1="154.94" x2="163.83" y2="154.94" width="0.1524" layer="91"/>
<pinref part="R1" gate="B" pin="2"/>
<wire x1="148.59" y1="160.02" x2="148.59" y2="162.56" width="0.1524" layer="91"/>
<junction x="148.59" y="162.56"/>
<pinref part="R1" gate="D" pin="2"/>
<pinref part="R1" gate="A" pin="2"/>
<wire x1="154.94" y1="160.02" x2="154.94" y2="162.56" width="0.1524" layer="91"/>
<junction x="154.94" y="162.56"/>
</segment>
<segment>
<pinref part="SUPPLY9" gate="G$2" pin="VCC_1"/>
<pinref part="A5" gate="R" pin="1"/>
<wire x1="222.25" y1="82.55" x2="222.25" y2="80.01" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="VDD_3V3EXP@1"/>
<wire x1="36.83" y1="97.79" x2="16.51" y2="97.79" width="0.1524" layer="91"/>
<pinref part="SUPPLY2" gate="G$2" pin="VCC_1"/>
<wire x1="16.51" y1="97.79" x2="16.51" y2="100.33" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="VDD_3V3EXP@2"/>
<wire x1="92.71" y1="97.79" x2="105.41" y2="97.79" width="0.1524" layer="91"/>
<pinref part="SUPPLY3" gate="G$2" pin="VCC_1"/>
<wire x1="105.41" y1="97.79" x2="105.41" y2="100.33" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="A4" gate="R" pin="2"/>
<pinref part="SUPPLY8" gate="G$2" pin="VCC_1"/>
<wire x1="208.28" y1="80.01" x2="208.28" y2="82.55" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="A0" gate="R" pin="2"/>
<pinref part="SUPPLY4" gate="G$2" pin="VCC_1"/>
<wire x1="152.4" y1="80.01" x2="152.4" y2="82.55" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY5" gate="G$2" pin="VCC_1"/>
<pinref part="A1" gate="R" pin="2"/>
<wire x1="166.37" y1="82.55" x2="166.37" y2="80.01" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="A2" gate="R" pin="2"/>
<pinref part="SUPPLY6" gate="G$2" pin="VCC_1"/>
<wire x1="180.34" y1="80.01" x2="180.34" y2="82.55" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="A3" gate="R" pin="2"/>
<pinref part="SUPPLY7" gate="G$2" pin="VCC_1"/>
<wire x1="194.31" y1="80.01" x2="194.31" y2="82.55" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWM_0" class="0">
<segment>
<wire x1="204.47" y1="154.94" x2="215.9" y2="154.94" width="0.1524" layer="91"/>
<label x="205.74" y="154.94" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PWM0"/>
</segment>
</net>
<net name="PWM_1" class="0">
<segment>
<wire x1="204.47" y1="152.4" x2="215.9" y2="152.4" width="0.1524" layer="91"/>
<label x="205.74" y="152.4" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PWM1"/>
</segment>
</net>
<net name="PWM_2" class="0">
<segment>
<wire x1="204.47" y1="149.86" x2="215.9" y2="149.86" width="0.1524" layer="91"/>
<label x="205.74" y="149.86" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PWM2"/>
</segment>
</net>
<net name="PWM_3" class="0">
<segment>
<wire x1="204.47" y1="147.32" x2="215.9" y2="147.32" width="0.1524" layer="91"/>
<label x="205.74" y="147.32" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PWM3"/>
</segment>
</net>
<net name="PWM_4" class="0">
<segment>
<wire x1="204.47" y1="144.78" x2="215.9" y2="144.78" width="0.1524" layer="91"/>
<label x="205.74" y="144.78" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PWM4"/>
</segment>
</net>
<net name="PWM_5" class="0">
<segment>
<wire x1="204.47" y1="142.24" x2="215.9" y2="142.24" width="0.1524" layer="91"/>
<label x="205.74" y="142.24" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PWM5"/>
</segment>
</net>
<net name="PWM_6" class="0">
<segment>
<wire x1="204.47" y1="139.7" x2="215.9" y2="139.7" width="0.1524" layer="91"/>
<label x="205.74" y="139.7" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PWM6"/>
</segment>
</net>
<net name="PWM_7" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PWM7"/>
<wire x1="204.47" y1="137.16" x2="215.9" y2="137.16" width="0.1524" layer="91"/>
<label x="205.74" y="137.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM_8" class="0">
<segment>
<wire x1="204.47" y1="134.62" x2="215.9" y2="134.62" width="0.1524" layer="91"/>
<label x="205.74" y="134.62" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PWM8"/>
</segment>
</net>
<net name="PWM_9" class="0">
<segment>
<wire x1="204.47" y1="132.08" x2="215.9" y2="132.08" width="0.1524" layer="91"/>
<label x="205.74" y="132.08" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PWM9"/>
</segment>
</net>
<net name="PWM_10" class="0">
<segment>
<wire x1="204.47" y1="129.54" x2="215.9" y2="129.54" width="0.1524" layer="91"/>
<label x="205.74" y="129.54" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PWM10"/>
</segment>
</net>
<net name="PWM_11" class="0">
<segment>
<wire x1="204.47" y1="127" x2="215.9" y2="127" width="0.1524" layer="91"/>
<label x="205.74" y="127" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PWM11"/>
</segment>
</net>
<net name="PWM_12" class="0">
<segment>
<wire x1="204.47" y1="124.46" x2="215.9" y2="124.46" width="0.1524" layer="91"/>
<label x="205.74" y="124.46" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PWM12"/>
</segment>
</net>
<net name="PWM_13" class="0">
<segment>
<wire x1="204.47" y1="121.92" x2="215.9" y2="121.92" width="0.1524" layer="91"/>
<label x="205.74" y="121.92" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PWM13"/>
</segment>
</net>
<net name="PWM_14" class="0">
<segment>
<wire x1="204.47" y1="119.38" x2="215.9" y2="119.38" width="0.1524" layer="91"/>
<label x="205.74" y="119.38" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PWM14"/>
</segment>
</net>
<net name="PWM_15" class="0">
<segment>
<wire x1="204.47" y1="116.84" x2="215.9" y2="116.84" width="0.1524" layer="91"/>
<label x="205.74" y="116.84" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="PWM15"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SDA"/>
<pinref part="R1" gate="A" pin="1"/>
<wire x1="163.83" y1="149.86" x2="154.94" y2="149.86" width="0.1524" layer="91"/>
<wire x1="154.94" y1="149.86" x2="154.94" y2="152.4" width="0.1524" layer="91"/>
<wire x1="154.94" y1="149.86" x2="151.13" y2="149.86" width="0.1524" layer="91"/>
<junction x="154.94" y="149.86"/>
<label x="149.86" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="I2C2_SDA"/>
<wire x1="92.71" y1="77.47" x2="106.68" y2="77.47" width="0.1524" layer="91"/>
<label x="101.6" y="77.47" size="1.778" layer="95"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SCL"/>
<pinref part="R1" gate="B" pin="1"/>
<wire x1="163.83" y1="147.32" x2="148.59" y2="147.32" width="0.1524" layer="91"/>
<wire x1="148.59" y1="147.32" x2="148.59" y2="152.4" width="0.1524" layer="91"/>
<wire x1="148.59" y1="147.32" x2="144.78" y2="147.32" width="0.1524" layer="91"/>
<junction x="148.59" y="147.32"/>
<label x="143.51" y="147.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="I2C2_SCL"/>
<wire x1="36.83" y1="77.47" x2="22.86" y2="77.47" width="0.1524" layer="91"/>
<label x="22.86" y="77.47" size="1.778" layer="95"/>
</segment>
</net>
<net name="EXTCLK" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="EXTCLK"/>
<wire x1="163.83" y1="137.16" x2="154.94" y2="137.16" width="0.1524" layer="91"/>
<wire x1="154.94" y1="137.16" x2="154.94" y2="132.08" width="0.1524" layer="91"/>
<wire x1="154.94" y1="137.16" x2="143.51" y2="137.16" width="0.1524" layer="91"/>
<junction x="154.94" y="137.16"/>
<label x="143.51" y="137.16" size="1.778" layer="95"/>
<pinref part="R1" gate="C" pin="1"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="EHRPWM1A"/>
<wire x1="92.71" y1="85.09" x2="106.68" y2="85.09" width="0.1524" layer="91"/>
<label x="97.79" y="85.09" size="1.778" layer="95"/>
</segment>
</net>
<net name="A0" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="A0"/>
<wire x1="163.83" y1="134.62" x2="158.75" y2="134.62" width="0.1524" layer="91"/>
<label x="158.75" y="134.62" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="152.4" y1="69.85" x2="148.59" y2="69.85" width="0.1524" layer="91"/>
<label x="148.59" y="69.85" size="1.778" layer="95"/>
<pinref part="A0" gate="R" pin="1"/>
<wire x1="152.4" y1="69.85" x2="152.4" y2="72.39" width="0.1524" layer="91"/>
<pinref part="R8" gate="A" pin="2"/>
<wire x1="152.4" y1="69.85" x2="152.4" y2="67.31" width="0.1524" layer="91"/>
<junction x="152.4" y="69.85"/>
</segment>
</net>
<net name="A1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="A1"/>
<wire x1="163.83" y1="132.08" x2="158.75" y2="132.08" width="0.1524" layer="91"/>
<label x="158.75" y="132.08" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="166.37" y1="69.85" x2="162.56" y2="69.85" width="0.1524" layer="91"/>
<label x="162.56" y="69.85" size="1.778" layer="95"/>
<pinref part="A1" gate="R" pin="1"/>
<wire x1="166.37" y1="72.39" x2="166.37" y2="69.85" width="0.1524" layer="91"/>
<pinref part="R8" gate="B" pin="2"/>
<wire x1="166.37" y1="67.31" x2="166.37" y2="69.85" width="0.1524" layer="91"/>
<junction x="166.37" y="69.85"/>
</segment>
</net>
<net name="A2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="A2"/>
<wire x1="163.83" y1="129.54" x2="158.75" y2="129.54" width="0.1524" layer="91"/>
<label x="158.75" y="129.54" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="180.34" y1="69.85" x2="176.53" y2="69.85" width="0.1524" layer="91"/>
<label x="176.53" y="69.85" size="1.778" layer="95"/>
<pinref part="A2" gate="R" pin="1"/>
<wire x1="180.34" y1="69.85" x2="180.34" y2="72.39" width="0.1524" layer="91"/>
<pinref part="R8" gate="C" pin="2"/>
<wire x1="180.34" y1="69.85" x2="180.34" y2="67.31" width="0.1524" layer="91"/>
<junction x="180.34" y="69.85"/>
</segment>
</net>
<net name="A3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="A3"/>
<wire x1="163.83" y1="127" x2="158.75" y2="127" width="0.1524" layer="91"/>
<label x="158.75" y="127" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="194.31" y1="69.85" x2="190.5" y2="69.85" width="0.1524" layer="91"/>
<label x="190.5" y="69.85" size="1.778" layer="95"/>
<pinref part="A3" gate="R" pin="1"/>
<wire x1="194.31" y1="72.39" x2="194.31" y2="69.85" width="0.1524" layer="91"/>
<pinref part="R8" gate="D" pin="2"/>
<wire x1="194.31" y1="67.31" x2="194.31" y2="69.85" width="0.1524" layer="91"/>
<junction x="194.31" y="69.85"/>
</segment>
</net>
<net name="A4" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="A4"/>
<wire x1="163.83" y1="124.46" x2="158.75" y2="124.46" width="0.1524" layer="91"/>
<label x="158.75" y="124.46" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="A4" gate="R" pin="1"/>
<wire x1="208.28" y1="72.39" x2="208.28" y2="69.85" width="0.1524" layer="91"/>
<wire x1="208.28" y1="69.85" x2="208.28" y2="67.31" width="0.1524" layer="91"/>
<wire x1="208.28" y1="69.85" x2="204.47" y2="69.85" width="0.1524" layer="91"/>
<junction x="208.28" y="69.85"/>
<label x="204.47" y="69.85" size="1.778" layer="95"/>
<pinref part="R9" gate="A" pin="1"/>
</segment>
</net>
<net name="A5" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="A5"/>
<wire x1="163.83" y1="121.92" x2="158.75" y2="121.92" width="0.1524" layer="91"/>
<label x="158.75" y="121.92" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="A5" gate="R" pin="2"/>
<wire x1="222.25" y1="72.39" x2="222.25" y2="69.85" width="0.1524" layer="91"/>
<wire x1="222.25" y1="69.85" x2="218.44" y2="69.85" width="0.1524" layer="91"/>
<label x="218.44" y="69.85" size="1.778" layer="95"/>
<pinref part="R9" gate="B" pin="1"/>
<wire x1="222.25" y1="67.31" x2="222.25" y2="69.85" width="0.1524" layer="91"/>
<junction x="222.25" y="69.85"/>
</segment>
</net>
<net name="OE" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="#OE"/>
<wire x1="163.83" y1="142.24" x2="142.24" y2="142.24" width="0.1524" layer="91"/>
<pinref part="R1" gate="D" pin="1"/>
<wire x1="142.24" y1="142.24" x2="135.89" y2="142.24" width="0.1524" layer="91"/>
<wire x1="142.24" y1="149.86" x2="142.24" y2="142.24" width="0.1524" layer="91"/>
<junction x="142.24" y="142.24"/>
<label x="135.89" y="142.24" size="1.778" layer="95"/>
<wire x1="135.89" y1="143.51" x2="135.89" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="GPIO1_17"/>
<wire x1="36.83" y1="72.39" x2="22.86" y2="72.39" width="0.1524" layer="91"/>
<label x="22.86" y="72.39" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="179.07" y="72.39" size="1.778" layer="91">Out to Motor Array</text>
<text x="180.594" y="148.336" size="1.778" layer="91">VMOT Conn
2-pos screw terminal</text>
<text x="17.78" y="7.62" size="2.54" layer="91">Christopher Earley (coord.space)</text>
</plain>
<instances>
<instance part="FRAME2" gate="G$1" x="0" y="0"/>
<instance part="FRAME2" gate="V" x="0" y="0"/>
<instance part="JP2" gate="A" x="187.96" y="99.06"/>
<instance part="Q1" gate="Q" x="19.05" y="156.21"/>
<instance part="C1" gate="G$1" x="24.13" y="168.91"/>
<instance part="D1" gate="G$1" x="13.97" y="170.18" rot="R90"/>
<instance part="GND14" gate="1" x="19.05" y="140.97"/>
<instance part="SUPPLY10" gate="G$1" x="19.05" y="177.8"/>
<instance part="SUPPLY14" gate="G$1" x="200.66" y="118.11"/>
<instance part="Q2" gate="Q" x="50.8" y="156.21"/>
<instance part="C2" gate="G$1" x="55.88" y="168.91"/>
<instance part="D2" gate="G$1" x="45.72" y="170.18" rot="R90"/>
<instance part="GND15" gate="1" x="50.8" y="140.97"/>
<instance part="SUPPLY11" gate="G$1" x="50.8" y="177.8"/>
<instance part="Q3" gate="Q" x="82.55" y="156.21"/>
<instance part="C3" gate="G$1" x="87.63" y="168.91"/>
<instance part="D3" gate="G$1" x="77.47" y="170.18" rot="R90"/>
<instance part="GND16" gate="1" x="82.55" y="140.97"/>
<instance part="SUPPLY12" gate="G$1" x="82.55" y="177.8"/>
<instance part="Q4" gate="Q" x="114.3" y="156.21"/>
<instance part="C4" gate="G$1" x="119.38" y="168.91"/>
<instance part="D4" gate="G$1" x="109.22" y="170.18" rot="R90"/>
<instance part="GND17" gate="1" x="114.3" y="140.97"/>
<instance part="SUPPLY13" gate="G$1" x="114.3" y="177.8"/>
<instance part="Q5" gate="Q" x="19.05" y="107.95"/>
<instance part="C5" gate="G$1" x="24.13" y="120.65"/>
<instance part="D5" gate="G$1" x="13.97" y="121.92" rot="R90"/>
<instance part="GND18" gate="1" x="19.05" y="92.71"/>
<instance part="SUPPLY15" gate="G$1" x="19.05" y="129.54"/>
<instance part="Q6" gate="Q" x="50.8" y="107.95"/>
<instance part="C6" gate="G$1" x="55.88" y="120.65"/>
<instance part="D6" gate="G$1" x="45.72" y="121.92" rot="R90"/>
<instance part="GND19" gate="1" x="50.8" y="92.71"/>
<instance part="SUPPLY16" gate="G$1" x="50.8" y="129.54"/>
<instance part="Q7" gate="Q" x="82.55" y="107.95"/>
<instance part="C7" gate="G$1" x="87.63" y="120.65"/>
<instance part="D7" gate="G$1" x="77.47" y="121.92" rot="R90"/>
<instance part="GND20" gate="1" x="82.55" y="92.71"/>
<instance part="SUPPLY17" gate="G$1" x="82.55" y="129.54"/>
<instance part="Q8" gate="Q" x="114.3" y="107.95"/>
<instance part="C8" gate="G$1" x="119.38" y="120.65"/>
<instance part="D8" gate="G$1" x="109.22" y="121.92" rot="R90"/>
<instance part="GND21" gate="1" x="114.3" y="92.71"/>
<instance part="SUPPLY18" gate="G$1" x="114.3" y="129.54"/>
<instance part="Q9" gate="Q" x="19.05" y="60.96"/>
<instance part="C9" gate="G$1" x="24.13" y="73.66"/>
<instance part="D9" gate="G$1" x="13.97" y="74.93" rot="R90"/>
<instance part="GND22" gate="1" x="19.05" y="45.72"/>
<instance part="SUPPLY19" gate="G$1" x="19.05" y="82.55"/>
<instance part="Q10" gate="Q" x="50.8" y="60.96"/>
<instance part="C10" gate="G$1" x="55.88" y="73.66"/>
<instance part="D10" gate="G$1" x="45.72" y="74.93" rot="R90"/>
<instance part="GND23" gate="1" x="50.8" y="45.72"/>
<instance part="SUPPLY20" gate="G$1" x="50.8" y="82.55"/>
<instance part="Q11" gate="Q" x="82.55" y="60.96"/>
<instance part="C11" gate="G$1" x="87.63" y="73.66"/>
<instance part="D11" gate="G$1" x="77.47" y="74.93" rot="R90"/>
<instance part="GND24" gate="1" x="82.55" y="45.72"/>
<instance part="SUPPLY21" gate="G$1" x="82.55" y="82.55"/>
<instance part="Q12" gate="Q" x="114.3" y="60.96"/>
<instance part="C12" gate="G$1" x="119.38" y="73.66"/>
<instance part="D12" gate="G$1" x="109.22" y="74.93" rot="R90"/>
<instance part="GND25" gate="1" x="114.3" y="45.72"/>
<instance part="SUPPLY22" gate="G$1" x="114.3" y="82.55"/>
<instance part="Q13" gate="Q" x="137.16" y="21.59"/>
<instance part="C13" gate="G$1" x="142.24" y="34.29"/>
<instance part="D13" gate="G$1" x="132.08" y="35.56" rot="R90"/>
<instance part="GND26" gate="1" x="137.16" y="6.35"/>
<instance part="SUPPLY23" gate="G$1" x="137.16" y="43.18"/>
<instance part="Q14" gate="Q" x="168.91" y="21.59"/>
<instance part="C14" gate="G$1" x="173.99" y="34.29"/>
<instance part="D14" gate="G$1" x="163.83" y="35.56" rot="R90"/>
<instance part="GND27" gate="1" x="168.91" y="6.35"/>
<instance part="SUPPLY24" gate="G$1" x="168.91" y="43.18"/>
<instance part="Q15" gate="Q" x="200.66" y="21.59"/>
<instance part="C15" gate="G$1" x="205.74" y="34.29"/>
<instance part="D15" gate="G$1" x="195.58" y="35.56" rot="R90"/>
<instance part="GND28" gate="1" x="200.66" y="6.35"/>
<instance part="SUPPLY25" gate="G$1" x="200.66" y="43.18"/>
<instance part="Q16" gate="Q" x="232.41" y="21.59"/>
<instance part="C16" gate="G$1" x="237.49" y="34.29"/>
<instance part="D16" gate="G$1" x="227.33" y="35.56" rot="R90"/>
<instance part="GND29" gate="1" x="232.41" y="6.35"/>
<instance part="SUPPLY26" gate="G$1" x="232.41" y="43.18"/>
<instance part="SUPPLY27" gate="G$1" x="179.07" y="140.97"/>
<instance part="GND30" gate="1" x="179.07" y="125.73"/>
<instance part="JP3" gate="G$1" x="194.31" y="136.398"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND14" gate="1" pin="GND"/>
<pinref part="Q1" gate="Q" pin="S"/>
<wire x1="19.05" y1="151.13" x2="19.05" y2="143.51" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND15" gate="1" pin="GND"/>
<pinref part="Q2" gate="Q" pin="S"/>
<wire x1="50.8" y1="151.13" x2="50.8" y2="143.51" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND16" gate="1" pin="GND"/>
<pinref part="Q3" gate="Q" pin="S"/>
<wire x1="82.55" y1="151.13" x2="82.55" y2="143.51" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND17" gate="1" pin="GND"/>
<pinref part="Q4" gate="Q" pin="S"/>
<wire x1="114.3" y1="151.13" x2="114.3" y2="143.51" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND18" gate="1" pin="GND"/>
<pinref part="Q5" gate="Q" pin="S"/>
<wire x1="19.05" y1="102.87" x2="19.05" y2="95.25" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND19" gate="1" pin="GND"/>
<pinref part="Q6" gate="Q" pin="S"/>
<wire x1="50.8" y1="102.87" x2="50.8" y2="95.25" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND20" gate="1" pin="GND"/>
<pinref part="Q7" gate="Q" pin="S"/>
<wire x1="82.55" y1="102.87" x2="82.55" y2="95.25" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND21" gate="1" pin="GND"/>
<pinref part="Q8" gate="Q" pin="S"/>
<wire x1="114.3" y1="102.87" x2="114.3" y2="95.25" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND22" gate="1" pin="GND"/>
<pinref part="Q9" gate="Q" pin="S"/>
<wire x1="19.05" y1="55.88" x2="19.05" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND23" gate="1" pin="GND"/>
<pinref part="Q10" gate="Q" pin="S"/>
<wire x1="50.8" y1="55.88" x2="50.8" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND24" gate="1" pin="GND"/>
<pinref part="Q11" gate="Q" pin="S"/>
<wire x1="82.55" y1="55.88" x2="82.55" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND25" gate="1" pin="GND"/>
<pinref part="Q12" gate="Q" pin="S"/>
<wire x1="114.3" y1="55.88" x2="114.3" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND26" gate="1" pin="GND"/>
<pinref part="Q13" gate="Q" pin="S"/>
<wire x1="137.16" y1="16.51" x2="137.16" y2="8.89" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND27" gate="1" pin="GND"/>
<pinref part="Q14" gate="Q" pin="S"/>
<wire x1="168.91" y1="16.51" x2="168.91" y2="8.89" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND28" gate="1" pin="GND"/>
<pinref part="Q15" gate="Q" pin="S"/>
<wire x1="200.66" y1="16.51" x2="200.66" y2="8.89" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND29" gate="1" pin="GND"/>
<pinref part="Q16" gate="Q" pin="S"/>
<wire x1="232.41" y1="16.51" x2="232.41" y2="8.89" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND30" gate="1" pin="GND"/>
<wire x1="179.07" y1="131.318" x2="179.07" y2="128.27" width="0.1524" layer="91"/>
<pinref part="JP3" gate="G$1" pin="2"/>
<wire x1="179.07" y1="131.318" x2="181.61" y2="131.318" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC_2" class="0">
<segment>
<pinref part="SUPPLY10" gate="G$1" pin="VCC_2"/>
<wire x1="19.05" y1="175.26" x2="19.05" y2="177.8" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="-"/>
<wire x1="13.97" y1="173.99" x2="13.97" y2="175.26" width="0.1524" layer="91"/>
<wire x1="13.97" y1="175.26" x2="19.05" y2="175.26" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="24.13" y1="173.99" x2="24.13" y2="175.26" width="0.1524" layer="91"/>
<wire x1="24.13" y1="175.26" x2="19.05" y2="175.26" width="0.1524" layer="91"/>
<junction x="19.05" y="175.26"/>
</segment>
<segment>
<pinref part="JP2" gate="A" pin="2"/>
<wire x1="193.04" y1="116.84" x2="200.66" y2="116.84" width="0.1524" layer="91"/>
<wire x1="200.66" y1="116.84" x2="200.66" y2="114.3" width="0.1524" layer="91"/>
<pinref part="JP2" gate="A" pin="4"/>
<wire x1="200.66" y1="114.3" x2="200.66" y2="111.76" width="0.1524" layer="91"/>
<wire x1="200.66" y1="111.76" x2="200.66" y2="109.22" width="0.1524" layer="91"/>
<wire x1="200.66" y1="109.22" x2="200.66" y2="106.68" width="0.1524" layer="91"/>
<wire x1="200.66" y1="106.68" x2="200.66" y2="104.14" width="0.1524" layer="91"/>
<wire x1="200.66" y1="104.14" x2="200.66" y2="101.6" width="0.1524" layer="91"/>
<wire x1="200.66" y1="101.6" x2="200.66" y2="99.06" width="0.1524" layer="91"/>
<wire x1="200.66" y1="99.06" x2="200.66" y2="96.52" width="0.1524" layer="91"/>
<wire x1="200.66" y1="96.52" x2="200.66" y2="93.98" width="0.1524" layer="91"/>
<wire x1="200.66" y1="93.98" x2="200.66" y2="91.44" width="0.1524" layer="91"/>
<wire x1="200.66" y1="91.44" x2="200.66" y2="88.9" width="0.1524" layer="91"/>
<wire x1="200.66" y1="88.9" x2="200.66" y2="86.36" width="0.1524" layer="91"/>
<wire x1="200.66" y1="86.36" x2="200.66" y2="83.82" width="0.1524" layer="91"/>
<wire x1="200.66" y1="83.82" x2="200.66" y2="81.28" width="0.1524" layer="91"/>
<wire x1="200.66" y1="81.28" x2="200.66" y2="78.74" width="0.1524" layer="91"/>
<wire x1="193.04" y1="114.3" x2="200.66" y2="114.3" width="0.1524" layer="91"/>
<junction x="200.66" y="114.3"/>
<pinref part="JP2" gate="A" pin="6"/>
<wire x1="193.04" y1="111.76" x2="200.66" y2="111.76" width="0.1524" layer="91"/>
<junction x="200.66" y="111.76"/>
<pinref part="JP2" gate="A" pin="8"/>
<wire x1="193.04" y1="109.22" x2="200.66" y2="109.22" width="0.1524" layer="91"/>
<junction x="200.66" y="109.22"/>
<pinref part="JP2" gate="A" pin="10"/>
<wire x1="193.04" y1="106.68" x2="200.66" y2="106.68" width="0.1524" layer="91"/>
<junction x="200.66" y="106.68"/>
<pinref part="JP2" gate="A" pin="12"/>
<wire x1="193.04" y1="104.14" x2="200.66" y2="104.14" width="0.1524" layer="91"/>
<junction x="200.66" y="104.14"/>
<pinref part="JP2" gate="A" pin="14"/>
<wire x1="193.04" y1="101.6" x2="200.66" y2="101.6" width="0.1524" layer="91"/>
<junction x="200.66" y="101.6"/>
<pinref part="JP2" gate="A" pin="16"/>
<wire x1="193.04" y1="99.06" x2="200.66" y2="99.06" width="0.1524" layer="91"/>
<junction x="200.66" y="99.06"/>
<pinref part="JP2" gate="A" pin="18"/>
<wire x1="193.04" y1="96.52" x2="200.66" y2="96.52" width="0.1524" layer="91"/>
<junction x="200.66" y="96.52"/>
<pinref part="JP2" gate="A" pin="20"/>
<wire x1="193.04" y1="93.98" x2="200.66" y2="93.98" width="0.1524" layer="91"/>
<junction x="200.66" y="93.98"/>
<pinref part="JP2" gate="A" pin="22"/>
<wire x1="193.04" y1="91.44" x2="200.66" y2="91.44" width="0.1524" layer="91"/>
<junction x="200.66" y="91.44"/>
<pinref part="JP2" gate="A" pin="24"/>
<wire x1="193.04" y1="88.9" x2="200.66" y2="88.9" width="0.1524" layer="91"/>
<junction x="200.66" y="88.9"/>
<pinref part="JP2" gate="A" pin="26"/>
<wire x1="193.04" y1="86.36" x2="200.66" y2="86.36" width="0.1524" layer="91"/>
<junction x="200.66" y="86.36"/>
<pinref part="JP2" gate="A" pin="28"/>
<wire x1="193.04" y1="83.82" x2="200.66" y2="83.82" width="0.1524" layer="91"/>
<junction x="200.66" y="83.82"/>
<pinref part="JP2" gate="A" pin="30"/>
<wire x1="193.04" y1="81.28" x2="200.66" y2="81.28" width="0.1524" layer="91"/>
<junction x="200.66" y="81.28"/>
<pinref part="JP2" gate="A" pin="32"/>
<wire x1="193.04" y1="78.74" x2="200.66" y2="78.74" width="0.1524" layer="91"/>
<pinref part="SUPPLY14" gate="G$1" pin="VCC_2"/>
<wire x1="200.66" y1="116.84" x2="200.66" y2="118.11" width="0.1524" layer="91"/>
<junction x="200.66" y="116.84"/>
</segment>
<segment>
<pinref part="SUPPLY11" gate="G$1" pin="VCC_2"/>
<wire x1="50.8" y1="175.26" x2="50.8" y2="177.8" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="-"/>
<wire x1="45.72" y1="173.99" x2="45.72" y2="175.26" width="0.1524" layer="91"/>
<wire x1="45.72" y1="175.26" x2="50.8" y2="175.26" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="55.88" y1="173.99" x2="55.88" y2="175.26" width="0.1524" layer="91"/>
<wire x1="55.88" y1="175.26" x2="50.8" y2="175.26" width="0.1524" layer="91"/>
<junction x="50.8" y="175.26"/>
</segment>
<segment>
<pinref part="SUPPLY12" gate="G$1" pin="VCC_2"/>
<wire x1="82.55" y1="175.26" x2="82.55" y2="177.8" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="-"/>
<wire x1="77.47" y1="173.99" x2="77.47" y2="175.26" width="0.1524" layer="91"/>
<wire x1="77.47" y1="175.26" x2="82.55" y2="175.26" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="87.63" y1="173.99" x2="87.63" y2="175.26" width="0.1524" layer="91"/>
<wire x1="87.63" y1="175.26" x2="82.55" y2="175.26" width="0.1524" layer="91"/>
<junction x="82.55" y="175.26"/>
</segment>
<segment>
<pinref part="SUPPLY13" gate="G$1" pin="VCC_2"/>
<wire x1="114.3" y1="175.26" x2="114.3" y2="177.8" width="0.1524" layer="91"/>
<pinref part="D4" gate="G$1" pin="-"/>
<wire x1="109.22" y1="173.99" x2="109.22" y2="175.26" width="0.1524" layer="91"/>
<wire x1="109.22" y1="175.26" x2="114.3" y2="175.26" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="119.38" y1="173.99" x2="119.38" y2="175.26" width="0.1524" layer="91"/>
<wire x1="119.38" y1="175.26" x2="114.3" y2="175.26" width="0.1524" layer="91"/>
<junction x="114.3" y="175.26"/>
</segment>
<segment>
<pinref part="SUPPLY15" gate="G$1" pin="VCC_2"/>
<wire x1="19.05" y1="127" x2="19.05" y2="129.54" width="0.1524" layer="91"/>
<pinref part="D5" gate="G$1" pin="-"/>
<wire x1="13.97" y1="125.73" x2="13.97" y2="127" width="0.1524" layer="91"/>
<wire x1="13.97" y1="127" x2="19.05" y2="127" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="24.13" y1="125.73" x2="24.13" y2="127" width="0.1524" layer="91"/>
<wire x1="24.13" y1="127" x2="19.05" y2="127" width="0.1524" layer="91"/>
<junction x="19.05" y="127"/>
</segment>
<segment>
<pinref part="SUPPLY16" gate="G$1" pin="VCC_2"/>
<wire x1="50.8" y1="127" x2="50.8" y2="129.54" width="0.1524" layer="91"/>
<pinref part="D6" gate="G$1" pin="-"/>
<wire x1="45.72" y1="125.73" x2="45.72" y2="127" width="0.1524" layer="91"/>
<wire x1="45.72" y1="127" x2="50.8" y2="127" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="55.88" y1="125.73" x2="55.88" y2="127" width="0.1524" layer="91"/>
<wire x1="55.88" y1="127" x2="50.8" y2="127" width="0.1524" layer="91"/>
<junction x="50.8" y="127"/>
</segment>
<segment>
<pinref part="SUPPLY17" gate="G$1" pin="VCC_2"/>
<wire x1="82.55" y1="127" x2="82.55" y2="129.54" width="0.1524" layer="91"/>
<pinref part="D7" gate="G$1" pin="-"/>
<wire x1="77.47" y1="125.73" x2="77.47" y2="127" width="0.1524" layer="91"/>
<wire x1="77.47" y1="127" x2="82.55" y2="127" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="87.63" y1="125.73" x2="87.63" y2="127" width="0.1524" layer="91"/>
<wire x1="87.63" y1="127" x2="82.55" y2="127" width="0.1524" layer="91"/>
<junction x="82.55" y="127"/>
</segment>
<segment>
<pinref part="SUPPLY18" gate="G$1" pin="VCC_2"/>
<wire x1="114.3" y1="127" x2="114.3" y2="129.54" width="0.1524" layer="91"/>
<pinref part="D8" gate="G$1" pin="-"/>
<wire x1="109.22" y1="125.73" x2="109.22" y2="127" width="0.1524" layer="91"/>
<wire x1="109.22" y1="127" x2="114.3" y2="127" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="119.38" y1="125.73" x2="119.38" y2="127" width="0.1524" layer="91"/>
<wire x1="119.38" y1="127" x2="114.3" y2="127" width="0.1524" layer="91"/>
<junction x="114.3" y="127"/>
</segment>
<segment>
<pinref part="SUPPLY19" gate="G$1" pin="VCC_2"/>
<wire x1="19.05" y1="80.01" x2="19.05" y2="82.55" width="0.1524" layer="91"/>
<pinref part="D9" gate="G$1" pin="-"/>
<wire x1="13.97" y1="78.74" x2="13.97" y2="80.01" width="0.1524" layer="91"/>
<wire x1="13.97" y1="80.01" x2="19.05" y2="80.01" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="24.13" y1="78.74" x2="24.13" y2="80.01" width="0.1524" layer="91"/>
<wire x1="24.13" y1="80.01" x2="19.05" y2="80.01" width="0.1524" layer="91"/>
<junction x="19.05" y="80.01"/>
</segment>
<segment>
<pinref part="SUPPLY20" gate="G$1" pin="VCC_2"/>
<wire x1="50.8" y1="80.01" x2="50.8" y2="82.55" width="0.1524" layer="91"/>
<pinref part="D10" gate="G$1" pin="-"/>
<wire x1="45.72" y1="78.74" x2="45.72" y2="80.01" width="0.1524" layer="91"/>
<wire x1="45.72" y1="80.01" x2="50.8" y2="80.01" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="55.88" y1="78.74" x2="55.88" y2="80.01" width="0.1524" layer="91"/>
<wire x1="55.88" y1="80.01" x2="50.8" y2="80.01" width="0.1524" layer="91"/>
<junction x="50.8" y="80.01"/>
</segment>
<segment>
<pinref part="SUPPLY21" gate="G$1" pin="VCC_2"/>
<wire x1="82.55" y1="80.01" x2="82.55" y2="82.55" width="0.1524" layer="91"/>
<pinref part="D11" gate="G$1" pin="-"/>
<wire x1="77.47" y1="78.74" x2="77.47" y2="80.01" width="0.1524" layer="91"/>
<wire x1="77.47" y1="80.01" x2="82.55" y2="80.01" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="87.63" y1="78.74" x2="87.63" y2="80.01" width="0.1524" layer="91"/>
<wire x1="87.63" y1="80.01" x2="82.55" y2="80.01" width="0.1524" layer="91"/>
<junction x="82.55" y="80.01"/>
</segment>
<segment>
<pinref part="SUPPLY22" gate="G$1" pin="VCC_2"/>
<wire x1="114.3" y1="80.01" x2="114.3" y2="82.55" width="0.1524" layer="91"/>
<pinref part="D12" gate="G$1" pin="-"/>
<wire x1="109.22" y1="78.74" x2="109.22" y2="80.01" width="0.1524" layer="91"/>
<wire x1="109.22" y1="80.01" x2="114.3" y2="80.01" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="119.38" y1="78.74" x2="119.38" y2="80.01" width="0.1524" layer="91"/>
<wire x1="119.38" y1="80.01" x2="114.3" y2="80.01" width="0.1524" layer="91"/>
<junction x="114.3" y="80.01"/>
</segment>
<segment>
<pinref part="SUPPLY23" gate="G$1" pin="VCC_2"/>
<wire x1="137.16" y1="40.64" x2="137.16" y2="43.18" width="0.1524" layer="91"/>
<pinref part="D13" gate="G$1" pin="-"/>
<wire x1="132.08" y1="39.37" x2="132.08" y2="40.64" width="0.1524" layer="91"/>
<wire x1="132.08" y1="40.64" x2="137.16" y2="40.64" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="142.24" y1="39.37" x2="142.24" y2="40.64" width="0.1524" layer="91"/>
<wire x1="142.24" y1="40.64" x2="137.16" y2="40.64" width="0.1524" layer="91"/>
<junction x="137.16" y="40.64"/>
</segment>
<segment>
<pinref part="SUPPLY24" gate="G$1" pin="VCC_2"/>
<wire x1="168.91" y1="40.64" x2="168.91" y2="43.18" width="0.1524" layer="91"/>
<pinref part="D14" gate="G$1" pin="-"/>
<wire x1="163.83" y1="39.37" x2="163.83" y2="40.64" width="0.1524" layer="91"/>
<wire x1="163.83" y1="40.64" x2="168.91" y2="40.64" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="173.99" y1="39.37" x2="173.99" y2="40.64" width="0.1524" layer="91"/>
<wire x1="173.99" y1="40.64" x2="168.91" y2="40.64" width="0.1524" layer="91"/>
<junction x="168.91" y="40.64"/>
</segment>
<segment>
<pinref part="SUPPLY25" gate="G$1" pin="VCC_2"/>
<wire x1="200.66" y1="40.64" x2="200.66" y2="43.18" width="0.1524" layer="91"/>
<pinref part="D15" gate="G$1" pin="-"/>
<wire x1="195.58" y1="39.37" x2="195.58" y2="40.64" width="0.1524" layer="91"/>
<wire x1="195.58" y1="40.64" x2="200.66" y2="40.64" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="205.74" y1="39.37" x2="205.74" y2="40.64" width="0.1524" layer="91"/>
<wire x1="205.74" y1="40.64" x2="200.66" y2="40.64" width="0.1524" layer="91"/>
<junction x="200.66" y="40.64"/>
</segment>
<segment>
<pinref part="SUPPLY26" gate="G$1" pin="VCC_2"/>
<wire x1="232.41" y1="40.64" x2="232.41" y2="43.18" width="0.1524" layer="91"/>
<pinref part="D16" gate="G$1" pin="-"/>
<wire x1="227.33" y1="39.37" x2="227.33" y2="40.64" width="0.1524" layer="91"/>
<wire x1="227.33" y1="40.64" x2="232.41" y2="40.64" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="237.49" y1="39.37" x2="237.49" y2="40.64" width="0.1524" layer="91"/>
<wire x1="237.49" y1="40.64" x2="232.41" y2="40.64" width="0.1524" layer="91"/>
<junction x="232.41" y="40.64"/>
</segment>
<segment>
<pinref part="SUPPLY27" gate="G$1" pin="VCC_2"/>
<wire x1="179.07" y1="137.668" x2="179.07" y2="140.97" width="0.1524" layer="91"/>
<pinref part="JP3" gate="G$1" pin="1"/>
<wire x1="179.07" y1="137.668" x2="181.61" y2="137.668" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MOTOR_0" class="0">
<segment>
<pinref part="JP2" gate="A" pin="1"/>
<wire x1="185.42" y1="116.84" x2="168.91" y2="116.84" width="0.1524" layer="91"/>
<label x="168.91" y="116.84" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q1" gate="Q" pin="D"/>
<wire x1="19.05" y1="161.29" x2="19.05" y2="162.56" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="19.05" y1="162.56" x2="19.05" y2="165.1" width="0.1524" layer="91"/>
<wire x1="19.05" y1="165.1" x2="24.13" y2="165.1" width="0.1524" layer="91"/>
<wire x1="24.13" y1="165.1" x2="24.13" y2="166.37" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="+"/>
<wire x1="19.05" y1="165.1" x2="13.97" y2="165.1" width="0.1524" layer="91"/>
<wire x1="13.97" y1="165.1" x2="13.97" y2="166.37" width="0.1524" layer="91"/>
<junction x="19.05" y="165.1"/>
<wire x1="19.05" y1="162.56" x2="31.75" y2="162.56" width="0.1524" layer="91"/>
<junction x="19.05" y="162.56"/>
<label x="21.59" y="162.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOTOR_1" class="0">
<segment>
<pinref part="JP2" gate="A" pin="3"/>
<wire x1="185.42" y1="114.3" x2="168.91" y2="114.3" width="0.1524" layer="91"/>
<label x="168.91" y="114.3" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q2" gate="Q" pin="D"/>
<wire x1="50.8" y1="161.29" x2="50.8" y2="162.56" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="50.8" y1="162.56" x2="50.8" y2="165.1" width="0.1524" layer="91"/>
<wire x1="50.8" y1="165.1" x2="55.88" y2="165.1" width="0.1524" layer="91"/>
<wire x1="55.88" y1="165.1" x2="55.88" y2="166.37" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="+"/>
<wire x1="50.8" y1="165.1" x2="45.72" y2="165.1" width="0.1524" layer="91"/>
<wire x1="45.72" y1="165.1" x2="45.72" y2="166.37" width="0.1524" layer="91"/>
<junction x="50.8" y="165.1"/>
<wire x1="50.8" y1="162.56" x2="63.5" y2="162.56" width="0.1524" layer="91"/>
<junction x="50.8" y="162.56"/>
<label x="53.34" y="162.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOTOR_2" class="0">
<segment>
<pinref part="JP2" gate="A" pin="5"/>
<wire x1="185.42" y1="111.76" x2="168.91" y2="111.76" width="0.1524" layer="91"/>
<label x="168.91" y="111.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q3" gate="Q" pin="D"/>
<wire x1="82.55" y1="161.29" x2="82.55" y2="162.56" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="82.55" y1="162.56" x2="82.55" y2="165.1" width="0.1524" layer="91"/>
<wire x1="82.55" y1="165.1" x2="87.63" y2="165.1" width="0.1524" layer="91"/>
<wire x1="87.63" y1="165.1" x2="87.63" y2="166.37" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="+"/>
<wire x1="82.55" y1="165.1" x2="77.47" y2="165.1" width="0.1524" layer="91"/>
<wire x1="77.47" y1="165.1" x2="77.47" y2="166.37" width="0.1524" layer="91"/>
<junction x="82.55" y="165.1"/>
<wire x1="82.55" y1="162.56" x2="95.25" y2="162.56" width="0.1524" layer="91"/>
<junction x="82.55" y="162.56"/>
<label x="85.09" y="162.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOTOR_3" class="0">
<segment>
<pinref part="JP2" gate="A" pin="7"/>
<wire x1="185.42" y1="109.22" x2="168.91" y2="109.22" width="0.1524" layer="91"/>
<label x="168.91" y="109.22" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q4" gate="Q" pin="D"/>
<wire x1="114.3" y1="161.29" x2="114.3" y2="162.56" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="114.3" y1="162.56" x2="114.3" y2="165.1" width="0.1524" layer="91"/>
<wire x1="114.3" y1="165.1" x2="119.38" y2="165.1" width="0.1524" layer="91"/>
<wire x1="119.38" y1="165.1" x2="119.38" y2="166.37" width="0.1524" layer="91"/>
<pinref part="D4" gate="G$1" pin="+"/>
<wire x1="114.3" y1="165.1" x2="109.22" y2="165.1" width="0.1524" layer="91"/>
<wire x1="109.22" y1="165.1" x2="109.22" y2="166.37" width="0.1524" layer="91"/>
<junction x="114.3" y="165.1"/>
<wire x1="114.3" y1="162.56" x2="127" y2="162.56" width="0.1524" layer="91"/>
<junction x="114.3" y="162.56"/>
<label x="118.11" y="162.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOTOR_4" class="0">
<segment>
<pinref part="JP2" gate="A" pin="9"/>
<wire x1="185.42" y1="106.68" x2="168.91" y2="106.68" width="0.1524" layer="91"/>
<label x="168.91" y="106.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q5" gate="Q" pin="D"/>
<wire x1="19.05" y1="113.03" x2="19.05" y2="114.3" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="19.05" y1="114.3" x2="19.05" y2="116.84" width="0.1524" layer="91"/>
<wire x1="19.05" y1="116.84" x2="24.13" y2="116.84" width="0.1524" layer="91"/>
<wire x1="24.13" y1="116.84" x2="24.13" y2="118.11" width="0.1524" layer="91"/>
<pinref part="D5" gate="G$1" pin="+"/>
<wire x1="19.05" y1="116.84" x2="13.97" y2="116.84" width="0.1524" layer="91"/>
<wire x1="13.97" y1="116.84" x2="13.97" y2="118.11" width="0.1524" layer="91"/>
<junction x="19.05" y="116.84"/>
<wire x1="19.05" y1="114.3" x2="31.75" y2="114.3" width="0.1524" layer="91"/>
<junction x="19.05" y="114.3"/>
<label x="22.86" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOTOR_5" class="0">
<segment>
<pinref part="JP2" gate="A" pin="11"/>
<wire x1="185.42" y1="104.14" x2="168.91" y2="104.14" width="0.1524" layer="91"/>
<label x="168.91" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q6" gate="Q" pin="D"/>
<wire x1="50.8" y1="113.03" x2="50.8" y2="114.3" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="50.8" y1="114.3" x2="50.8" y2="116.84" width="0.1524" layer="91"/>
<wire x1="50.8" y1="116.84" x2="55.88" y2="116.84" width="0.1524" layer="91"/>
<wire x1="55.88" y1="116.84" x2="55.88" y2="118.11" width="0.1524" layer="91"/>
<pinref part="D6" gate="G$1" pin="+"/>
<wire x1="50.8" y1="116.84" x2="45.72" y2="116.84" width="0.1524" layer="91"/>
<wire x1="45.72" y1="116.84" x2="45.72" y2="118.11" width="0.1524" layer="91"/>
<junction x="50.8" y="116.84"/>
<wire x1="50.8" y1="114.3" x2="63.5" y2="114.3" width="0.1524" layer="91"/>
<junction x="50.8" y="114.3"/>
<label x="53.34" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOTOR_6" class="0">
<segment>
<pinref part="JP2" gate="A" pin="13"/>
<wire x1="185.42" y1="101.6" x2="168.91" y2="101.6" width="0.1524" layer="91"/>
<label x="168.91" y="101.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q7" gate="Q" pin="D"/>
<wire x1="82.55" y1="113.03" x2="82.55" y2="114.3" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="82.55" y1="114.3" x2="82.55" y2="116.84" width="0.1524" layer="91"/>
<wire x1="82.55" y1="116.84" x2="87.63" y2="116.84" width="0.1524" layer="91"/>
<wire x1="87.63" y1="116.84" x2="87.63" y2="118.11" width="0.1524" layer="91"/>
<pinref part="D7" gate="G$1" pin="+"/>
<wire x1="82.55" y1="116.84" x2="77.47" y2="116.84" width="0.1524" layer="91"/>
<wire x1="77.47" y1="116.84" x2="77.47" y2="118.11" width="0.1524" layer="91"/>
<junction x="82.55" y="116.84"/>
<wire x1="82.55" y1="114.3" x2="95.25" y2="114.3" width="0.1524" layer="91"/>
<junction x="82.55" y="114.3"/>
<label x="85.09" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOTOR_7" class="0">
<segment>
<pinref part="JP2" gate="A" pin="15"/>
<wire x1="185.42" y1="99.06" x2="168.91" y2="99.06" width="0.1524" layer="91"/>
<label x="168.91" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q8" gate="Q" pin="D"/>
<wire x1="114.3" y1="113.03" x2="114.3" y2="114.3" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="114.3" y1="114.3" x2="114.3" y2="116.84" width="0.1524" layer="91"/>
<wire x1="114.3" y1="116.84" x2="119.38" y2="116.84" width="0.1524" layer="91"/>
<wire x1="119.38" y1="116.84" x2="119.38" y2="118.11" width="0.1524" layer="91"/>
<pinref part="D8" gate="G$1" pin="+"/>
<wire x1="114.3" y1="116.84" x2="109.22" y2="116.84" width="0.1524" layer="91"/>
<wire x1="109.22" y1="116.84" x2="109.22" y2="118.11" width="0.1524" layer="91"/>
<junction x="114.3" y="116.84"/>
<wire x1="114.3" y1="114.3" x2="127" y2="114.3" width="0.1524" layer="91"/>
<junction x="114.3" y="114.3"/>
<label x="116.84" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOTOR_8" class="0">
<segment>
<pinref part="JP2" gate="A" pin="17"/>
<wire x1="185.42" y1="96.52" x2="168.91" y2="96.52" width="0.1524" layer="91"/>
<label x="168.91" y="96.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q9" gate="Q" pin="D"/>
<wire x1="19.05" y1="66.04" x2="19.05" y2="67.31" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="19.05" y1="67.31" x2="19.05" y2="69.85" width="0.1524" layer="91"/>
<wire x1="19.05" y1="69.85" x2="24.13" y2="69.85" width="0.1524" layer="91"/>
<wire x1="24.13" y1="69.85" x2="24.13" y2="71.12" width="0.1524" layer="91"/>
<pinref part="D9" gate="G$1" pin="+"/>
<wire x1="19.05" y1="69.85" x2="13.97" y2="69.85" width="0.1524" layer="91"/>
<wire x1="13.97" y1="69.85" x2="13.97" y2="71.12" width="0.1524" layer="91"/>
<junction x="19.05" y="69.85"/>
<wire x1="19.05" y1="67.31" x2="31.75" y2="67.31" width="0.1524" layer="91"/>
<junction x="19.05" y="67.31"/>
<label x="21.59" y="67.31" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOTOR_9" class="0">
<segment>
<pinref part="JP2" gate="A" pin="19"/>
<wire x1="185.42" y1="93.98" x2="168.91" y2="93.98" width="0.1524" layer="91"/>
<label x="168.91" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q10" gate="Q" pin="D"/>
<wire x1="50.8" y1="66.04" x2="50.8" y2="67.31" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="50.8" y1="67.31" x2="50.8" y2="69.85" width="0.1524" layer="91"/>
<wire x1="50.8" y1="69.85" x2="55.88" y2="69.85" width="0.1524" layer="91"/>
<wire x1="55.88" y1="69.85" x2="55.88" y2="71.12" width="0.1524" layer="91"/>
<pinref part="D10" gate="G$1" pin="+"/>
<wire x1="50.8" y1="69.85" x2="45.72" y2="69.85" width="0.1524" layer="91"/>
<wire x1="45.72" y1="69.85" x2="45.72" y2="71.12" width="0.1524" layer="91"/>
<junction x="50.8" y="69.85"/>
<wire x1="50.8" y1="67.31" x2="63.5" y2="67.31" width="0.1524" layer="91"/>
<junction x="50.8" y="67.31"/>
<label x="53.34" y="67.31" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOTOR_10" class="0">
<segment>
<pinref part="JP2" gate="A" pin="21"/>
<wire x1="185.42" y1="91.44" x2="168.91" y2="91.44" width="0.1524" layer="91"/>
<label x="168.91" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q11" gate="Q" pin="D"/>
<wire x1="82.55" y1="66.04" x2="82.55" y2="67.31" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="82.55" y1="67.31" x2="82.55" y2="69.85" width="0.1524" layer="91"/>
<wire x1="82.55" y1="69.85" x2="87.63" y2="69.85" width="0.1524" layer="91"/>
<wire x1="87.63" y1="69.85" x2="87.63" y2="71.12" width="0.1524" layer="91"/>
<pinref part="D11" gate="G$1" pin="+"/>
<wire x1="82.55" y1="69.85" x2="77.47" y2="69.85" width="0.1524" layer="91"/>
<wire x1="77.47" y1="69.85" x2="77.47" y2="71.12" width="0.1524" layer="91"/>
<junction x="82.55" y="69.85"/>
<wire x1="82.55" y1="67.31" x2="95.25" y2="67.31" width="0.1524" layer="91"/>
<junction x="82.55" y="67.31"/>
<label x="85.09" y="67.31" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOTOR_11" class="0">
<segment>
<pinref part="JP2" gate="A" pin="23"/>
<wire x1="185.42" y1="88.9" x2="168.91" y2="88.9" width="0.1524" layer="91"/>
<label x="168.91" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q12" gate="Q" pin="D"/>
<wire x1="114.3" y1="66.04" x2="114.3" y2="67.31" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="114.3" y1="67.31" x2="114.3" y2="69.85" width="0.1524" layer="91"/>
<wire x1="114.3" y1="69.85" x2="119.38" y2="69.85" width="0.1524" layer="91"/>
<wire x1="119.38" y1="69.85" x2="119.38" y2="71.12" width="0.1524" layer="91"/>
<pinref part="D12" gate="G$1" pin="+"/>
<wire x1="114.3" y1="69.85" x2="109.22" y2="69.85" width="0.1524" layer="91"/>
<wire x1="109.22" y1="69.85" x2="109.22" y2="71.12" width="0.1524" layer="91"/>
<junction x="114.3" y="69.85"/>
<wire x1="114.3" y1="67.31" x2="127" y2="67.31" width="0.1524" layer="91"/>
<junction x="114.3" y="67.31"/>
<label x="116.84" y="67.31" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOTOR_12" class="0">
<segment>
<pinref part="JP2" gate="A" pin="25"/>
<wire x1="185.42" y1="86.36" x2="168.91" y2="86.36" width="0.1524" layer="91"/>
<label x="168.91" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q13" gate="Q" pin="D"/>
<wire x1="137.16" y1="26.67" x2="137.16" y2="27.94" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="137.16" y1="27.94" x2="137.16" y2="30.48" width="0.1524" layer="91"/>
<wire x1="137.16" y1="30.48" x2="142.24" y2="30.48" width="0.1524" layer="91"/>
<wire x1="142.24" y1="30.48" x2="142.24" y2="31.75" width="0.1524" layer="91"/>
<pinref part="D13" gate="G$1" pin="+"/>
<wire x1="137.16" y1="30.48" x2="132.08" y2="30.48" width="0.1524" layer="91"/>
<wire x1="132.08" y1="30.48" x2="132.08" y2="31.75" width="0.1524" layer="91"/>
<junction x="137.16" y="30.48"/>
<wire x1="137.16" y1="27.94" x2="149.86" y2="27.94" width="0.1524" layer="91"/>
<junction x="137.16" y="27.94"/>
<label x="139.7" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOTOR_13" class="0">
<segment>
<pinref part="JP2" gate="A" pin="27"/>
<wire x1="185.42" y1="83.82" x2="168.91" y2="83.82" width="0.1524" layer="91"/>
<label x="168.91" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q14" gate="Q" pin="D"/>
<wire x1="168.91" y1="26.67" x2="168.91" y2="27.94" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="168.91" y1="27.94" x2="168.91" y2="30.48" width="0.1524" layer="91"/>
<wire x1="168.91" y1="30.48" x2="173.99" y2="30.48" width="0.1524" layer="91"/>
<wire x1="173.99" y1="30.48" x2="173.99" y2="31.75" width="0.1524" layer="91"/>
<pinref part="D14" gate="G$1" pin="+"/>
<wire x1="168.91" y1="30.48" x2="163.83" y2="30.48" width="0.1524" layer="91"/>
<wire x1="163.83" y1="30.48" x2="163.83" y2="31.75" width="0.1524" layer="91"/>
<junction x="168.91" y="30.48"/>
<wire x1="168.91" y1="27.94" x2="181.61" y2="27.94" width="0.1524" layer="91"/>
<junction x="168.91" y="27.94"/>
<label x="171.45" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOTOR_14" class="0">
<segment>
<pinref part="JP2" gate="A" pin="29"/>
<wire x1="185.42" y1="81.28" x2="168.91" y2="81.28" width="0.1524" layer="91"/>
<label x="168.91" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q15" gate="Q" pin="D"/>
<wire x1="200.66" y1="26.67" x2="200.66" y2="27.94" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="200.66" y1="27.94" x2="200.66" y2="30.48" width="0.1524" layer="91"/>
<wire x1="200.66" y1="30.48" x2="205.74" y2="30.48" width="0.1524" layer="91"/>
<wire x1="205.74" y1="30.48" x2="205.74" y2="31.75" width="0.1524" layer="91"/>
<pinref part="D15" gate="G$1" pin="+"/>
<wire x1="200.66" y1="30.48" x2="195.58" y2="30.48" width="0.1524" layer="91"/>
<wire x1="195.58" y1="30.48" x2="195.58" y2="31.75" width="0.1524" layer="91"/>
<junction x="200.66" y="30.48"/>
<wire x1="200.66" y1="27.94" x2="213.36" y2="27.94" width="0.1524" layer="91"/>
<junction x="200.66" y="27.94"/>
<label x="203.2" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOTOR_15" class="0">
<segment>
<pinref part="JP2" gate="A" pin="31"/>
<wire x1="185.42" y1="78.74" x2="168.91" y2="78.74" width="0.1524" layer="91"/>
<label x="168.91" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q16" gate="Q" pin="D"/>
<wire x1="232.41" y1="26.67" x2="232.41" y2="27.94" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="232.41" y1="27.94" x2="232.41" y2="30.48" width="0.1524" layer="91"/>
<wire x1="232.41" y1="30.48" x2="237.49" y2="30.48" width="0.1524" layer="91"/>
<wire x1="237.49" y1="30.48" x2="237.49" y2="31.75" width="0.1524" layer="91"/>
<pinref part="D16" gate="G$1" pin="+"/>
<wire x1="232.41" y1="30.48" x2="227.33" y2="30.48" width="0.1524" layer="91"/>
<wire x1="227.33" y1="30.48" x2="227.33" y2="31.75" width="0.1524" layer="91"/>
<junction x="232.41" y="30.48"/>
<wire x1="232.41" y1="27.94" x2="245.11" y2="27.94" width="0.1524" layer="91"/>
<junction x="232.41" y="27.94"/>
<label x="234.95" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM_0" class="0">
<segment>
<pinref part="Q1" gate="Q" pin="G"/>
<wire x1="13.97" y1="156.21" x2="5.08" y2="156.21" width="0.1524" layer="91"/>
<label x="2.54" y="156.21" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM_1" class="0">
<segment>
<pinref part="Q2" gate="Q" pin="G"/>
<wire x1="45.72" y1="156.21" x2="36.83" y2="156.21" width="0.1524" layer="91"/>
<label x="34.29" y="156.21" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM_2" class="0">
<segment>
<pinref part="Q3" gate="Q" pin="G"/>
<wire x1="77.47" y1="156.21" x2="68.58" y2="156.21" width="0.1524" layer="91"/>
<label x="66.04" y="156.21" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM_3" class="0">
<segment>
<pinref part="Q4" gate="Q" pin="G"/>
<wire x1="109.22" y1="156.21" x2="100.33" y2="156.21" width="0.1524" layer="91"/>
<label x="97.79" y="156.21" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM_4" class="0">
<segment>
<pinref part="Q5" gate="Q" pin="G"/>
<wire x1="13.97" y1="107.95" x2="5.08" y2="107.95" width="0.1524" layer="91"/>
<label x="2.54" y="107.95" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM_5" class="0">
<segment>
<pinref part="Q6" gate="Q" pin="G"/>
<wire x1="45.72" y1="107.95" x2="36.83" y2="107.95" width="0.1524" layer="91"/>
<label x="34.29" y="107.95" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM_6" class="0">
<segment>
<pinref part="Q7" gate="Q" pin="G"/>
<wire x1="77.47" y1="107.95" x2="68.58" y2="107.95" width="0.1524" layer="91"/>
<label x="66.04" y="107.95" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM_7" class="0">
<segment>
<pinref part="Q8" gate="Q" pin="G"/>
<wire x1="109.22" y1="107.95" x2="100.33" y2="107.95" width="0.1524" layer="91"/>
<label x="97.79" y="107.95" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM_8" class="0">
<segment>
<pinref part="Q9" gate="Q" pin="G"/>
<wire x1="13.97" y1="60.96" x2="5.08" y2="60.96" width="0.1524" layer="91"/>
<label x="2.54" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM_9" class="0">
<segment>
<pinref part="Q10" gate="Q" pin="G"/>
<wire x1="45.72" y1="60.96" x2="36.83" y2="60.96" width="0.1524" layer="91"/>
<label x="34.29" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM_10" class="0">
<segment>
<pinref part="Q11" gate="Q" pin="G"/>
<wire x1="77.47" y1="60.96" x2="68.58" y2="60.96" width="0.1524" layer="91"/>
<label x="64.77" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM_11" class="0">
<segment>
<pinref part="Q12" gate="Q" pin="G"/>
<wire x1="109.22" y1="60.96" x2="100.33" y2="60.96" width="0.1524" layer="91"/>
<label x="96.52" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM_12" class="0">
<segment>
<pinref part="Q13" gate="Q" pin="G"/>
<wire x1="132.08" y1="21.59" x2="123.19" y2="21.59" width="0.1524" layer="91"/>
<label x="119.38" y="21.59" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM_13" class="0">
<segment>
<pinref part="Q14" gate="Q" pin="G"/>
<wire x1="163.83" y1="21.59" x2="154.94" y2="21.59" width="0.1524" layer="91"/>
<label x="151.13" y="21.59" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM_14" class="0">
<segment>
<pinref part="Q15" gate="Q" pin="G"/>
<wire x1="195.58" y1="21.59" x2="186.69" y2="21.59" width="0.1524" layer="91"/>
<label x="182.88" y="21.59" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM_15" class="0">
<segment>
<pinref part="Q16" gate="Q" pin="G"/>
<wire x1="227.33" y1="21.59" x2="218.44" y2="21.59" width="0.1524" layer="91"/>
<label x="214.63" y="21.59" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="104,1,36.83,97.79,JP1,VDD_3V3EXP,VCC_1,,,"/>
<approved hash="104,1,92.71,97.79,JP1,VDD_3V3EXP,VCC_1,,,"/>
<approved hash="204,1,36.83,95.25,JP1,VDD_5V,,,,"/>
<approved hash="204,1,92.71,95.25,JP1,VDD_5V,,,,"/>
<approved hash="204,1,36.83,92.71,JP1,SYS_5V,,,,"/>
<approved hash="204,1,36.83,52.07,JP1,AIN0,,,,"/>
<approved hash="202,1,36.83,49.53,JP1,CLKOUT2,,,,"/>
<approved hash="104,1,163.83,154.94,U1,VDD,VCC_1,,,"/>
<approved hash="113,2,189.188,99.1912,JP2,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
