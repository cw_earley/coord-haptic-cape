# Haptic Cape for BeagleBone Black/Green by Coord.Space

This is the official repository for the Haptic Cape by coord.system. A simple daughterboard for the BeagleBone Black/Green line of development platforms that aims to make experiemnting with haptic neuroprosthesis as easy as possible.

For more information on the current state of the project, check out the [Hackster.IO project documentation!](https://www.hackster.io/cw-earley/haptic-neurohacking-101-08ac18)

## How To Use

Within this repo are two assets use can use to get a cape fabricated:

* A pregenerated set of gerbers made with Seeed Studio's cam job file

You can use these to get your board fabbed using Seeed's Fusions service. It's what I used to get the first run made with nice results.

* The full Eagle Schematics + Board Layout

Just open up the .brd file in Eagle (Free Edition is fine) and use the cam job file from your desired fab house to generate compatible gerbers.

## Contributing

Feel free to fork, pull, suggest improvements, and report any bugs.
If you require any help getting the system installed and working, feel free to message me via
bitbucket mail.

## License

These assets were made available under the [Creative Commons Attribution-ShareAlike 4.0](http://creativecommons.org/licenses/by-sa/4.0/) license.